<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];
$productUid = $_SESSION['product_uid'];

$conn = connDB();

$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

	$image_array_1 = explode(";", $data);

	$image_array_2 = explode(",", $image_array_1[1]);

	$data = base64_decode($image_array_2[1]);

	$imageName = time().$productUid.'.png';
	// $imageName = time();

	file_put_contents('uploads/'.$imageName, $data);

	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";
		//echo $imageName;

		if($imageName)
		{
		array_push($tableName,"image_two");
		array_push($tableValue,$imageName);
		$stringType .=  "s";
        }
        
		array_push($tableValue,$productUid);
		$stringType .=  "s";
		$product = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
		// unset($_SESSION['product_uid']);
		if($product)
		{
			?>
			<div class="preview-img-div">
			    <img src=uploads/<?php echo $imageName  ?>  class="width100" />
			</div>
			<div class="width100 overflow text-center">     
				<p class="review-product-name text-center ow-margin-top20">Complete</p>
			</div>
			<?php
		}
		else
		{
			echo "fail";
		}

	}
	else
	{
		echo "ERROR";
	}


}

?>