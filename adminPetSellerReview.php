<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Reviews.php';
require_once dirname(__FILE__) . '/classes/ReviewsRespond.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Seller Review | Mypetslibrary" />
<title>Pet Seller Review | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="review-big-div width100 same-padding overflow menu-distance min-height3">

<?php
// Program to display URL of current page.
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
$link = "https";
else
$link = "http";

// Here append the common URL characters.
$link .= "://";

// Append the host(domain name, ip) to the URL.
$link .= $_SERVER['HTTP_HOST'];

// Append the requested resource location to the URL
$link .= $_SERVER['REQUEST_URI'];

if(isset($_GET['id']))
{
    $referrerUidLink = $_GET['id'];
    // echo $referrerUidLink;
}
else
{
    $referrerUidLink = "";
    // echo $referrerUidLink;
}
?>

        <?php
        if(isset($_GET['id']))
        {
            $conn = connDB();
            $reviewDetails = getReviews($conn,"WHERE uid = ? AND display = 'Yes' ", array("uid") ,array($_GET['id']),"s");
            // $reviewDetails = getReviews($conn);
            if($reviewDetails)
            {
                for($cnt = 0;$cnt < count($reviewDetails) ;$cnt++)
                {
                ?>
                    <div class="one-row-review-admin">
                        <div class="left-review-profile">
                            <!-- <img src="img/pet-seller2.jpg" class="profile-pic-css"> -->

                            <?php
                            $reviewAuthorUid =  $reviewDetails[$cnt]->getAuthorUid();

                            $userRows = getUser($conn,"WHERE uid = ? ", array("uid") ,array($reviewAuthorUid),"s");
                            $userData = $userRows[0];
                            $userProPic = $userData->getProfilePic();
                            ?>

                            <img src="userProfilePic/<?php echo $userProPic;?>" class="profile-pic-css">

                        </div>
                        <div class="left-review-data">
                            <p class="review-username-p">
                                <!-- <?php //echo $reviewDetails[$cnt]->getAuthorName();?> -->
                                <?php $reviewWriter = $reviewDetails[$cnt]->getAuthorName();
                                    if($reviewWriter == 'admin')
                                    {
                                        echo "Mypetslibrary";
                                    }
                                    else
                                    {
                                        echo $reviewWriter;
                                    }
                                ?>
                            </p>

                            <div class="review-star-div">
                                <?php $display = $reviewDetails[$cnt]->getRating();?>

                                <?php
                                    if ($display == 1)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                    else if ($display == 2)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                    else if ($display == 3)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                            '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                    else if ($display == 4)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                            '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                    else if ($display == 5)
                                    {
                                        echo '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                        '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">','<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">',
                                        '<img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">';
                                    }
                                ?>
                            </div>
                        </div>



                        <div class="clear"></div>
                        <div class="width100 review-content">
                            <p class="review-content-p">
                                <?php echo $reviewDetails[$cnt]->getParagraphOne();?>
                            </p>
                        </div>
                        <p class="review-date"><?php echo $date = date("d-m-Y",strtotime($reviewDetails[$cnt]->getDateCreated()));?></p>

                    </div>

                <?php
                }
                ?>
            <?php
            }
            ?>
        <?php
        }
        ?>

        <div class="clear"></div>

        <?php
        if($uid == $referrerUidLink)
        {
        ?>
        <?php
        }
        elseif($uid !="")
        {
        ?>


            <!-- Review Modal -->
            <div id="review-modal" class="modal-css">
            <!-- Modal content -->
            <!-- <div class="modal-content-css forgot-modal-content login-modal-content review-modal-margin"> -->
            <div class="modal-content-css forgot-modal-content login-modal-content review-modal-margin star-review-modal">
                <span class="close-css close-review">&times;</span>
                <h2 class="green-text h2-title">Review</h2>
                <div class="green-border"></div>
                <div class="clear"></div>


                <!-- Upload  -->
                <!-- <form id="file-upload-form" class="uploader"> -->

            </div>
            </div>



        <?php
        }
        else
        {
        ?>

        <?php
        }
        ?>

    </div>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Review submitted ! <br> Waiting approval from admin !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to submit review !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Review has been reported and submitted to admin !!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Unable to report the review !!";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Fail to report review !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>
