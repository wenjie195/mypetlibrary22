<div class="sticky-footer width100 same-padding green-bg overflow">

    <div class="web-view-footer">
    	<a class="hover1 pointer" onclick="goBack()">
            <img src="img/back.png" class="footer-img hover1a" alt="Back" title="Back">
            <img src="img/back2.png" class="footer-img hover1b" alt="Back" title="Back">        	
        </a>    
    	<a href="index.php" class="hover1 footer-margin1 home-a">
            <img src="img/home-1.png" class="footer-img hover1a" alt="Home" title="Home">
            <img src="img/home-2.png" class="footer-img hover1b" alt="Home" title="Home">        	
        </a>
    	<a href="malaysia-cute-puppy-dog.php" class="hover1 footer-margin1 puppy-a">
            <img src="img/puppy-1.png" class="footer-img hover1a" alt="Puppy" title="Puppy">
            <img src="img/puppy-2.png" class="footer-img hover1b" alt="Puppy" title="Puppy">        	
        </a>
    	<a href="malaysia-cute-kitten-cat.php" class="hover1 footer-margin1 kitten-a">
            <img src="img/cat-1.png" class="footer-img hover1a" alt="Kitten" title="Kitten">
            <img src="img/cat-2.png" class="footer-img hover1b" alt="Kitten" title="Kitten">        	
        </a>        
    	<!--<a href="malaysia-cute-reptiles.php" class="hover1 footer-margin1 reptile-a">
            <img src="img/reptile-1.png" class="footer-img hover1a" alt="Reptile" title="Reptile">
            <img src="img/reptile-2.png" class="footer-img hover1b" alt="Reptile" title="Reptile">        	
        </a>-->
    	<a href="malaysia-pet-food-toy-product.php" class="hover1 footer-margin1 product-a">
            <img src="img/pet-food-1.png" class="footer-img hover1a" alt="Pet Food" title="Pet Food">
            <img src="img/pet-food-2.png" class="footer-img hover1b" alt="Pet Food" title="Pet Food">        	
        </a>
    	<a href="pet-seller-grooming-delivery-hotel.php" class="hover1 footer-margin1 partner-a">
            <img src="img/partner-1.png" class="footer-img hover1a" alt="Partner" title="Partner">
            <img src="img/partner-2.png" class="footer-img hover1b" alt="Partner" title="Partner">        	
        </a>        
    	<a href="malaysia-pet-blog.php" class="hover1 footer-margin1 blog-a">
            <img src="img/pet-blog.png" class="footer-img hover1a" alt="Pet Blog" title="Pet Blog">
            <img src="img/pet-blog-2.png" class="footer-img hover1b" alt="Pet Blog" title="Pet Blog">        	
        </a>        
                            
    </div>

</div>