<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

//$id = $_SESSION['id'];
$conn = connDB();

//$userRows = getUser($conn," WHERE id = ? ",array("id"),array($id),"i");
//$userDetails = $userRows[0];

$states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add User | Mypetslibrary" />
<title>Add User | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add New User</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        <form method="POST" action="utilities/registerUserFunction.php">
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Username*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Username" name="register_name" id="register_name">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Email*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Email" required name="register_email" id="register_email">     
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Facebook ID (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Facebook ID (Optional)"  required name="register_fbId" id="register_fbId">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Contact No.*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Contact No."  required name="register_phone" id="register_phone">    
        </div>   
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Birthday</p>
        	<input class="input-name clean input-textarea admin-input" type="date" placeholder="Birthday" name="register_birthday" id="register_birthday">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Gender</p>
        	<select class="input-name clean admin-input" required name="register_gender" id="register_gender">
            	<option>Male</option>
                <option>Female</option>
            </select>   
        </div>           
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Account Status</p>
        	<select class="input-name clean admin-input" required name="register_status" id="register_status">
            	<option>Active</option>
                <option>Banned</option>
            </select>   
        </div>   

        <div class="clear"></div>  

        <!-- Photo cropping into square size feature -->
		<!-- <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Upload Profile Picture</p>
        </div>       -->
               
        <div class="clear"></div>
    	<p class="review-product-name">Shipping Details</p>        
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Receiver Name</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Receiver Name" name="register_receiver_name" id="register_receiver_name">    
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Receiver Contact No.</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Receiver Contact Number" name="register_receiver_no" id="register_receiver_no">    
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">State</p>
        	<!-- <select class="input-name clean admin-input" name="register_state" id="register_state">
            	<option>State</option>
                <option>Penang</option>
            </select>       -->

            <select class="input-name clean admin-input" type="text" name="register_state" id="register_state" required>
                <option value="">State</option>
                <?php
                for ($cnt=0; $cnt <count($states) ; $cnt++)
                {
                ?>
                    <option value="<?php echo $states[$cnt]->getStateName(); ?>"> 
                        <?php echo $states[$cnt]->getStateName(); ?>
                    </option>
                <?php
                }
                ?>
            </select> 

        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Area</p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Area" name="register_area" id="register_area">  
        	<!-- <select class="input-name clean admin-input" name="register_area" id="register_area">
            	<option>Area</option>
                <option>Bayan Baru</option>
            </select>        -->
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Postal Code</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Postal Code" name="register_postal" id="register_postal">    
        </div> 
   
        <div class="clear"></div>                
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Shipping Address</p>
        	<textarea class="input-name clean input-textarea address-textarea admin-address-textarea" type="text" placeholder="Shipping Address" name="register_address" id="register_address"></textarea>
        </div>
        <div class="clear"></div>
    	<p class="review-product-name">Bank Account Details</p> 
        <div class="dual-input">
            <p class="input-top-p admin-top-p">Bank</p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Bank Name" name="register_bank" id="register_bank">  
        	<!-- <select class="input-name clean admin-input" required name="register_bank" id="register_bank">
            	<option>Bank</option>
                <option>Maybank</option>
                <option>CIMB</option>
            </select>      -->
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Bank Account Holder Name</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Bank Account Holder Name" name="register_bank_holder" id="register_bank_holder">      
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Bank Account No.</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Bank Account No." name="register_bank_no" id="register_bank_no">         
        </div>
        <div class="clear"></div>
    	<!--<p class="review-product-name">Credit/Debit Card</p>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Name on Card</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Name on Card" required name="register_name_onCard" id="register_name_onCard">    
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Card Number</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Card Number" required name="register_card_no" id="register_card_no">    
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Card type</p>
        	<select class="clean input-name admin-input" name="register_card_type" id="register_card_type" required>


                <option value="" name=" ">Card Type</option>
            	<option value="Credit" name="Credit">Credit</option>
                <option value="Debit" name="Debit">Debit</option>

            </select>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Expiry Date</p>
        	<input class="input-name clean input-textarea admin-input" type="date" placeholder="Expiry Date" required name="register_expiry" id="register_expiry">          
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">CCV</p>
 
            	<input class="input-name clean input-password edit-password-input admin-input"  type="text" placeholder="CCV" required name="register_ccv" id="register_ccv">
                <p class="edit-p-password open-ccv"><img src="img/ccv.png" class="hover1a edit-password-img ccv-img" alt="CCV" title="CCV"><img src="img/ccv2.png" class="hover1b edit-password-img ccv-img" alt="CCV" title="CCV"></p>
                
        </div> 
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Postal Code</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Postal Code" required name="register_postal_code" id="register_postal_code">          
        </div>         
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Billing Address</p>
        	<textarea class="input-name clean address-textarea admin-address-textarea" type="text" placeholder="Billing Address" required name="register_billing_address" id="register_billing_address"></textarea>      
        </div>         -->

        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Next</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new user!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Registration of new seller failed !";
        } 
        else if($_GET['type'] == 6)
        {
            $messageType = "Registration data has been taken <br> Please re-enter a new data !";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>