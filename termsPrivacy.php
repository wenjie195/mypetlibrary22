<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();



$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Terms and Privacy | Mypetslibrary" />
<title>Terms and Privacy | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">

<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
 
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'header.php'; ?>
 
<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    // Print the link 
    // echo $link; 
?>



<div class="width100 blog-big-div overflow min-height menu-distance2">
	<div class="blog-inner-div">
    	<div class="blog-content">
				<div class="cover-div bone-bg">
                	<h1 class="white-text">Terms & Privacy</h1>
                </div>
                
                <h1 class="green-text user-title ow-margin-bottom-0">DEAR MYPETSLIBRARY’S CUSTOMER</h1>
                <!-- <p class="author-p">Author Name</p> -->

                <p class="article-paragraph">
In our effort to ensure compliance to Personal Data Protection Act 2010 (PDPA). We wish to draw your attention to Mypetslibrary's Privacy Policy as follow:<br><br>

<b>Mypetslibrary Privacy Policy</b><br><br>

<strong>1. Information We Receive or Collect from You</strong><br><br>

Registration Data: When you register, we ask you to provide certain information, which includes your email address, birth year, gender, and zip code (the "Registration Data"), as well as a password for your account.<br><br>

Payment Information: If you make any purchase or payment for our service, you will also be asked to provide (at a minimum) your name, contact number, address and credit card information ("Payment Information").<br><br>

Information You Choose to Provide to Mypetslibrary: You have the ability to provide a variety of information during your interactions with us and the Mypetslibrary Service, such as emails you may send us, polls or surveys you choose to participate in, ads you respond to, and emails or newsletters that you sign up to receive. Mypetslibrary or third parties acting on our behalf receive data from you whenever you provide us with any of this type of information.<br><br>

Community Posting: You have the ability to post comments and information in community networking features available on or accessible through the Service, such as your public profile, our blog, and our social networking pages. You should be aware that any information you submit in the course of these community activities can be read, collected, or used by other users of these parts of the Service, as further discussed below. We are not responsible for the information you choose to make public in any of the community networking features available on or through the Service.<br><br>

Technical Information: As is true of many internet-enabled services, Mypetslibrary may collect certain non-personally identifiable technical information through the use of log files and servers. Web and application servers create log files automatically as part of their setup and configuration. Information in a log file may include but not limited to IP address, browser type, Internet service provider, date/time stamps, MAC address, file requested, and other usage information and statistics.<br><br>

Contact Information: If you choose to participate in certain features and/or rewards offered through the Service, you may provide us, and we may collect, your contact information, including your name, mailing address, and phone number ("Contact Information"). The email address you provided as part of your Registration Data is not considered Contact Information for the purposes of this policy.<br><br>

Device Information: If you access and use the Service from a computer, mobile phone, tablet, automobile, or other electronic device, we may collect information about those devices. For example, our servers receive and store information about your computer and browser, including your Internet Protocol (IP) address, browser type, and other related software or hardware information. If you access the Service from a mobile phone, tablet, automobile, or other electronic device, we may collect and store information such as device type, operating system version and type, unique identifiers (such as Android Advertising ID, VIN, MAC address, and IDFA), carrier, and other related information for that device.<br><br>

Location Information: As part of registration for the Mypetslibrary Service, you agree to provide us with, and allow us to use, your zip or postal code. Additionally, we may collect and use approximated or realtime location information, such as GPS location, from devices you use to access the Mypetslibrary Service. We will provide you with notice and an opportunity to consent before we access and collect your realtime location information, and depending on your device manufacturer, you may have the ability to revoke this consent at any time.<br><br>

Third Party Personal Information: There are certain features on the Service that, should you choose to use them, will require you to provide personal information about your friends (Third Party Personal Information). If you choose to provide Third Party Personal Information in connection with such features, we may store and use the information for purposes of providing those features. By way of example, if you choose to purchase a product to the Service for a friend (give a gift feature) or share information appearing on the Service with a friend (share via email feature), we may ask for your friend’s name and email address to provide the gift (give a gift) or share the information (share via email). We may also use Third Party Personal Information for related purposes, such as to prevent fraud.<br><br><br>

<strong>2. Information We Receive or Collect from Third Parties</strong><br><br>

We may receive or collect information about you from third parties, and combine and store it on our servers with other information we may have already received or collected from you. These third parties include:<br><br>

Service partners that provide or make available advertising, features and functionality, and content on or through the Service.<br><br>

Advertising partners that collect and maintain information on their customers. These partners may provide Mypetslibrary with certain information about those customers for the purposes of serving advertisements and/or marketing offers to their customers on the Mypetslibrary Service.<br><br>

Marketing companies and data providers that create, maintain, and distribute professional marketing lists or segments, or maintain and distribute other marketing, or similar data. Governmental or quasi- or pseudo-governmental agencies or organizations that provide or make available, to the public, census and demographic data.<br><br>

Third-party social media websites, applications, or services that you use, when we allow those websites, applications, or services to interact through the Service to provide personalized services to you. In some cases, those websites, applications, or services may automatically provide us with information about you to facilitate personalization unless you use the controls available on those websites, applications, or services to opt-out of such sharing.<br><br>

People who store or post information about you on the community features of the Mypetslibrary Service, or by your enabling connectivity with another website, application, or service where friends or other Mypetslibrary store such information. For instance, your friends may store information about you in places such as their friend lists, address books, or photos on our Service or other websites, applications, or services such as social media applications with which the Mypetslibrary Service interacts.<br><br>

Mypetslibrary is not responsible for, and will assume no liability, if a business partner or other entity collects, uses, or shares any information about you in violation of its own privacy policy or any applicable laws, rules, or regulations.<br><br><br>

<strong>3. Information Collection Technologies</strong><br><br>

Our use of cookies: Mypetslibrary uses a technology that is commonly known as "cookies." A cookie is a file our server writes to your hard drive that contains an alphanumeric identifier. We use the identifier in a cookie to help us manage and report on your interaction with the Service. Through cookies, we are able to collect information that we use to improve the Service, keep count of return visits to our website or our advertisers' or partners' websites, collect and report on aggregate statistical information, authenticate your login credentials, or manage multiple instances of the Service in a single browser. We may also collect other data such as the page or site that referred you to the Service, the date and time you visited the Service, and your current IP address. The cookies we place on your hard drive are known as "first-party cookies."<br><br>

We use both session cookies and persistent cookies. A session cookie expires when you close your browser. A persistent cookie remains on your hard drive for an extended period of time. You may be able to remove persistent cookies by following directions provided in your browser's "help" section. If you do not accept first-party cookies you may still use our Service, but your ability to use some areas of our website, and the ability to stay logged in, will be limited.<br><br>

Advertisers' and other third parties' use of cookies: Advertisers and third-party advertising partners that deliver ads to you on the Service may place or recognize a unique cookie on your hard drive. These types of non-Mypetslibrary cookies are known as "third-party cookies." Advertisers and third-party advertising partners may use third-party cookies in order to collect information about you, which may include how many times you have seen their ads or whether you have interacted with an ad. They may also use third-party cookies to provide you with interest-based advertising. Most major web browsers provide users with the option to accept or reject third-party cookies. The use of third-party cookies is not covered by this privacy policy. We do not have access to or control over cookies placed by advertisers and other third parties. If you would like to review and modify your interest-based advertising settings, visit Your Advertising Choices on Mypetslibrary.<br><br>

Beacons and tracking pixels: Mypetslibrary, its third-party advertising partners, and tracking-utility partners employ a technology known as "beacons" or "tracking pixels" (each, a "Beacon"). A Beacon is a one-pixel-by-one-pixel clear image that is embedded in HTML content, and is about the size of a period at the end of a sentence. When HTML content containing a Beacon is rendered, the Beacon transmits anonymous information to a server, such as a numeric count, unique identifier, or IP address. Mypetslibrary and its partners use Beacons to help us better manage content on our Service. For example, we may place a Beacon in HTML-based emails to let us know which emails recipients have opened, or on a webpage to count the number of unique visitors to that page. The use of a Beacon may also allow us to gauge the effectiveness of certain communications and of our marketing campaigns.<br><br>

HTML5 and Flash Local Storage: Mypetslibrary and its advertising and technology partners may, in some instances, use HTML5 and/or Flash Local Storage (collectively, "Local Storage") to enhance your listening and advertising experiences on the Service. For example, we use Local Storage to keep track of the current song playing in the event your browser window reloads inadvertently, that way we can continue playing that song starting at the same position it was prior to reload. Our advertising and technology partners may use Local Storage to detect your bandwidth speeds for optimal video playback performance. Each browser implements HTML5 Local Storage differently and provides tools for managing content stored within local storage. Review your browser's privacy settings to manage the content stored within HTML5 Local Storage. Similarly, for more information on how to manage content stored with Flash Local Storage, please review the document provided by Adobe.<br><br><br>

<strong>4. How We Use Information We Receive or Collect</strong><br><br>

Generally: In general, Mypetslibrary may use your Registration Data and/or other information or data we receive or collect, as well as data we derive or infer from combinations of the foregoing, for a variety of purposes, such as:<br><br>

To facilitate the creation of and secure your account on the Mypetslibrary Service.<br><br>

To customize and personalize the advertising and other content we deliver to you both on the Service and on other services offered by our publishing partners. We use this information to provide you with relevant and interesting advertising and other content.<br><br>

To measure and analyze Service usage and enhance the Mypetslibrary experience on our Service. We use tracking information to determine how well each page and station performs overall, based on aggregate Mypetslibrary demographics and traffic patterns to those pages and stations. This helps us continue to build a better Service for you.<br><br>

To fulfill your requests for certain products and services, such as distributing electronic newsletters and enabling you to participate in and renew paid services, surveys, and public forums.<br><br>

To send you information that you agreed to receive.<br><br>

To alert you to the latest developments and features on our Service and to notify you of administrative information, such as security or support and maintenance advisories.<br><br>

To invite you to participate in events or special promotions related to artists or products we think you may like or in which you may be interested.<br><br>

Email Address: We do not sell or give your email address to other companies for their own marketing purposes without your permission. However, we may use your email address or other Registration Data to provide you with technical support, send you notices about the Service or other promotional offers you have elected to receive, and to serve you with ads that are more relevant to your interests. We may also work with data partners and advertising platforms to help increase the relevancy of ads we provide to our Mypetslibrary. In doing so, we may use information representing an encrypted or hashed value derived from information we have received, such as your email address, in connection with these partners and platforms.<br><br>

Contact Information: We do not sell or give your Contact Information to companies for their own marketing purposes without your permission. We do use Contact Information, however, to contact you, and to provide you with special offers and other information. You may at any time request that we cease using your Contact Information by contacting Mypetslibrary’s Support.<br><br><br>

<strong>5. How We Share Information We Receive or Collect with Others</strong><br><br>

How Your Information Is Shared: Mypetslibrary may share information we receive or collect in a variety of ways, such as:<br><br>

When you give Mypetslibrary permission through an affirmative election (for example, clicking "yes" in response to a message such as "share my email with this advertiser").<br><br>

When you participate in community and social networking features on the Mypetslibrary Service, such as forums, station sharing, Mypetslibrary opinions and reviews, or other forms of communication and interaction which you elect to make public or share on public profiles or in other public forums.<br><br>

If we contract or partner with third parties to provide specialized services on our behalf, such as credit card processors, customer support, ad servers or other providers of advertising services, bulk email processors who send out emails on our behalf, or parties who assist us with sweepstakes management and prize fulfillment. These companies are authorized to use your personal information only as necessary to provide these services to Mypetslibrary. They are not authorized to use your personal information for their own, unrelated purposes.<br><br>

If you elect to take advantage of certain features or services provided jointly by Mypetslibrary and third parties, we may share certain information about you with those third parties for account authentication and management, and to enable the Service on their platforms. For example, when you elect to use products, services, features, or functionality, such as Google Now or Mypetslibrary for Business, provided by third-party partners, such as Google, you agree that we may provide certain data about you to those partners in order to enable the product, service, feature, or functionality you intend to use. As another example, when you share information through integrated social networking platforms, such as Facebook, you are agreeing to allow the Mypetslibrary service to communicate or "talk" with the other service in order to make the social features available for your use. These service partners may have their own data collection, use, and sharing practices that may also be applicable to your personal information, and that are not covered by this Policy. You acknowledge and agree that you are solely responsible for your use of those third-party service providers and that it is your responsibility to review the terms of use and privacy policies of the third-party service providers and the methods they use for changing the privacy or sharing settings on such services.<br><br>

To measure and analyze Service usage. For example, we use third-party providers to measure how long and how frequently you are using the Service. Our advertising partners may use these providers to measure the effectiveness of their advertising campaigns. When we use these providers, we provide them with non-personally identifiable information about you, such as your device identifier.<br><br>

Sharing of Personally Identifiable Information: We do not share personally identifiable information with third parties other than as described in this policy. However, we may share your information, including personally identifiable information, in order to (i) protect or defend the legal rights or property of Mypetslibrary, or the legal rights of our business partners, employees, agents, and contractors (including enforcement of our agreements); (ii) protect the safety and security of Mypetslibrary users or members of the public including acting in urgent circumstances; (iii) protect against fraud or to conduct risk management; or (iv) comply with the law, legal process, or legal requests. Additionally, we may share your data, including any personally identifiable information, with our successor in interest in the event of a corporate reorganization, merger, or sale of all or substantially all of our assets.<br><br>

Sharing of Device Data: Mypetslibrary may share information from devices you use to access the Service with its third-party vendors or service providers, manufacturing or distribution partners, or advertising partners. We share this information for a variety of purposes such as mobile listening capping, advertising frequency capping, tracking advertising conversion events, estimating the number of unique users, security and fraud detection, debugging problems with the Mypetslibrary Service, and for providing you with more relevant advertisements.<br><br>

Sharing of Anonymized, Non-Personally Identifiable or Aggregated Data: Mypetslibrary may share with third parties, advertisers, and/or business partners anonymized, non-personally identifiable, or aggregated data we receive or collect, such as de-identified demographic information, de-identified location information, information about the computer or device from which you access the Service, or information about the stations, tracks, and artists you listen to, thumb-up, thumb-down, or bookmark. We share such information for a variety of reasons, such as to analyze Service usage, improve the Mypetslibrary Service and your Mypetslibrary experience, improve the serving of advertisements, or for other similar purposes. The use and disclosure of such anonymized, non-personally identifiable, or aggregated information is not subject to any restrictions under this policy.<br><br>

Information You Disclose in a Public Profile or in Public Forums: The Mypetslibrary Service offers publicly accessible and available profile pages, blogs, and pages on social media platforms. You should be aware that any information you provide or post in these areas may be read, collected, and used by others who access them. If your profile is public, any information you place in your user profile, including biographical information, the people you are following, and the people whom you allow to follow you, may be read, collected, and used by others who access them. To request removal of such information from our public forums, contact our Mypetslibrary Support team. In some cases, we may not be able to remove your information, in which case we will let you know if we are unable to do so and why. To find out more about how to make your profile private, please see the section below on Mypetslibrary Profile Visibility.<br><br>

Onward Transfer: If your personally identifiable information is transferred as described in this policy, we will seek assurances from the recipients of such personally identifiable information (prior to the transfer) that they will safeguard the personally identifiable information in a manner consistent with this policy. We ask recipients of such information to enter into a contractual relationship with us that includes confidentiality and non-disclosure clauses, and provides the same level of commitment to and protections of information as provided in this Policy.<br><br><br>

<strong>6. Advertising & Measurement Settings</strong><br><br>

Opting Out of Behavioral Advertising on the Web: In our effort to provide you with advertisements that may interest you, Mypetslibrary and its third-party advertising partners use data in the manner disclosed in this policy to provide you with relevant ads. To learn about how the online advertising industry uses information it collects to provide you with relevant ads, and to control whether you want to receive those relevant ads from third-party advertisers, please review the information at the following links:<br><br>

Your Advertising Choices on Mypetslibrary<br>

AdChoices<br>

Understanding Online Advertising (About Our Ads)<br><br>

Because we do not control the privacy practices of third-party advertising companies, you should read and understand their privacy policies. You acknowledge and agree that you are solely responsible for reviewing the privacy policies of such third-party advertising companies and the methods available, if any, for changing privacy settings and the like.<br><br>

Opting Out of Behavioral Advertising on Mobile and Tablet Devices: If you are using an iOS device and you do not want to receive tailored in-application advertisements that relate to your interests, you may be able to opt-out by accessing the iAd Network Advertising Settings page in a browser on your iOS-based device. If you are using an iOS 7 or later device, you may also be able to limit the delivery of tailored in-application advertisements by performing the following steps: Open Settings and tap "Privacy", Scroll to the bottom of Privacy and tap "Advertising"; Flip "Limit Ad Tracking" to ON. The above steps and features, although current as of the date of this policy, are not controlled by Mypetslibrary and are therefore subject to change at any time. You should contact the platform operator if the above options are no longer available.<br><br>

If you are using an Android-based device and you do not wish to receive tailored in-application advertisements, you can visit Google's Ads Settings page from a browser on your Android-based device and make your choices there. Mypetslibrary does not control how the applicable platform operator allows you to control receiving tailored in-application ads; thus, you should contact the platform operator if the above options are no longer available.<br><br>

Opting Out of Third Party Service Measurement and Analytics: As disclosed in this policy, we use third-party providers to aid us in measuring and analyzing service usage. You may opt-out of web-based participation in these measurement services by following the instructions on the following web pages for each respective company: Nielsen and Scorecard Research. To opt-out of participation on mobile and tablet devices, please follow the instructions above listed in the section for Opting Out of Behavioral Advertising on Mobile and Tablet Devices.<br><br><br>

<strong>7. Managing Your Information</strong><br><br>

Modification of Your Registration Data: We provide you with the ability to modify your Registration Data, which you may do through the Settings section of our Service or by contacting Mypetslibrary Support. For detailed instructions on how to modify your Registration Data or to otherwise access this information, visit our support site at https://www.mypetslibrary.com. We generally respond to email requests within 24 hours of receiving a request, but it may take up to 30 days depending on the volume of requests we receive.<br><br>

Retention of Your Registration Data and other Information: We will retain and use your Registration Data and the other information we collect about you for as long as your account is active or as needed to provide you Services. We will retain and use this information for the purposes for which it was collected (as specified in this policy or as Mypetslibrary discloses to its Mypetslibrary outside of this policy), including, to provide the Service to you, comply with our legal obligations, resolve disputes, and enforce our agreements.<br><br>

If you would like to delete your Registration Data, or if you would like more information on cancelling or deactivating your account, see the section below on Cancellation or Deactivation of Accounts.<br><br>

Your Email Preferences: When you register for the Service, you may elect to receive promotional, marketing, or other similar emails tailored to your interests. You have the option to change this election in the Settings section of our Service. For information on how to opt in or out of receiving promotional, marketing, or other similar emails from us, visit https://www.mypetslibrary.com.<br><br>

Additionally, you may also follow the unsubscribe instructions contained in the promotional, marketing, or other similar emails you receive.<br><br>

We will send you transaction confirmation emails and other Service-related announcements when it is necessary to do so. For instance, if our Service is temporarily suspended for maintenance, we might send you an email. Generally, you may not opt-out of these communications, which are not promotional in nature. If you do not wish to receive them, you have the option to deactivate your account.<br><br>

Cancellation or Deactivation of Accounts: If you would like to request the cancellation or deactivation of your account, you should contact our Mypetslibrary Support team for assistance. Cancellation or deactivation of your account does not ensure complete or comprehensive removal of the content or information you may have posted or otherwise made available publicly on the Service while you were a registered user. You should also contact our Mypetslibrary Support team to request the deactivation of a profile you believe is fake or otherwise unauthorized.<br><br>

Cached Profiles on Search Engines: Please note that although we may deactivate your account or make your profile private at your request, Internet search engines such as Google and Bing cache publicly available webpages for a period of time beyond the control of Mypetslibrary, and may make your deactivated or formerly public profile available to users of their services on their platforms until such time as they refresh their webpage cache. Please consult with the applicable search engine to determine how you may remove webpages from their webpage cache.<br><br><br>

<strong>8. Security and Content from Other Websites</strong><br><br>

Protection of Data from Loss: We have implemented security measures designed to protect against the loss, misuse, and alteration of the information we collect or receive from you. For example, when you enter sensitive information (such as a credit card number) on our order forms, we encrypt the transmission of that information using secure socket layer technology (SSL). However, despite our efforts, no security measures are perfect or impenetrable. If you have any questions about security on our Service, you can contact our Mypetslibrary Support team.<br><br>

Data Integrity: We will use personally identifiable information, if any, that we collect and receive from you in ways that are relevant and compatible with the purpose for which that information was collected or provided to us as more fully disclosed in this policy. We will take steps to ensure that all personally identifiable information collected, processed, and/or stored is protected from destruction, corruption, or use in a manner inconsistent with the purpose for which we received it.<br><br>

Links to Other Websites: Our Service and certain advertisements on our Service include links to other websites whose privacy practices may differ from those of Mypetslibrary. If you submit personal information to any of those websites, the privacy statements and practices of those websites govern their use of your information. We encourage you to carefully read the privacy statement of any website you visit.<br><br>

Use of Framing Techniques: Some of our third-party partners may utilize framing techniques to serve content to and from webpages accessible through our Service while preserving the look and feel of our website. Please be aware that if a third-party partner utilizes framing techniques, you are providing your personal information to this third-party partner and not to Mypetslibrary.<br><br><br>

<strong>9. Our Policies Concerning Children</strong><br><br>

Mypetslibrary prohibits registration by, and does not knowingly collect personal information from, anyone under 13 years of age. In the event we obtain actual knowledge that we have collected information from children under the age of 13, we will take measures to remove such information from our servers. If you believe that we might have any personal information from a child under 13, please contact our Mypetslibrary Support team.<br><br><br>

<strong>10. Transfer of Information of International Mypetslibrary(s)</strong><br><br>

Please note that the information you submit to us may be transferred to other countries to be processed by us or our service providers in order to provide the Service to you or for such other purposes as set forth in this policy. If you are not a resident of Malaysia, you hereby consent and agree that we may collect, process, use, and store your information, as discussed in this policy, outside your resident jurisdiction, including in Malaysia. Please be aware that Malaysia law and the laws of other countries where we may store and process your information may offer different levels of protection for information than may be available in your country.<br><br><br>

<strong>11. Changes to our Privacy Policy</strong><br><br>

We will continue to evaluate this policy against new technologies, business practices, Safe Harbor requirements, and our Mypetslibrary' needs, and may make changes to the policy accordingly. Please check this page periodically for updates. If we make any material changes to this policy, we will post the updated terms of the policy on the Service, and provide you notice of such chances, which may include notice by email through a message sent to the email address you use to access the Service, or posting a message on the Service.<br><br>

Any material changes to this policy will be effective upon the earlier of thirty (30) calendar days following our dispatch of an email notice to you or thirty (30) calendar days following our posting of notice of the changes on the Service. These changes will be effective immediately for new users of the Service. Please note that at all times you are responsible for updating your information to provide us with your most current email address. In the event that the last email address that you have provided us is not valid, or for any reason is not capable of delivering to you the notice described above, our dispatch of the email containing such notice will nonetheless constitute effective notice of the changes described in the notice. If you do not wish to permit changes in our use of your information, you must notify us prior to the effective date of the changes that you wish to deactivate your account. Continued use of the Service following notice of such changes shall indicate your acknowledgement of such changes and agreement to be bound by the terms and conditions of such changes.<br><br><br>

<strong>12. Governance</strong><br><br>

Dispute Resolution: Questions or concerns regarding our use or disclosure of information may be directed to our Mypetslibrary Support team. Mypetslibrary will investigate and attempt to resolve complaints and disputes regarding use and disclosure of information in accordance with the principles contained in this policy. For complaints that cannot be resolved between Mypetslibrary and the complainant, Mypetslibrary agrees to cooperate with the respective data protection authorities located in the applicable country (or their authorized representatives) and participate in any dispute resolution procedures established by such authorities.<br><br><br>

<strong>13. Contact Us</strong><br><br>

If you have any questions about this privacy policy, or the privacy practices of Mypetslibrary, contact our Mypetslibrary Support team for more information at<a href="https://mypetslibrary.com/" target="_blank">www.mypetslibrary.com</a>.                    
                </p>
              
		</div>

                      
	</div>
</div>


<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>