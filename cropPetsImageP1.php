<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];
$newPetsUid = $_SESSION['newpets_uid'];
$newPetsType = $_SESSION['newpets_type'];

// $var_value = $_REQUEST['varname'];

$conn = connDB();

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
// 	$newUserUid = $registerUid;
// }

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($newUserUid),"s");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Upload Pets Image | Mypetslibrary" />
<title>Upload Pets Image | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->

<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="croppie.js"></script>
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<!-- <div class="width100 same-padding overflow min-height menu-distance2"> -->

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">

	<p class="review-product-name text-center ow-margin-top20">Upload Pets Image 1</p>

		<div class="width100 overflow text-center">
			<input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
			<br />
			<div id="uploaded_image" class="pet-photo-preview"></div>
        </div>

        <div class="clear"></div>
        
</div>
	
<div class="clear"></div>
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>
<div class="width100 same-padding green-footer">
	<p class="footer-p white-text">© <?php echo $time;?> Mypetslibrary, All Rights Reserved.</p>
</div>

<div id="uploadimageModal" class="modal update-profile-div" role="dialog">
	<div class="modal-dialog update-profile-second-div">
		<div class="modal-content update-profile-third-div">
			<div class="modal-header update-profile-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Upload Pets Image 1</h4>
			</div>
			<div class="modal-body update-profile-body">

						<div id="image_demo" class="profile-pic-crop"></div>
                        <div class="clear"></div>
                        <div class="width100 overflow text-center">
							<button class="green-button mid-btn-width clean btn btn-success crop_image">Crop Image</button>
                        </div>


			</div>
		</div>
	</div>
</div>
  
<script>
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
		enableExif: true,
		viewport:
		{
			width:'default',
			height:'default',
			type:'square' //circle
		},
		boundary:
		{
		width:'default',
		height:'default'
		}
	});

	$('#image_one').on('change', function(){
		var reader = new FileReader();
		reader.onload = function (event){
			$image_crop.croppie('bind',{
				url: event.target.result
			}).then(function(){
			console.log('jQuery bind complete');
			});
		}
		reader.readAsDataURL(this.files[0]);
		$('#uploadimageModal').modal('show');
	});

	$('.crop_image').click(function(event){
		$image_crop.croppie('result',{
			type: 'canvas',
			size: 'original'
		}).then(function(response){
		$.ajax({

		url:"cropPetsImageP1Function.php",

        type: "POST",
        data:{"image": response},
		success:function(data)
		{
			$('#uploadimageModal').modal('hide');
            $('#uploaded_image').html(data);
		}
		});
		})
	});

});
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>

</body>
</html>