<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$pendingPets = getPetsDetails($conn," WHERE status = 'Rejected' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Rejected Pets Details| Mypetslibrary" />
<title>Rejected Pets Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="dual-left-div">
            <h1 class="green-text h1-title">Rejected Pets Details | <a href="pendingPets.php" class="green-a">Pending</a></h1>
            <div class="green-border"></div>
        </div>
        <div class="dual-search-div">
        	<form>
            <input class="line-input clean" type="text" placeholder="Search" id="myInput" onkeyup="myFunction()">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a dual-search" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b dual-search" alt="Search" title="Search">
                </button>
            </form>
        </div>

        
    </div>
    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100" id="myTable">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Pet Type</th>
                    <!--<th>Pet Name</th>-->
                    <th>SKU</th>
                    <th>Added On</th>
                    <th>Status</th>
                    <th>Details</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    if($pendingPets)
                    {
                        for($cnt = 0;$cnt < count($pendingPets) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $pendingPets[$cnt]->getType();?></td>
                                <!--<td><?php echo $pendingPets[$cnt]->getName();?></td>-->
                                <td><?php echo $pendingPets[$cnt]->getSku();?></td>
                                <td>
                                    <!-- <?php //echo $pendingPets[$cnt]->getDateCreated();?> -->
                                    <?php echo $date = date("d-m-Y",strtotime($pendingPets[$cnt]->getDateCreated()));?>
                                </td>
                                <td><?php echo $pendingPets[$cnt]->getStatus();?></td>
          
                                <td>
                                    <form method="POST" action="pendingPetDetails.php" class="hover1">
                                    <!-- <form method="POST" action="#" class="hover1"> -->
                                        <button class="clean hover1 img-btn" type="submit" name="pets_uid" value="<?php echo $pendingPets[$cnt]->getUid();?>">
                                            <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>
                                
                            </tr>
                        <?php
                        }
                    }
                ?>    
            </tbody>

            <!-- <tbody>
            	<tr>
                	<td class="first-column">1.</td>
                    <td>Puppy</td>
                    <td>Husky</td>
                    <td>FF-A-01</td>
                    <td>1/12/2019</td>
                    <td>Rejected</td>
                    <td>
                    	<a href="pendingPetDetails.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Approval" title="Approval">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Approval" title="Approval">
                        </a>
                    </td>
                </tr>
            	<tr>
                	<td class="first-column">2.</td>
                    <td>Puppy</td>
                    <td>Husky</td>
                    <td>FF-A-01</td>
                    <td>1/12/2019</td>
                    <td>Rejected</td>
                    <td>
                    	<a href="pendingPetDetails.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Approval" title="Approval">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Approval" title="Approval">
                        </a>
                    </td>
                </tr> 
            	<tr>
                	<td class="first-column">3.</td>
                    <td>Puppy</td>
                    <td>Husky</td>
                    <td>FF-A-01</td>
                    <td>1/12/2019</td>
                    <td>Rejected</td>
                    <td>
                    	<a href="pendingPetDetails.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Approval" title="Approval">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Approval" title="Approval">
                        </a>
                    </td>
                </tr>                               
            </tbody> -->
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>