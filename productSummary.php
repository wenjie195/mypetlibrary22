<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productAmount = getProduct($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Product Summary | Mypetslibrary" />
<title>Product Summary | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Product Summary</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="allProducts.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/pet-food.png" alt="All Products" title="All Products" class="four-div-img">
                <p class="four-div-p">All Products</p>
                <?php
                if($productAmount)
                {   
                    $totalProduct = count($productAmount);
                }
                else
                {   $totalProduct = 0;   }
                ?>
                <!-- <p class="four-div-amount-p"><b>1000</b></p> -->
                <p class="four-div-amount-p"><b><?php echo $totalProduct;?></b></p>
            </div>
        </a>
        <a href="shippingRequest.php"  class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/delivery.png" alt="Pending" title="Pending" class="four-div-img">
                <p class="four-div-p">Pending</p>
                <p class="four-div-amount-p"><b>250</b></p>
            </div> 
        </a>
        <!--
        <a  href="allSales.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/income.png" alt="Product Sales (RM)" title="Product Sales (RM)" class="four-div-img">
                <p class="four-div-p">Product Sales (RM)</p>
                <p class="four-div-amount-p"><b>300,00.00</b></p>
            </div>  
        </a>-->
        <a href="addProduct.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/dog-food.png" alt="Add Product" title="Add Product" class="four-div-img">
                <p class="four-div-p mob-four-p">Add Product</p>
                <p class="four-div-amount-p"><b>&nbsp;</b></p>
            </div>  
        </a>
        <a href="category.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/pet-shampoo.png" alt="Edit Category" title="Edit Category" class="four-div-img">
                <p class="four-div-p mob-four-p">Edit Category</p>
                <p class="four-div-amount-p"><b>&nbsp;</b></p>
            </div> 
        </a> 
        <a href="brand.php"  class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/cute-puppy.png" alt="Edit Brand" title="Edit Brand" class="four-div-img">
                <p class="four-div-p">Edit Brand</p>
                <p class="four-div-amount-p"><b>&nbsp;</b></p>
            </div> 
        </a>                     
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>



<?php include 'js.php'; ?>
</body>
</html>