<div class="width103" id="app">  
<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';

require_once dirname(__FILE__) . '/classes/Services.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();
$serviceName = [];
if(isset($_POST["action"]))
{
	$query = "
		SELECT * FROM seller WHERE account_status = 'Active'
	";

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row_count = $statement->rowCount();

	if(isset($_POST["state"]))
	{
		$state_filter = implode("','", $_POST["state"]);
		$query .= "
		 AND state IN('".$state_filter."')
		";
	}
	if(isset($_POST["services"]))
	{
		$services_filter = implode(",", $_POST["services"]);
		$servicesFilterExp = explode(",",$services_filter);
		$lastServiceNumber = count($servicesFilterExp); // ex: 5
		$lastServiceNumberArr = $lastServiceNumber - 1;
		 // if (count($servicesFilterExp) > 1) {
		for ($i=0; $i <count($servicesFilterExp) ; $i++) {
			if (count($servicesFilterExp) == 1) {
				$query .= "
				 AND (services LIKE('%,".$servicesFilterExp[$i].",%') OR services LIKE('".$servicesFilterExp[$i]."') OR services LIKE('".$servicesFilterExp[$i].",%') OR services LIKE('%,".$servicesFilterExp[$i]."'))
				";
			}else{
				if ($i == 0) {
					$query .= "
					 AND (services LIKE('%,".$servicesFilterExp[$i].",%') OR services LIKE('".$servicesFilterExp[$i]."') OR services LIKE('".$servicesFilterExp[$i].",%') OR services LIKE('%,".$servicesFilterExp[$i]."')
					";
				}else
				if($lastServiceNumberArr == $i){
					$query .= "
					 OR services LIKE('".$servicesFilterExp[$i]."') OR services LIKE('%,".$servicesFilterExp[$i].",%') OR services LIKE('".$servicesFilterExp[$i].",%') OR services LIKE('%,".$servicesFilterExp[$i]."'))
					";
				}else {
					{
						$query .= "
						 OR services LIKE('".$servicesFilterExp[$i]."') OR services LIKE('%,".$servicesFilterExp[$i].",%') OR services LIKE('".$servicesFilterExp[$i].",%') OR services LIKE('%,".$servicesFilterExp[$i]."')
						";
					}
				}
			}

		}
	// }else{
	// 	$query .= "
	// 	 AND (services LIKE('".$services_filter."') OR services LIKE('%,".$services_filter.",%') OR services LIKE('".$services_filter.",%') OR services LIKE('%,".$services_filter."'))";
	// }
		// $query .= "
		//  AND (services LIKE('".$services_filter."') OR services LIKE('%,".$services_filter.",%') OR services LIKE('".$services_filter.",%') OR services LIKE('%,".$services_filter."'))
		// ";
		// AND services FIND_IN_SET('".$services_filter."')
	}

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row = $statement->rowCount();
    $output = '';

	if($total_row > 0)
	{
		foreach($result as $row)
		{
			$uid= $row['uid'];
			$id = $row['services'];
			$idExp = explode(",",$id);
			unset($serviceName);
			for ($i=0; $i <count($idExp) ; $i++) {

				$serviceDetails = getServices($conn, "WHERE id =?",array("id"),array($idExp[$i]), "s");
				if ($serviceDetails) {
					$serviceName[] = $serviceDetails[0]->getServiceType();
				}
			}
			if (isset($serviceName)) {
				$serviceNameImp = implode(",",$serviceName);
			}else {
				$serviceNameImp = '-';
			}

							$imageURL = './uploads/'.$row["company_logo"];

							if ($row['account_status'] == 'Active'){
								$output .= '
									<a href="petSellerDetails.php">
										<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['slug'].'">
											<div class="width100 white-bg  progressive">
											<a  class="progressive replace">
													<img  data-src="uploads/'. $row['company_logo'] .'" src="img/pet-load300.jpg" class="preview width100 two-border-radius lazy" alt="<?php echo '. $row['company_logo'] .'" title="<?php echo '. $row['company_logo'] .';?>" />
											</a>
											</div>
											<p align="center" class="width100 text-overflow slider-product-name">'. $row['company_name'] .'</p>
											<p align="center" class="width100 text-overflow slider-product-name">'. $serviceNameImp .'</p>

										</div>
									</a>
								';

							}
		}
	}
	else
	{
		$output = '<h3>No Data Found</h3>';
	}
	echo $output;
}

?>
</div>
<script type="text/javascript">

var totalId = <?php echo $total_row_count + 1 ?>;
// alert(totalId);
for(var i=0; i< totalId; i++){

	$("#"+i+"").click(function(){
					var x = $(this).attr('value');
					location.href = "./petSellerDetails.php?id="+x+"";
				});

}
</script>
  <script src="js/index2.js"></script>
  <script>
    (function(){
      new Progressive({
        el: '#app',
        lazyClass: 'lazy',
        removePreview: true,
        scale: true
      }).fire()
    })()
  </script>