<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$productsOrders =  getOrders($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $asd = $productsOrders[0];

// $aaaaa = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($productsOrders->$_SESSION['uid']),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pedigree Dentastix Dog Treats | Mypetslibrary" />
<title>Pedigree Dentastix Dog Treats | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet.
Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet. Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Purchase History</h1>
    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Oldest Order</option>
        <option class="filter-option">Latest Order</option>
        <option class="filter-option">Highest Price</option>
        <option class="filter-option">Lowest Price</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <!-- <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>ORDER NUMBER</th>
                        <th>ORDER DATE</th>
                        <th>PRICE (RM)</th>

                    </tr>
                </thead>

                <tbody>

                </tbody>

            </table>
        </div>
    </div> -->

    <div class="clear"></div>

    <?php
    if($productsOrders == "")
    {
    ?>

        <h3 class="profile-title">You Purchase History Is Empty</h3>

    <?php
    }
    else
    { ?>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>ORDER NUMBER</th>
                        <th>ORDER DATE</th>
                        <!-- <th>PRODUCT</th> -->
                        <th>PRICE (RM)</th>
                        <th>PAYMENT STATUS</th>
                        <th>SHIPPING STATUS</th>
                        <!-- <th>ACTION</th> -->
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $conn = connDB();

                    // if($productsOrders)
                    // {
                    //     for($a = 1;$a<count($productsOrders);$a++)

                    // if($productsOrders) 
                    // {
                        for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                        // {   
                        //     $orderDetails = getOrders($conn," WHERE id = ?  ",array("id"),array($productsOrders[$cnt]->getOrderId()),"s");
                        //     $orderDetails = getOrders($conn);
                        //     if($orderDetails != null)
                        //     {
                        //         for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                                {?>
                                        <tr>
                                            <td><?php echo ($cnt+1)?></td>
                                            <td>#<?php echo $productsOrders[$cnt]->getId();?></td>

                                            <td><?php $dateCreated = date("Y-m-d",strtotime($productsOrders[$cnt]->getDateCreated()));
                                                    echo $dateCreated;?>
                                            </td>
                                            <td><?php echo $productsOrders[$cnt]->getSubtotal();?></td>
                                            <td><?php echo $productsOrders[$cnt]->getPaymentStatus();?></td>
                                            <td><?php echo $productsOrders[$cnt]->getShippingStatus();?></td>
                                            </tr>
                                <?php
                                }
                    //         } 
                        
                    //    }
                    // } 
                    $conn->close();
                    // }
                    ?>
                </tbody>

            </table>
        </div>
    </div>

    <div class="clear"></div>

    <?php
    }?>

    <!-- <div class="bottom-big-container">
    	<div class="left-btm-page">
        	Page <select class="clean transparent-select"><option>1</option></select> of 1
        </div>
        <div class="middle-btm-page">
        	<a class="round-black-page">1</a>
            <a class="round-white-page">2</a>
        </div>
        <div class="right-btm-page">
        	Total: 2
        </div>
    </div> -->

</div>
<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Payment Submit Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Submit Payment, Please Try Again";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Error !";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "Error !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})
</script>
</body>
</html>
