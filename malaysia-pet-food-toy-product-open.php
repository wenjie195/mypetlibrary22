<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation1.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productDetails = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
//$productDetails = getProduct($conn);
$variationDetails = getVariation($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Food, Toys and Etc | Mypetslibrary" />
<title>Pet Food, Toys and Etc | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
      <div class="fix-filter width100 small-padding overflow">
            <h1 class="green-text user-title left-align-title">Products</h1>
            <div class="filter-div">
            	<a class="open-filter2 filter-a green-a">Filter</a>
            </div>
      </div>
<!--
<div class="fix-filter width100 same-padding overflow product-filter-white-div">
        <h1 class="green-text user-title left-align-title">Product</h1>
        <a class="open-productfilter filter-a green-a product-filter-a">Filter</a>

        <div class="filter-div product-filter-div">
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Favourite</button>
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Top Sale</button>
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Free Gift</button> 
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Price</button>
            
        </div> 
</div>-->

<div class="clear"></div>

<div class="width100 small-padding overflow min-height-with-filter filter-distance">
    <div class="width103" id="app">
        <?php
        $conn = connDB();
        $max =0;
        $min =0;
        if($productDetails && $variationDetails)
        {
            for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
            {
                $min_count=0;
                for($count = 0;$count < count($variationDetails) ;$count++)
                {
                    if($productDetails[$cnt]->getUid() == $variationDetails[$count]->getProductId())
                    {
                        if( $max < $variationDetails[$count]->getVariationPrice()){
                            $max = $variationDetails[$count]->getVariationPrice() ;
                        }else{
                            $max = $max ;
                        }
                        
                        if($min_count == 0){
                            $min = $variationDetails[$count]->getVariationPrice();
                        }
                        else{
                            if($min < $variationDetails[$count]->getVariationPrice()){
                                $min = $min ;
                            }else{
                                $min = $variationDetails[$count]->getVariationPrice() ;
                            }

                        }
                        $min_count++;
                    }  
                }
            ?>

                <a href='productDetails-open.php?id=<?php echo $productDetails[$cnt]->getUid();?>'  class="opacity-hover pointer">
                    <!-- <div class="shadow-white-box featured four-box-size">
                    <div class="width100 white-bg"> -->

                    <div class="shadow-white-box four-box-size ow-product-big-div opacity-hover">
                    	<div class="square">
                        <div class="width100 white-bg content progressive">
                            <!-- <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius"> -->
                            <img data-src="uploads/<?php echo $productDetails[$cnt]->getImageOne();?>" src="img/pet-load300.jpg" alt="<?php echo $productDetails[$cnt]->getName();?>" title="<?php echo $productDetails[$cnt]->getName();?>" class="preview width100 two-border-radius opacity-hover pointer lazy">
                        </div>
                        </div>
                        
                            <p class="width100 text-overflow slider-product-name"><?php echo $productDetails[$cnt]->getName();?></p>
                            <p class="width100 text-overflow slider-product-name">
                                <?php if( $max == $min){
                                   ?> 
                                RM<?php echo $max;?>
                                <?php
                                }
                                else{
                                    ?>
                                    RM<?php echo $min;?> - RM<?php echo $max;?>
                                <?php
                                }
                                ?>
                            </p>
                            <!-- <p class="slider-product-price right-like hover1">
                                <img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                                <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                            </p> -->
                            <!--
                            <div class="clear"></div>
                            <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                            <p class="right-sold">
                                <!-- <//?php echo $productDetails[$cnt]->getVariationOneStock();?> -->
                            <!--    <span class="sold-color"> Available</span>
                            </p>
                            <div class="clear"></div>-->
                        </div>
                   
                </a>

            <?php
            $max =0;
            $min =0;
            }
            ?>
        <?php
        }
        $conn->close();
        ?>

    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.product-a .hover1a{
		display:none !important;}
	.product-a .hover1b{
		display:inline-block !important;}	
</style>

<!-- <//?php include 'js.php'; ?> -->
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
  <script src="js/index2.js"></script>
  <script>
    (function(){
      new Progressive({
        el: '#app',
        lazyClass: 'lazy',
        removePreview: true,
        scale: true
      }).fire()
    })()
  </script>

</body>
</html>