<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Checkout.php';
require_once dirname(__FILE__) . '/classes/Orders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();


if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $id = rewrite($_POST["order_id"]);
    // $username = rewrite($_POST["insert_username"]);
    // $bankName = rewrite($_POST["insert_bankname"]);
    // $bankAccountHolder = rewrite($_POST["insert_bankaccountholder"]);
    // $bankAccountNo = rewrite($_POST["insert_bankaccountnumber"]);
    $name = rewrite($_POST["insert_name"]);
    $contactNo = rewrite($_POST["insert_contactNo"]);
    $address_1 = rewrite($_POST["insert_address_1"]);
    $address_2 = rewrite($_POST["insert_address_2"]);
    $city = rewrite($_POST["insert_city"]);
    $zipcode = rewrite($_POST["insert_zipcode"]);
    $state = rewrite($_POST["insert_state"]);
    $country = rewrite($_POST["insert_country"]);
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Check Out | Mypetslibrary" />
<title>Check Out | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<!--
    <div class="sticky-tab menu-distance2">
        <div class="tab sticky-tab-tab">
            <button class="tablinks active hover1 tab-tab-btn" onclick="openTab(event, 'Cart')">
                <div class="green-dot"></div>
                <img src="img/cart-1.png" class="tab-icon hover1a" alt="Cart" title="Cart">
                <img src="img/cart-2.png" class="tab-icon hover1b" alt="Cart" title="Cart">
                <p class="tab-tab-p">In Cart</p>
                
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Ship')">
                <div class="green-dot"></div>
                <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
                <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
                <p class="tab-tab-p">To Ship</p>
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Receive')">
                <div class="green-dot"></div>
                <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
                <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
                <p class="tab-tab-p">To Receive</p>
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Received')">
                <div class="green-dot"></div>
                <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
                <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
                <p class="tab-tab-p">Received</p>
            </button>        
        </div>
    </div>-->

    <div class="two-menu-space width100"></div>    
        <div class="width100 same-padding min-height4 adjust-padding ow-checkout">
	        <div  id="Cart" class="tabcontent block same-padding">
        
                    <form method="POST"  action="shipping.php"  enctype="multipart/form-data">
                    <p class="review-product-name">Deliver to</p>
                        <div class="width100 overflow border-bottom"> 
                        <?php
                        if($userDetails)
                        {
                            $conn = connDB();
                            //$orders = getUser($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
                            $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
                        ?>                  
                            <div class="dual-input">
                                <p class="input-top-p">Name</p>
                                <input class="input-name clean" type="text" id="insert_name" name="insert_name" placeholder="Name" value="<?php echo $userRows[0]->getReceiverName();?>" required>
                            </div>
                
                            <div class="dual-input second-dual-input">
                                <p class="input-top-p">Phone No.</p>
                                <input class="input-name clean" type="text" id="insert_contactNo" name="insert_contactNo" placeholder="Phone No." value="<?php echo $userRows[0]->getReceiverContactNo();?>" required> 
                            </div>
                        
                            <div class="clear"></div>
                            <div class="dual-input">
                                <p class="input-top-p">Address Line 1</p>
                                <input class="input-name clean" type="text" id="insert_address_1" name="insert_address_1" placeholder="Address Line 1" value="<?php echo $userRows[0]->getShippingAddress();?>" required>   
                            </div>
                
                            <div class="dual-input second-dual-input">
                                <p class="input-top-p">Address Line 2</p>
                                <input class="input-name clean" type="text" id="insert_address_2" name="insert_address_2"  placeholder="Address Line 2"> 
                            </div>
                        
                            <div class="clear"></div>
                            <div class="dual-input">
                                <p class="input-top-p">City</p>
                                <input class="input-name clean" type="text" id="insert_city" name="insert_city" placeholder="City" value="<?php echo $userRows[0]->getShippingArea();?>" required>   
                            </div>
                
                            <div class="dual-input second-dual-input">
                                <p class="input-top-p">Postcode</p>
                                <input class="input-name clean"  type="number" id="insert_zipcode" name="insert_zipcode"  placeholder="Postcode" value="<?php echo $userRows[0]->getShippingPostalCode();?>" required> 
                            </div>
                        
                            <div class="clear"></div> 
                            <div class="dual-input">
                                <p class="input-top-p">State</p>
                                <input class="input-name clean" type="text" id="insert_state" name="insert_state" placeholder="State" value="<?php echo $userRows[0]->getShippingState();?>" required>   
                            </div>
                
                            <div class="dual-input second-dual-input">
                                <p class="input-top-p">Country</p>
                                <input class="input-name clean"  type="text" id="insert_country" name="insert_country" placeholder="Country" value="<?php echo $userRows[0]->getCountry();?>" required> 
                            </div>
                        
                            <div class="clear"></div> 
        
                                <div class="dual-input">
                                    <input type="hidden" id="uid" name="uid" value="<?php echo $orderUid ?>">
                                </div>    
                                <div class="clear"></div>  
                            </div>
                            <?php
                        }
                    ?> 
    
                        <div class="right-status-div">        
                            <?php echo $productListHtml; ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php 
        if(isset($_GET['type']))
        {
            $messageType = null;

            if($_SESSION['messageType'] == 1)
            {
                if($_GET['type'] == 1)
                {
                    $messageType = "Please Fill Up The Required Details !";
                }
                if($_GET['type'] == 2)
                {
                    $messageType = "Fail To Make Order !";
                }

                echo '
                <script>
                    putNoticeJavascript("Notice !! ","'.$messageType.'");
                </script>
                ';   
                $_SESSION['messageType'] = 0;
            }
        }
    ?>
<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
</body>
</html>