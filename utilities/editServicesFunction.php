<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Services.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $serviceName = rewrite($_POST["update_services"]);
    $id = rewrite($_POST["services_id"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $serviceName."<br>";
    // echo $id."<br>";

    // $allServices = getServices($conn," WHERE service = ? AND id = ? ",array("service","id"),array($serviceName,$id),"si");
    $allServices = getServices($conn," WHERE service = ? AND type = '1' ",array("service"),array($serviceName),"s");
    $existingServices = $allServices[0];

    $services = getServices($conn," id = ?  ",array("id"),array($id),"i");    

    if(!$existingServices)
    {
        if(!$services)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($serviceName)
            {
                array_push($tableName,"service");
                array_push($tableValue,$serviceName);
                $stringType .=  "s";
            }
            array_push($tableValue,$id);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"services"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                $_SESSION['messageType'] = 2;
                header('Location: ../partnerServices.php?type=1');
            }
            else
            {
                $_SESSION['messageType'] = 2;
                header('Location: ../partnerServices.php?type=2');
            }
        }
        else
        {
            $_SESSION['messageType'] = 2;
            header('Location: ../partnerServices.php?type=3');
        }
    }
    else
    {
        $_SESSION['messageType'] = 2;
        header('Location: ../partnerServices.php?type=4');
    }
}
else 
{
    header('Location: ../index.php');
}
?>
