<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Seller.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$uid = $_POST['pic'];
// $uid = 'fdf5a4b4889f888d34a2838a42dce7cd';
$successDelete = [];

$pic = null;

$sellerDetails = getSeller($conn, "WHERE uid=?",array("uid"),array($uid), "s");

if ($sellerDetails)
{
  $tableName = array();
  $tableValue =  array();
  $stringType =  "";
  //echo "save to database";
  if(!$pic || $pic)
  {
    array_push($tableName,"company_pic");
    array_push($tableValue,$pic);
    $stringType .=  "s";
  }
  array_push($tableValue,$uid);
  $stringType .=  "s";
  $approvedPets = updateDynamicData($conn,"seller"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
  if($approvedPets)
  {
    $success = 1;
    // $message = "Success";
    $message = "Picture Deleted !!";
  }
  else
  {
    $success = 0;
    // $message = "Fail";
    $message = "Fail to delete picture !!";
  }
}

$successDelete[] = array("picDelete" => $message);

echo json_encode($successDelete);

?>