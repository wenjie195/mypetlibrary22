<?php
require_once dirname(__FILE__) . '/../sellerAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $title = rewrite($_POST['update_title']);
     $articleLink = rewrite($_POST['article_link']);

     $str = str_replace( array(' ',','), '', $title);
     $strkeywordOne = str_replace( array(','), '', $articleLink);
     $AddToEnd = "-";
     $seoTitle = $str.$AddToEnd.$strkeywordOne;

     $keywordTwo = rewrite($_POST['article_keyword']);

     $newPic = $_FILES['edit_cover_photo']['name'];
     if($newPic != "")
     {
          $titleCover = $timestamp.$_FILES['edit_cover_photo']['name'];
          $target_dir = "../uploadsArticle/";
          $target_file = $target_dir . basename($_FILES["edit_cover_photo"]["name"]);
          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");
          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['edit_cover_photo']['tmp_name'],$target_dir.$titleCover);
          }
     }
     else
     {    
          $titleCover = rewrite($_POST['article_oldpic']);
     }

     $imgCoverSrc = rewrite($_POST['article_photo_source']);
     $keywordOne = ($_POST['article_description']);
     $paragraphOne = ($_POST['editor']);  //no rewrite, cause error in db

     $uid = rewrite($_POST['article_uid']);

     $articleData = getArticles($conn," uid = ?   ",array("uid"),array($uid),"s");   

     $articleStatus = "Pending";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";
     // echo $paragraphOne."<br>";
     // echo $type."<br>";

     if(!$articleData)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($articleLink)
          {
               array_push($tableName,"article_link");
               array_push($tableValue,$articleLink);
               $stringType .=  "s";
          }
          if($seoTitle)
          {
               array_push($tableName,"seo_title");
               array_push($tableValue,$seoTitle);
               $stringType .=  "s";
          }
          if($keywordTwo)
          {
               array_push($tableName,"keyword_two");
               array_push($tableValue,$keywordTwo);
               $stringType .=  "s";
          }
          if($titleCover)
          {
               array_push($tableName,"title_cover");
               array_push($tableValue,$titleCover);
               $stringType .=  "s";
          }
          if($imgCoverSrc)
          {
               array_push($tableName,"img_cover_source");
               array_push($tableValue,$imgCoverSrc);
               $stringType .=  "s";
          }
          if($keywordOne)
          {
               array_push($tableName,"keyword_one");
               array_push($tableValue,$keywordOne);
               $stringType .=  "s";
          }
          if($paragraphOne)
          {
               array_push($tableName,"paragraph_one");
               array_push($tableValue,$paragraphOne);
               $stringType .=  "s";
          }

          if($articleStatus)
          {
               array_push($tableName,"display");
               array_push($tableValue,$articleStatus);
               $stringType .=  "s";
          }

          array_push($tableValue,$uid);
          $stringType .=  "s";
          $articleUpdated = updateDynamicData($conn,"articles"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($articleUpdated)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../sellerPendingArticle.php?type=1');
          }
          else
          {
               // echo "gg";
               $_SESSION['messageType'] = 1;
               header('Location: ../sellerPendingArticle.php?type=2');
          }
     }
     else
     {         
          // echo "error";
          $_SESSION['messageType'] = 1;
          header('Location: ../sellerPendingArticle.php?type=3');
     }
}
else 
{
     header('Location: ../index.php');
}
?>