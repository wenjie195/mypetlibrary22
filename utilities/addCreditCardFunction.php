<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userID = $_SESSION['uid'];

function addCreditCard($conn,$uid,$username,$nameOnCard,$cardNo,$cardType,$ccv,$expiryDate,$billingAddress,$postalCode,$status)
{
     if(insertDynamicData($conn,"credit_card",array("uid","username","name_on_card","card_no","card_type","ccv","expiry_date","billing_address","postal_code","status"),
          array($uid,$username,$nameOnCard,$cardNo,$cardType,$ccv,$expiryDate,$billingAddress,$postalCode,$status),"sssisissis") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = $userID;

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userID),"s");

     $username = $userDetails[0]->getName();

     $nameOnCard = rewrite($_POST['card_name']);
     $cardNo = rewrite($_POST['card_number']);
     $cardType = rewrite($_POST['card_type']);
     $ccv = rewrite($_POST['card_ccv']);
     $expiryDate = rewrite($_POST['card_expired_date']);
     $billingAddress = rewrite($_POST['billing_address']);
     $postalCode = rewrite($_POST['postal_code']);

     $status = "Active";

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $uid."<br>";
    //  echo $username."<br>";
    //  echo $bank."<br>";
    //  echo $bankHolder."<br>";
    //  echo $bankNo."<br>";

     if(addCreditCard($conn,$uid,$username,$nameOnCard,$cardNo,$cardType,$ccv,$expiryDate,$billingAddress,$postalCode,$status))
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../editBankDetails.php?type=1');
     }
     else 
     {
          echo "fail";   
     }
 
}
else 
{
     header('Location: ../index.php');
}

?>