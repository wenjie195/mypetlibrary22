<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid  = $_SESSION['uid'];

     $editPassword_current  = $_POST['password'];

     $newEmail  = $_POST['new_email'];

     // $editPassword_new  = $_POST['register_password'];
     // $editPassword_reenter  = $_POST['register_retype_password'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userDetails = $user[0];
     // echo $userDetails->getUsername();

     $dbPass =  $userDetails->getPassword();
     $dbSalt =  $userDetails->getSalt();

     $editPassword_current_hashed = hash('sha256',$editPassword_current);
     $editPassword_current_hashed_salted = hash('sha256', $dbSalt . $editPassword_current_hashed);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $editPassword_current."<br>";

     if($editPassword_current_hashed_salted == $dbPass)
     {
          // if(!$user)
          if(isset($_POST['new_email']))
          {  

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($newEmail)
               {
                   array_push($tableName,"email");
                   array_push($tableValue,$newEmail);
                   $stringType .=  "s";
               }    
       
               array_push($tableValue,$uid);
               $stringType .=  "s";
               $editEmail = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($editEmail)
               {
                   // echo "credit card deleted";
                   $_SESSION['messageType'] = 1;
                   header('Location: ../adminChangeEmail.php?type=1');
               }
               else
               {
                   $_SESSION['messageType'] = 1;
                   header('Location: ../adminChangeEmail.php?type=2');
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminChangeEmail.php?type=3');
          }
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminChangeEmail.php?type=4');
     }    
}
else 
{
     header('Location: ../index.php');
}  
?>