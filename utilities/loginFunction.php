<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

// $_SESSION['url'] = $_SERVER['REQUEST_URI'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){

        $name = rewrite($_POST['register_name']);
        // $password = rewrite($_POST['password']);
        $phoneNo = rewrite($_POST['phone_no']);

        $userRows = getUser($conn," WHERE name = ? ",array("name"),array($name),"s");
        if($userRows)
        {
            $user = $userRows[0];

            // $tempPass = hash('sha256',$password);
            // $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($phoneNo == $user->getPhoneNo()) 
                {
                    // if(isset($_POST['remember-me'])) 
                    // {
                    //     setcookie('name-mypetslib', $name, time() + (86400 * 30), "/");
                    //     setcookie('password-mypetslib', $password, time() + (86400 * 30), "/");
                    //     setcookie('remember-mypetslib', 1, time() + (86400 * 30), "/");
                    //     // echo 'remember me';
                    // }
                    // else 
                    // {
                    //     setcookie('name-mypetslib', '', time() + (86400 * 30), "/");
                    //     setcookie('password-mypetslib', '', time() + (86400 * 30), "/");
                    //     setcookie('remember-mypetslib', 0, time() + (86400 * 30), "/");
                    //     // echo 'null';
                    // }

                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['usertype'] = $user->getUserType();
                    
                    // if($user->getUserType() == 0)
                    // {
                    //     header('Location: ../adminDashboard.php');
                    // }
                    // elseif($user->getUserType() == 2)
                    // {
                    //     header('Location: ../sellerDashboard.php');
                    // }
                    // elseif($user->getUserType() == 1)

                    if($user->getUserType() == 1)
                    {
                        // header('Location: ../profile.php');

                        if(isset($_SESSION['url'])) 
                        {
                            $url = $_SESSION['url']; 
                             header("location: $url");
                        }
                        else 
                        {
                            // header("location: $url");
                            header('Location: ../profile.php');
                        }
                    }
                    elseif($user->getUserType() == 5)
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../index.php?type=8');
                    }
                    elseif($user->getUserType() == 7)
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../index.php?type=9');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../index.php?type=6');
                    }
                }
                else 
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=4');
                    //header('Location: ../index.php?type=11');
                    //echo "<script>alert('incorrect password');window.location='../index.php'</script>";
                }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../index.php?type=2');
            // echo "// no user with this email ";
            // echo "<script>alert('no user with this username');window.location='../index.php'</script>";
        }
    }

    $conn->close();
}
?>