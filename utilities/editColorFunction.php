<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/../classes/Breed.php';
require_once dirname(__FILE__) . '/../classes/Color.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $colorName = rewrite($_POST["update_color_name"]);
    $id = rewrite($_POST["color_id"]);

    $previousType = rewrite($_POST["color_type"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $allColor = getColor($conn," WHERE name = ? AND type = ? ",array("name","type"),array($colorName,$previousType),"si");
    $existingColor = $allColor[0];

    $color = getColor($conn," id = ?   ",array("id"),array($id),"s");    

    if($previousType == '1')
    {
        if(!$existingColor)
        {
            if(!$color)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($colorName)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$colorName);
                    $stringType .=  "s";
                }
                array_push($tableValue,$id);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"color"," WHERE id = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../puppyColor.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../puppyColor.php?type=2');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../puppyColor.php?type=3');
            }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../puppyColor.php?type=4');
        }
    }
    elseif($previousType == '2')
    {
        if(!$existingColor)
        {
            if(!$color)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($colorName)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$colorName);
                    $stringType .=  "s";
                }
                array_push($tableValue,$id);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"color"," WHERE id = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../kittenColor.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../kittenColor.php?type=2');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../kittenColor.php?type=3');
            }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../kittenColor.php?type=4');
        }
    }
    elseif($previousType == '3')
    {
        if(!$existingColor)
        {
            if(!$color)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($colorName)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$colorName);
                    $stringType .=  "s";
                }
                array_push($tableValue,$id);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"color"," WHERE id = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../reptileColor.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../reptileColor.php?type=2');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../reptileColor.php?type=3');
            }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../reptileColor.php?type=4');
        }
    }


    // if(!$color)
    // {   
    //     $tableName = array();
    //     $tableValue =  array();
    //     $stringType =  "";
    //     //echo "save to database";

    //     if($colorName)
    //     {
    //         array_push($tableName,"name");
    //         array_push($tableValue,$colorName);
    //         $stringType .=  "s";
    //     }

    //     array_push($tableValue,$id);
    //     $stringType .=  "s";
    //     $passwordUpdated = updateDynamicData($conn,"color"," WHERE id = ? ",$tableName,$tableValue,$stringType);
    //     if($passwordUpdated)
    //     {
    //         // $_SESSION['messageType'] = 1;
    //         // header('Location: ../puppyColor.php?type=1');

    //         if($previousType == '1')
    //         {
    //             $_SESSION['messageType'] = 1;
    //             header('Location: ../puppyColor.php?type=1');
    //         }
    //         elseif($previousType == '2')
    //         {
    //             $_SESSION['messageType'] = 1;
    //             header('Location: ../kittenColor.php?type=1');
    //         }
    //         elseif($previousType == '3')
    //         {
    //             $_SESSION['messageType'] = 1;
    //             header('Location: ../reptileColor.php?type=1');
    //         }

    //     }
    //     else
    //     {
    //         $_SESSION['messageType'] = 1;
    //         header('Location: ../puppyColor.php?type=2');
    //     }
    // }
    // else
    // {
    //     $_SESSION['messageType'] = 1;
    //     header('Location: ../puppyColor.php?type=3');
    // }

}
else 
{
    header('Location: ../index.php');
}
?>
