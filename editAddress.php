<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Shipping Address | Mypetslibrary" />
<title>Edit Shipping Address | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name">Shipping Address</p>
 		<!-- <form> -->
        <form method="POST" action="utilities/editAddressFunction.php">
        <div class="dual-input">
        	<p class="input-top-p">Receiver Name</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Receiver Name" value="<?php echo $userData->getReceiverName();?>" name="update_receiver_name" id="update_receiver_name">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Receiver Contact No.</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Receiver Contact Number" value="<?php echo $userData->getReceiverContactNo();?>" name="update_receiver_phone" id="update_receiver_phone">      
        </div>      
          
        <div class="clear"></div>

        <div class="width100 overflow">
        	<p class="input-top-p">Address</p>
        	<textarea class="input-name clean address-textarea" type="text" placeholder="Address" name="update_address" id="update_address"><?php echo $userData->getShippingAddress();?></textarea>      
        </div>   

        <div class="clear"></div>  

        <div class="dual-input">
        <!-- <div class="dual-input second-dual-input"> -->
            <p class="input-top-p">Area</p>
            <input class="input-name clean input-textarea" type="text" placeholder="Area" value="<?php echo $userData->getShippingArea();?>" name="update_area" id="update_area"> 
        	<!-- <select class="input-name clean" required >
            	<option>Area</option>
                <option>Bayan Baru</option>
            </select>        -->
        </div>      
        
        <!-- <div class="dual-input"> -->
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Postal Code</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Postal Code" value="<?php echo $userData->getShippingPostalCode();?>" name="update_code" id="update_code">     
        </div> 

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p">State</p>
            <!-- <input class="input-name clean input-textarea" type="text" placeholder="State" value="<?php echo $userData->getShippingState();?>" name="update_state" id="update_state">  -->

            <select class="input-name clean" type="text" name="update_state" id="update_state" required>
                        <?php
                        if($userData->getShippingState() == '')
                        {
                        ?>
                            <option selected>Select State</option>
                            <?php
                            for ($cnt=0; $cnt <count($states) ; $cnt++)
                            {
                            ?>
                                <option value="<?php echo $states[$cnt]->getStateName(); ?>"> 
                                    <?php echo $states[$cnt]->getStateName(); ?>
                                </option>
                            <?php
                            }
                        }
                        else
                        {
                            for ($cnt=0; $cnt <count($states) ; $cnt++)
                            {
                                if ($userData->getShippingState() == $states[$cnt]->getStateName())
                                {
                                ?>
                                    <option selected value="<?php echo $states[$cnt]->getStateName(); ?>"> 
                                        <?php echo $states[$cnt]->getStateName(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $states[$cnt]->getStateName(); ?>"> 
                                        <?php echo $states[$cnt]->getStateName(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
            </select>    

        </div>

        <div class="clear"></div>
        
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Address Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update address";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "User details is not available !!";
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>