<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Product Review | Mypetslibrary" />
<title>Product Review | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="review-big-div width100 same-padding overflow menu-distance min-height3">
 		<div class="one-row-review">
        	<div class="left-review-profile">
            	<img src="img/pet-seller2.jpg" class="profile-pic-css">
            </div>
            <div class="left-review-data">
            	<p class="review-username-p">Jack Lim</p>
                <div class="review-star-div">
                	<img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                </div>
            </div>
            <div class="right-review-div">
            	<button class="clean transparent-button like-button hover1 pointer react-button">
                	<img src="img/like.png" class="hover1a" alt="Like" title="Like">
                    <img src="img/like2.png" class="hover1b" alt="Like" title="Like">
                    <p class="like-no-p">1000</p>
                </button>
            	<button class="clean transparent-button report-button hover1 pointer react-button open-report">
                	<img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                    <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                </button>                
            </div>
            <div class="clear"></div>
            <div class="width100 review-content">
            	<p class="review-content-p">
                	Good Product
                </p>
            </div>
            <p class="review-date">12/12/2019</p>
        </div>
 		<div class="one-row-review">
        	<div class="left-review-profile">
            	<img src="img/pet-seller2.jpg" class="profile-pic-css">
            </div>
            <div class="left-review-data">
            	<p class="review-username-p">Jack Lim</p>
                <div class="review-star-div">
                	<img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                </div>
            </div>
            <div class="right-review-div">
            	<button class="clean transparent-button like-button hover1 pointer react-button">
                	<img src="img/like.png" class="hover1a" alt="Like" title="Like">
                    <img src="img/like2.png" class="hover1b" alt="Like" title="Like">
                </button>
            	<button class="clean transparent-button report-button hover1 pointer react-button open-report">
                	<img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                    <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                </button>                
            </div>
            <div class="clear"></div>
            <div class="width100 review-content">
            	<p class="review-content-p">
                	Good Product
                </p>
            </div>
            <p class="review-date">12/12/2019</p>
        </div>
 		<div class="one-row-review">
        	<div class="left-review-profile">
            	<img src="img/pet-seller2.jpg" class="profile-pic-css">
            </div>
            <div class="left-review-data">
            	<p class="review-username-p">Jack Lim</p>
                <div class="review-star-div">
                	<img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                </div>
            </div>
            <div class="right-review-div">
            	<button class="clean transparent-button like-button hover1 pointer react-button">
                	<img src="img/like.png" class="hover1a" alt="Like" title="Like">
                    <img src="img/like2.png" class="hover1b" alt="Like" title="Like">
                    <p class="like-no-p">1000</p>
                </button>
            	<button class="clean transparent-button report-button hover1 pointer react-button open-report">
                	<img src="img/flag.png" class="hover1a" alt="Report" title="Report">
                    <img src="img/flag2.png" class="hover1b" alt="Report" title="Report">
                </button>                
            </div>
            <div class="clear"></div>
            <div class="width100 review-content">
            	<p class="review-content-p">
                	Good Product
                </p>
            </div>
            <p class="review-date">12/12/2019</p>
        </div>                
        <div class="clear"></div>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>