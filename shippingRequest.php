<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

//Product Order
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productsOrders =  getProductOrders($conn," WHERE quantity > 2 ");
$productsOrders =  getProductOrders($conn," Order By date_created DESC ");

$conn->close();

$conn = connDB();
$post = array();
$start_date = date("Y-m-d");
$end_date = date("Y-m-d");
$name = "";
$phoneNo="";

$record_per_page = 8;
$page = '';
if(isset($_GET["page"]))
{
 $page = $_GET["page"];
}
else
{
 $page = 1;
}

$start_from = ($page-1)*$record_per_page;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

if (isset($_GET["start_date"]) && isset($_GET["end_date"]) && isset($_GET["name"]))
{
	$start_date = $_GET["start_date"];
	$end_date = $_GET["end_date"];
	$name = $_GET["name"];
  $id = $_GET["id"];
  $phoneNo = $_GET["contactNo"];
  //$total = $_GET["total"];
	$post = $_GET;
}

$products = getProduct($conn);
$list = GetList($post, $conn,$start_from,$record_per_page);

//echo json_encode($list);//exit;

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function GetList($post, $conn,$start_from,$record_per_page)
{
	$sql = "SELECT name,date_created, ";
	$sql .= "id,contactNo,total ";
	$sql .= "FROM orders ";
	$sql .= "WHERE payment_status = 'ACCEPTED' AND shipping_status = 'PENDING'";

  if (isset($post['reset'])) {
    if (isset($post["start_date"]) && strlen($post["start_date"]) < 0)
  	{
  		$sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
  	}

  	if (isset($post["end_date"]) && strlen($post["end_date"]) < 0)
  	{
  		$sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
  	}

  	if (isset($post["name"]) && strlen($post["name"]) < 0)
  	{
      $sql .= "AND name LIKE '%" . $post["name"] . "%' ";
  	}
    if (isset($post["id"]) && strlen($post["id"]) < 0)
  	{
      $sql .= "AND id LIKE '%" . $post["id"] . "%' ";
  	}
    if (isset($post["contactNo"]) && strlen($post["contactNo"]) < 0)
  	{
      $sql .= "AND contactNo LIKE '%" . $post["contactNo"] . "%' ";
  	}
  }else {
    if (isset($post["start_date"]))
  	{
  		$sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
  	}

  	if (isset($post["end_date"]))
  	{
  		$sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
  	}

  	if (isset($post["name"]) && strlen($post["name"]) > 0)
  	{
      $sql .= "AND name LIKE '%" . $post["name"] . "%' ";
  	}
    if (isset($post["id"]) && strlen($post["id"]) > 0)
  	{
      $sql .= "AND id LIKE '%" . $post["id"] . "%' ";
  	}
    if (isset($post["contactNo"]) && strlen($post["contactNo"]) > 0)
  	{
      $sql .= "AND contactNo LIKE '%" . $post["contactNo"] . "%' ";
  	}
  }

	$sql .= "ORDER BY date_created ASC LIMIT $start_from, $record_per_page";
	//echo $sql;exit;

	$result = $conn->query($sql);
	$output = array();

	if ($result->num_rows > 0)
	{
		// output data of each row
		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}
	}

	return $output;
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Shipping Request | Mypetslibrary" />
<title>Shipping Request | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="width100 ship-top-div">
        <!-- <h1 class="green-text h1-title"><a href="paymentVerification.php" class="green-a">Payment Verification</a> | Shipping Request | <a href="shippingOut.php" class="green-a">Shipping Out</a> | <a href="shippingCompleted.php" class="green-a">Completed</a> | <a href="shippingRefund.php" class="green-a">Refund</a></h1> -->
        <h1 class="green-text h1-title"><a href="paymentVerification.php" class="green-a">Payment Verification</a> | Shipping Request | <a href="shippingOut.php" class="green-a">Shipping Out</a> | <a href="shippingCompleted.php" class="green-a">Completed</a></h1>
        <div class="green-border"></div>
        </div>
        <div class="width100 ship-bottom-div">
        	<form>
            <input class="line-input clean" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
        </div>  
    </div>


    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
  <?php $conn = connDB();?>
    	<table class="green-table width100" id="myTable">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Order No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Paid On</th>
                    <th>Details</th>                   
                </tr>
            </thead>
            <tbody>
            <?php
            if($list): $ind=0;?>
              <?php foreach ($list AS $ls): $ind++; ?> 
            	    <tr>
                        <td><?php echo $ind; ?></td>
                        <td><?php echo $ls["id"]; ?></td>
                        <td><?php echo $ls["name"]; ?></td>
                        <td><?php echo $ls["contactNo"]; ?></td>
                        <td><?php echo $ls["date_created"]; ?></td>

                        <td>
                          <form action="editShippingRequest.php" method="POST">
                            <button class="clean edit-anc-btn hover1" type="submit" name="order_id" value="<?php echo $ls["id"];?>">
                                <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                            </button>     
                          </form>              
                        </td>
                  </tr>
                  <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
										<!-- <td colspan="7">No result</td> -->
								</tr>
								<?php endif; ?>    
            	                               
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>