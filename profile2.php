<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Seller Review | Mypetslibrary" />
<title>Pet Seller Review | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
 		<div class="width100 overflow profile-top-div">
        	<a href="updateProfilePic.php">
                <div class="profile-pic-div">
                    <div class="update-div-div">
                        <img src="img/update-profile-pic.jpg" class="profile-pic update-profile-pic" alt="Update Profile Picture" title="Update Profile Picture">
                    </div>
                    <!-- <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture"> -->
                    <?php 
                        $proPic = $userData->getProfilePic();
                        if($proPic != "")
                        {
                        ?>
                            <img src="userProfilePic/<?php echo $userData->getProfilePic();?>" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                        <?php
                        }
                        else
                        {
                        ?>
                            <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                        <?php
                        }
                    ?>
                </div>
            </a>
            <div class="next-to-profile-pic-div">
            	<!-- <p class="profile-greeting">Hi, Username</p> -->
                <p class="profile-greeting">Hi, <?php echo $userData->getName();?><br>Points: <?php echo $userData->getPoints();?></p>
                <a href="editProfile.php" class="green-a edit-a hover1"><img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit Profile</a>
            </div>
        </div>
        <div class="clear"></div>

        <div class="line-divider width100 overflow">
        	<a href="cart.php">
                <div class="four-div hover1">
                    <div class="green-dot green-dot2"><p class="cart-text">100</p></div>
                    <img src="img/cart-1.png" class="tab-icon hover1a" alt="Cart" title="Cart">
                    <img src="img/cart-2.png" class="tab-icon hover1b" alt="Cart" title="Cart">
                    <p class="tab-tab-p">In Cart</p>
                </div>
            </a>

            <a href="shipDeliverReceive.php">
                <div class="four-div">
                    <div class="green-dot green-dot2"><p class="cart-text">100</p></div>
                    <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
                    <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
                    <p class="tab-tab-p">To Ship</p>
                </div>
            </a>
    
            <a href="shipDeliverReceive.php">
                <div class="four-div">
                    <div class="green-dot green-dot2"><p class="cart-text">100</p></div>
                    <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
                    <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
                    <p class="tab-tab-p">To Receive</p>
                </div>
            </a>

            <a href="shipDeliverReceive.php">
                <div class="four-div">
                    <div class="green-dot green-dot2"><p class="cart-text">100</p></div>
                    <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
                    <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
                    <p class="tab-tab-p">Received</p>
                </div>
            </a>
        </div>	
        <!--<a href="history.php">
            <div class="profile-line-divider opacity-hover border-top-profile">
                <div class="left-icon-div"><img src="img/history.png" class="left-icon-png" alt="Browse History" title="Browse History"></div>
                <div class="right-profile-content">Browse History</div>
            </div>
        </a>
        <a href="reviewHistory.php">
            <div class="profile-line-divider opacity-hover">
                <div class="left-icon-div"><img src="img/rating.png" class="left-icon-png" alt="Review History" title="Review History"></div>
                <div class="right-profile-content">Review History</div>
            </div>
        </a>        
        <!-- <a href="editEmail.php">
            <div class="profile-line-divider opacity-hover">
                <div class="left-icon-div"><img src="img/email.png" class="left-icon-png" alt="Change Email" title="Change Email"></div>
                <div class="right-profile-content">Change Email</div>
            </div>
        </a> 
        <a href="editContact.php">
            <div class="profile-line-divider opacity-hover">
                <div class="left-icon-div"><img src="img/contact-number.png" class="left-icon-png" alt="Change Phone Number" title="Change Phone Number"></div>
                <div class="right-profile-content">Change Phone Number</div>
            </div>
        </a>         -->
        <a href="editPassword.php">
            <div class="profile-line-divider opacity-hover">
                <div class="left-icon-div"><img src="img/password.png" class="left-icon-png" alt="Change Password" title="Change Password"></div>
                <div class="right-profile-content">Change Password</div>
            </div>
        </a>          
        <a href="editAddress.php">
            <div class="profile-line-divider opacity-hover">
                <div class="left-icon-div"><img src="img/address.png" class="left-icon-png" alt="Shipping Address" title="Shipping Address"></div>
                <div class="right-profile-content">Shipping Address</div>
            </div>
        </a>        
        <a href="bankDetailsEdit.php">
            <div class="profile-line-divider opacity-hover">
                <div class="left-icon-div"><img src="img/banking.png" class="left-icon-png" alt="Banking Details" title="Banking Details"></div>
                <div class="right-profile-content">Banking Details</div>
            </div>
        </a>
        <a href="logout.php">
            <div class="profile-line-divider opacity-hover">
                <div class="left-icon-div"><img src="img/logout.png" class="left-icon-png" alt="Logout" title="Logout"></div>
                <div class="right-profile-content">Logout</div>
            </div>
        </a>                        
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Update Password Successfully !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Update Shipping Address Successfully !"; 
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Profile Picture Updated !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>