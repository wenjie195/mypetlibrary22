<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="My Favourite | Mypetslibrary" />
<title>My Favourite | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 menu-distance2">
	<a href="malaysia-cute-puppy-dog.php">
        <div class="float-left w50 fav orange-bg opacity-hover">
            <img src="img/favourite-dog.png" class="fav-img" alt="Favourite Puppy" title="Favourite Puppy">
            <p class="white-text fav-p">Puppy</p>
        </div>
    </a>
    <a href="malaysia-cute-kitten-cat.php">
        <div class="float-left w50 fav peach-bg opacity-hover">
            <img src="img/favourite-cat.png" class="fav-img" alt="Favourite Kitten" title="Favourite Kitten">
            <p class="white-text fav-p">Kitten</p> 
        </div>
    </a>
    <div class="clear"></div>
    <a href="malaysia-cute-reptiles.php">
        <div class="float-left w50 fav sky-bg opacity-hover">
            <img src="img/favourite-reptile.png" class="fav-img" alt="Favourite Reptile" title="Favourite Reptile">
            <p class="white-text fav-p">Reptile</p>
        </div>
    </a>
    <a href="malaysia-pet-food-toy-product.php">
        <div class="float-left w50 fav tur-bg opacity-hover">
            <img src="img/favourite-product.png" class="fav-img" alt="Favourite Product" title="Favourite Product">
            <p class="white-text fav-p">Product</p>
        </div>
    </a>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>