<div class="width103" id="app">
<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';

// if(isset($_POST["action"]))
// {

	if (isset($_POST['pageno'])) {
		$pageno = $_POST['pageno'];
	} else {
		$pageno = 1;
	}

	$no_of_records_per_page = 500;
	$offset = ($pageno-1) * $no_of_records_per_page;

	$query = "SELECT * FROM puppy WHERE status = 'Available' or status = 'Sold'";

	// $query = "
	// 	SELECT * FROM puppy WHERE status = 'Available' & 'Sold'
	// ";

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row_count = $statement->rowCount();
	$total_pages = ceil($total_row_count / $no_of_records_per_page);

	if(isset($_POST["minimum_price"], $_POST["maximum_price"]) && !empty($_POST["minimum_price"]) && !empty($_POST["maximum_price"]))
	{
		$query .= "
		 AND price BETWEEN '".$_POST["minimum_price"]."' AND '".$_POST["maximum_price"]."'
		";
	}
	// if(isset($_POST["type"]))
	// {
	// 	$type_filter = implode("','", $_POST["type"]);
	// 	$query .= "
	// 	 AND type IN('".$type_filter."')
	// 	";
	// }
	if(isset($_POST["gender"]))
	{
		$gender_filter = implode("','", $_POST["gender"]);
		$query .= "
		 AND gender IN('".$gender_filter."')
		";
	}
	if(isset($_POST["vaccinated"]))
	{
		$vaccinated_filter = implode("','", $_POST["vaccinated"]);
		$query .= "
		 AND vaccinated IN('".$vaccinated_filter."')
		";
	}
	if(isset($_POST["breed"]))
	{
		$breed_filter = implode("','", $_POST["breed"]);
		$query .= "
		 AND breed IN('".$breed_filter."')
		";
	}
	if(isset($_POST["color"]))
	{
		$color_filter = implode("','", $_POST["color"]);
		$query .= "
		 AND color IN('".$color_filter."')
		";
	}
	if(!isset($_POST["gender"]) && !isset($_POST["vaccinated"]) && !isset($_POST["breed"]) && !isset($_POST["color"])){
			$query .= " LIMIT ".$offset.", ".$no_of_records_per_page."";
	}
	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row = $statement->rowCount();
	$output = '';

	if($total_row > 0)
	{
		foreach($result as $row)
		{
			$uid= $row['uid'];

			$imageURL = './uploads/'.$row["default_image"];

			$pupGender = $row['gender'];
			if($pupGender == 'Female')
			{
				$puppyGender = 'F';
			}
			elseif($pupGender == 'Male')
			{
				$puppyGender = 'M';
			}

			$length = strlen($row['price']);?>
                <?php
                if($length == 2)
                {
                    $hiddenPrice = "X";
                }
                elseif($length == 3)
                {
                    $hiddenPrice = "XX";
                }
                elseif($length == 4)
                {
                    $hiddenPrice = "XXX";
                }
                elseif($length == 5)
                {
                    $hiddenPrice = "XXXX";
                }
                elseif($length == 6)
                {
                	$hiddenPrice = "XXXXX";
                }
                elseif($length == 7)
                {
                    $hiddenPrice = "XXXXXX";
				}


			if ($row['status'] == 'Sold'){
				$output .= '

					<a href="puppyDogForSale.php?='.$row['uid'].'" class="opacity-hover pointer">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer pointer-div" value = "'.$row['uid'].'">
						  <div class="square">
							<div class="width100 white-bg content progressive">

									<img data-src="uploads/'. $row['default_image'] .'" src="img/pet-load300.jpg" class="preview width100 two-border-radius opacity-hover pointer  lazy" alt="<?php echo '. $row['image_one'] .'" title="<?php echo '. $row['image_one'] .';?>" />

							</div>
					       </div>
							<div class="sold-label all-pets-sold">Sold</div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['breed'] .' | '. $puppyGender .'</p>
							<p class="slider-product-name text-overflow">
								RM'. substr($row['price'],0,1).$hiddenPrice .'
							</p>
						</div>
					</a>
				';

			}
			elseif ($row['status'] == 'Available'){
				$output .= '

					<a href="puppyDogForSale.php?='.$row['uid'].'" class="opacity-hover pointer">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer pointer-div" value = "'.$row['uid'].'">
						<div class="square">
							<div class="width100 white-bg content progressive">

									<img data-src="uploads/'. $row['default_image'] .'" src="img/pet-load300.jpg"   class="preview width100 two-border-radius opacity-hover pointer lazy" alt="<?php echo '. $row['image_one'] .'" title="<?php echo '. $row['image_one'] .';?>" />

							</div>
						</div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['breed'] .' | '. $puppyGender .'</p>
							<p class="slider-product-name text-overflow">
								RM'. substr($row['price'],0,1).$hiddenPrice .'
							</p>
						</div>
					</a>
				';
			}
		}
	}
	else
	{
		$output = '<h3>No Data Found</h3>';
	}
	// $output .= '
	//
	// 	';

	echo $output;
// }

?>
</div>
<div class="clear"></div>
<div class="width100 text-center overflow">
    <ul class="pagination page-pagi">
        <li><a href="?pageno=1">First</a></li>
        <li class="<?php if($pageno <= 1){ echo disabled; } ?>">
            <a href="<?php if($pageno <= 1){ echo "#"; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
        </li>
        <li class="<?php if($pageno >= $total_pages){ echo disabled; } ?>">
            <a href="<?php if($pageno >= $total_pages){ echo "#"; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
        </li>
        <li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
    </ul>
</div>
<script type="text/javascript">

var totalId = <?php echo $total_row_count + 1 ?>;
// alert(totalId);
for(var i=0; i< totalId; i++){

	$("#"+i+"").click(function(){
					var x = $(this).attr('value');
					location.href = "./puppyDogForSale.php?id="+x+"";
				});

}
</script>
  <script src="js/index2.js"></script>
  <script>
    (function(){
      new Progressive({
        el: '#app',
        lazyClass: 'lazy',
        removePreview: true,
        scale: true
      }).fire()
    })()
  </script>
