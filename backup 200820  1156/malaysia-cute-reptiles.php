<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';

require_once dirname(__FILE__) . '/classes/Reptile.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

if (isset($_GET['pageno'])) {
      $pageno = $_GET['pageno'];
    } else {
      $pageno = 1;
    }
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Reptiles For Sale in Malaysia | Mypetslibrary" />
<title>Reptiles For Sale in Malaysia | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴,buy reptile, buy pet online, Malaysia, buy pet in Malaysia, 马来西亚,马来西亚宠物店, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<script src="js/jquery-2.2.0.min.js"></script>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

      <div class="fix-filter width100 small-padding overflow">
            <h1 class="green-text user-title left-align-title">Reptiles</h1>
            <div class="filter-div">
            	<a class="open-filter2 filter-a green-a">Filter</a>
            </div>
      </div>

      <div class="clear"></div>    
<!-- Filter Modal -->
<div id="filter-modal2" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css big-filter-margin">
    <span class="close-css close-filter2">&times;</span>

    <div class="clear"></div>      
    <h2 class="green-text h2-title">Filter</h2>
    <div class="green-border filter-border"></div> 
    <div class="filter-div-section">

                        <p class="filter-label filter-label3 green-filter">Gender</p>
                        <?php
                        $query = "
                        SELECT DISTINCT(gender) FROM reptile WHERE status = 'available' ORDER BY gender DESC
                        ";
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $result = $statement->fetchAll();
                        foreach($result as $row)
                        {
                        ?>
                        <div class="filter-option">
                            <label  class="filter-label filter-label2">
                                <input type="checkbox" class="common_selector filter-option gender" value="<?php echo $row['gender']; ?>" > <?php echo $row['gender']; ?><span class="checkmark"></span>
                            </label>  
                        </div>  
                        <?php    
                        }
                        ?>
                  </div>
                   <!--           
                  <div class="list-group">
                        <h3>Vaccination</h3>
                        <?php
                        $query = "
                        SELECT DISTINCT(vaccinated) FROM reptile WHERE status = 'available' ORDER BY vaccinated DESC
                        ";
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $result = $statement->fetchAll();
                        foreach($result as $row)
                        {
                        ?>
                              <div class="list-group-item checkbox">
                                    <label><input type="checkbox" class="common_selector vaccinated" value="<?php echo $row['vaccinated']; ?>"  > <?php echo $row['vaccinated']; ?></label>
                              </div>
                        <?php
                        }
                        ?>	
                  </div>-->

                  <div class="filter-div-section">
                        <p class="filter-label filter-label3 green-filter">Breed</p>
                        <?php
                        $query = "
                        SELECT DISTINCT(breed) FROM reptile WHERE status = 'available' ORDER BY breed DESC
                        ";
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $result = $statement->fetchAll();
                        foreach($result as $row)
                        {
                        ?>
                        <div class="filter-option">
                            <label  class="filter-label filter-label2">
                                <input type="checkbox" class="common_selector filter-option breed" value="<?php echo $row['breed']; ?>" >  <?php echo $row['breed']; ?><span class="checkmark"></span>
                            </label>  
                        </div>  

                        <?php
                        }
                        ?>	
                  </div>

            <div class="filter-div-section">
				<p class="filter-label filter-label3 green-filter">Color</p>
                        <?php
                        $query = "
                        SELECT DISTINCT(color) FROM reptile WHERE status = 'available' ORDER BY color DESC
                        ";
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $result = $statement->fetchAll();
                        foreach($result as $row)
                        {
                        ?>
                       <div class="filter-option">
                            <label  class="filter-label filter-label2">
                                <input type="checkbox" class="common_selector filter-option color" value="<?php echo $row['color']; ?>" >  <?php echo $row['color']; ?><span class="checkmark"></span>
                            </label>  
                        </div> 

                        <?php
                        }
                        ?>	
                  </div>
			<div class="filter-div-section">
				<p class="filter-label filter-label3 green-filter">Price</p>
                <div class="distance-slider">
                    <input type="hidden" id="hidden_minimum_price" value="0" />
                    <input type="hidden" id="hidden_maximum_price" value="65000" />
                    <p id="price_show">100 - 65000</p>
                    <div id="price_range"></div>
                </div>
            </div>	
            <input type="hidden" id="pageno" value="<?php echo $pageno ?>">
                    <!-- 	<div class="filter-div-section">
            <label for="price" class="filter-label filter-label3">Price Range: <span id="amount"></span>
                <input type="checkbox" name="price" id="price" value="0" class="filter-input" />
                <span class="checkmark"></span>
            </label>
            <div id="priceDiv" style="display:none" class="distance-slider">
            	  <div id="slider-range" class="clean"></div>

                  <form method="post" action="get_items.php">
                    <input type="hidden" id="amount1" class="clean">
                    <input type="hidden" id="amount2" class="clean">

                  </form>
            </div>
        </div>    -->	
        <div class="green-button mid-btn-width clean mid-btn-margin close-filter2">Close</div>
      </div>
      </div>
      </div>
      <!-- End of Filter Modal -->
<div class="width100 small-padding overflow min-height-with-filter filter-distance">

	
 
         
                  <div class="filter_data"></div>


     
</div>
      <div class="clear"></div>

      <style>
            .animated.slideUp{
                  animation:none !important;}
            .animated{
                  animation:none !important;}
            .reptile-a .hover1a{
                  display:none !important;}
            .reptile-a .hover1b{
                  display:inline-block !important;}	
      </style>
      <script>
            $(document).ready(function(){

                  filter_data();

                  function filter_data()
                  {
                        $('.filter_data').html('<div id="loading" style="" ></div>');
                        var action = 'filterdataReptile';
                        var pageno = $("#pageno").val();
                        var minimum_price = $('#hidden_minimum_price').val();
                        var maximum_price = $('#hidden_maximum_price').val();
                        // var type = get_filter('type');
                        var gender = get_filter('gender');
                        var vaccinated = get_filter('vaccinated');
                        var breed = get_filter('breed');
                        var color = get_filter('color');
                        $.ajax({
                        url:"filterdataReptile.php",
                        method:"POST",
                        // data:{action:action, minimum_price:minimum_price, maximum_price:maximum_price, type:type, gender:gender, vaccinated:vaccinated,
                        //     breed:breed, color:color },
                        data:{action:action, minimum_price:minimum_price, maximum_price:maximum_price, gender:gender, vaccinated:vaccinated,
                              breed:breed, color:color,pageno:pageno },
                        success:function(data){
                              $('.filter_data').html(data);
                        }
                        });
                  }

                  function get_filter(class_name)
                  {
                        var filter = [];
                        $('.'+class_name+':checked').each(function(){
                        filter.push($(this).val());
                        });
                        return filter;
                  }

                  $('.common_selector').click(function(){
                        filter_data();
                  });

                  $('#price_range').slider({
                        range:true,
                        min:100,
                        max:65000,
                        values:[100, 65000],
                        step:100,
                        stop:function(event, ui)
                        {
                        $('#price_show').html(ui.values[0] + ' - ' + ui.values[1]);
                        $('#hidden_minimum_price').val(ui.values[0]);
                        $('#hidden_maximum_price').val(ui.values[1]);
                        filter_data();
                        }
                  });

            });
      </script>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
</body>
</html>