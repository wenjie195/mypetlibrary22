<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];
$petsUid = $_SESSION['pets_uid'];

$conn = connDB();

$petsDetails = getPetsDetails($conn, "WHERE uid =?",array("uid"),array($petsUid),"s");
$petsType = $petsDetails[0]->getType();

$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

	$image_array_1 = explode(";", $data);

	$image_array_2 = explode(",", $image_array_1[1]);

	$data = base64_decode($image_array_2[1]);

	$imageName = time().$petsUid.'.png';
	// $imageName = time();

	file_put_contents('uploads/'.$imageName, $data);

	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";
		//echo $imageName;

		if($imageName)
		{
		array_push($tableName,"image_two");
		array_push($tableValue,$imageName);
		$stringType .=  "s";
		}
		array_push($tableValue,$petsUid);
		$stringType .=  "s";
		$uploadCompanyLogo = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
		// unset($_SESSION['pets_uid']);
		if($uploadCompanyLogo)
		{
			if($petsType == "Kitten")
			{
				if(isset($_POST["image"]))
				{
					$tableName = array();
					$tableValue =  array();
					$stringType =  "";
					//echo $imageName;
			
					if($imageName)
					{
					array_push($tableName,"image_two");
					array_push($tableValue,$imageName);
					$stringType .=  "s";
					}
					array_push($tableValue,$petsUid);
					$stringType .=  "s";
					$uploadKittenImage = updateDynamicData($conn,"kitten"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
					unset($_SESSION['pets_uid']);
					if($uploadKittenImage)
					{
					?>
						<div class="preview-img-div">
							<img src=uploads/<?php echo $imageName  ?>  class="width100" />
						</div>

						<div class="width100 overflow text-center">     
							<p class="review-product-name text-center ow-margin-top20">Complete</p>
						</div>
					<?php
					}
					else
					{
						echo "fail 2";
					}
			
				}
			}
			elseif($petsType == "Puppy")
			{
				if(isset($_POST["image"]))
				{
					$tableName = array();
					$tableValue =  array();
					$stringType =  "";
					//echo $imageName;
			
					if($imageName)
					{
					array_push($tableName,"image_two");
					array_push($tableValue,$imageName);
					$stringType .=  "s";
					}
					array_push($tableValue,$petsUid);
					$stringType .=  "s";
					$uploadPuppyImage = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
					unset($_SESSION['pets_uid']);
					if($uploadPuppyImage)
					{
					?>
						<div class="preview-img-div">
							<img src=uploads/<?php echo $imageName  ?>  class="width100" />
						</div>

						<div class="width100 overflow text-center">     
							<p class="review-product-name text-center ow-margin-top20">Complete</p>
						</div>
					<?php
					}
					else
					{
						echo "fail 2";
					}
			
				}
			}
			elseif($petsType == "Reptile")
			{
				if(isset($_POST["image"]))
				{
					$tableName = array();
					$tableValue =  array();
					$stringType =  "";
					//echo $imageName;
			
					if($imageName)
					{
					array_push($tableName,"image_two");
					array_push($tableValue,$imageName);
					$stringType .=  "s";
					}
					array_push($tableValue,$petsUid);
					$stringType .=  "s";
					$uploadReptileImage = updateDynamicData($conn,"reptile"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
					unset($_SESSION['pets_uid']);
					if($uploadReptileImage)
					{
					?>
						<div class="preview-img-div">
							<img src=uploads/<?php echo $imageName  ?>  class="width100" />
						</div>

						<div class="width100 overflow text-center">     
							<p class="review-product-name text-center ow-margin-top20">Complete</p>
						</div>
					<?php
					}
					else
					{
						echo "fail 2";
					}
			
				}
			}
		}
		else
		{
			echo "fail 4";
		}

	}
	else
	{
		echo "ERROR";
	}


}

?>