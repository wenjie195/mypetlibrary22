<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Variation.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    header('Location: ./cart.php');
}

if(isset($_GET['id'])){
    $referrerUidLink = $_GET['id'];
}
else{
    echo "error";
}

//$products = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
$products = getProduct($conn);
$variation = getVariation($conn);

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($variation,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($variation);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
    <?php
        $conn = connDB();
            if($products)
            {
                for($cnt = 0;$cnt < count($products) ;$cnt++)
                {
                    if($products[$cnt]->getUid()==$referrerUidLink)
                    {
                    ?>   
<!doctype html>
<html>
<head>
<!--default image-->
<meta property="og:image" content="https://ichibangame.com/mypetslibrary/uploads/<?php echo $products[$cnt]->getImageOne() ?>" /> 
<?php include 'meta.php'; ?>
<!--    <meta property="og:url" content="https://ichibangame.com/mypetslibrary/productDetails.php?id=" />
    <link rel="canonical" href="https://ichibangame.com/mypetslibrary/productDetails.php?id=" />-->
<meta property="og:title" content="<?php echo $products[$cnt]->getName();?> | Mypetslibrary" />
<title><?php echo $products[$cnt]->getName();?> | Mypetslibrary</title>
<meta property="og:description" content="<?php echo $products[$cnt]->getDescription();?>" />
<meta name="description" content="<?php echo $products[$cnt]->getDescription();?>" />
<meta name="keywords" content="<?php echo $products[$cnt]->getName();?>, for <?php echo $products[$cnt]->getAnimalType();?>, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<!-- <div class="width100 menu-distance3 same-padding min-height2">

    <form method="POST">
        <div >
        <//?php
        if(isset($_GET['id'])){
            $referrerUidLink = $_GET['id'];
            echo "tt";
            echo "tt";
            echo "tt";
            echo $referrerUidLink;
        }
        else{
            echo "error";
            echo "error";
            echo "error";
            echo "error";
        }
        ?>
            <//?php echo $productListHtml; ?>
        </div>

        <div class="clear"></div>  
        <div class="width100 text-center">                                                                         
            <button class="green-button checkout-btn clean">Add to Cart</button>
        </div>

    </form>
</div> -->

<div class="width100 menu-distance3 same-padding min-height2">
               
                        <div class="left-image-div">
                            <div class="item">
                                <div class="clearfix">
                                    <img src="<?php echo "uploads/".$products[$cnt]->getImageOne() ?>" class="pet-slider-img" alt="<?php echo $products[$cnt]->getName();?>" title="<?php echo $products[$cnt]->getName();?>" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="right-content-div2">
                            <p class="green-text breed-p"><?php echo $products[$cnt]->getBrand();?></p>
                            <h1 class="green-text pet-name"><?php echo $products[$cnt]->getName();?></h1>
                            <p class="price-p2">
                               RM 110 - 1110
                            </p>
                            <div class="right-info-div">
                                <a href="tel:+60134288626" class="contact-icon hover1">
                                    <img src="img/call.png" class="hover1a" alt="Call" title="Call">
                                    <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
                                </a>
                                <!--href="https://api.whatsapp.com/send?phone=60134288626&text=https://ichibangame.com/mypetslibrary/productDetails.php?id="-->
                                <a onclick="window.open(this.href); return false;"  class="contact-icon hover1  mid-whatsapp">
                                    <img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                                    <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
                                </a> 
                                <!-- <a class="contact-icon hover1">
                                    <img src="img/favourite-1.png" class="hover1a" alt="Favourite" title="Favourite">
                                    <img src="img/favourite-2.png" class="hover1b" alt="Favourite" title="Favourite">
                                </a>    -->

                                <a class="contact-icon hover1 last-contact-icon open-social">
                                    <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                                    <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
                                </a>
                            </div>
                            <div class="clear"></div>

                            <div class="pet-details-div">
                                <div class="tab">
                                    <button class="tablinks active" onclick="openTab(event, 'Details')">Details</button>
                                    <button class="tablinks" onclick="openTab(event, 'Terms')">Terms</button>
                                </div>
                                <div  id="Details" class="tabcontent block">
                                    <table class="pet-table">
                                        <!--<tr>
                                            <td class="grey-p">SKU</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $products[$cnt]->getSku();?></td>
                                        </tr>
                                         <tr>
                                            <td class="grey-p">Stock</td>
                                            <td class="grey-p">:</td>
                                            <td><//?php echo $products[$cnt]->getVariationStock();?></td>
                                        </tr>
                                        <tr>
                                            <td class="grey-p">Expiry Date</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $products[$cnt]->getExpiryDate();?></td>
                                        </tr> -->
                                        <tr>
                                            <td class="grey-p">For Animal Type</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $products[$cnt]->getAnimalType();?></td>
                                        </tr>
                                        <tr>
                                            <td class="grey-p">Category</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $products[$cnt]->getCategory();?></td>
                                        </tr>
                                        <tr>
                                            <td class="grey-p">Description</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $products[$cnt]->getDescription();?></td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="Terms" class="tabcontent">
                                    <p class="pet-table-p">Terms content</p>
                                </div>
                                <a href="malaysia-pet-food-toy-product.php">
                                    <div class="review-div hover1 upper-review-div product-line-div">
                                        <p class="left-review-p grey-p">Brand</p>
                                        <p class="left-review-mark ow-brand-p text-overflow"><?php echo $products[$cnt]->getBrand();?></p>

                                        <p class="right-arrow">
                                            <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                                            <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                                        </p>
                                        <!--
                                        <p class="beside-right-arrow grey-to-lightgreen">
                                            View All Product
                                        </p>-->
                                    </div>
                                </a>
                                <a href="productReview.php">
                                    <div class="review-div hover1 lower-review-div product-line-div">
                                        <p class="left-review-p grey-p">Reviews</p>
                                        <p class="left-review-mark">4/5</p>
                                        <p class="right-review-star ow-right-review-star-v">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                            <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                                        </p>
                                        <p class="right-arrow">
                                            <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                                            <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div> 
                    <?php
                    }
                }
            }
    ?>

<div class="clear"></div>
	<div class="seller-big-info variation-big-big-div">
    <h1 class="green-text seller-h1 margin-bottom20">Purchase Now</h1>

	<div class="clear"></div>    

        <div class="width103">
    <?php
    if($variation)
    {
        for($count = 0;$count < count($variation) ;$count++)
        {
            if($variation[$count]->getProductId()== $referrerUidLink)
            {
            ?>
                
                    <a  class="menu-padding black-to-white open-login menu-margin text-menu-a">
                        <div class="shadow-white-box four-box-size opacity-hover">
                        	<div class="square">  
                            <div class="width100 white-bg content">
                                <img src="uploads/<?php echo $variation[$count]->getVariationImage();?>" alt="<?php echo $variation[$count]->getName();?>" title="<?php echo $variation[$count]->getName();?>" class="width100 two-border-radius">
                            </div>
                            </div>
                            <div class="width100 product-details-div">
                                <p class="width100 text-overflow slider-product-name"><?php echo $variation[$count]->getName();?></p>
                                <p class="width100 text-overflow slider-product-name">RM<?php echo $variation[$count]->getVariationPrice();?>.00</p>
                                <div class="clear"></div>
                                <div class="peach-button">Add to Cart</div>
                            </div>
                        </div>
                    </a> 
              
            <?php
            }
        }
    }
    ?>
</div>
</div></div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<div class="sticky-distance2 width100"></div>

<div class="sticky-call-div">
<table class="width100 sticky-table">
	<tbody>
    	<tr>
        	<td class="call-td">
                <a  href="tel:+60134288626" class="text-center clean transparent-button same-text" >
                    Call
                </a>
        	</td>
            <td class="whatsapp-td">
            	<!--href="https://api.whatsapp.com/send?phone=601159118132"-->
                <a onclick="window.open(this.href); return false;"   class="text-center clean transparent-button same-text">
                    Whatsapp
                </a>
        	</td>
    	</tr>
    </tbody>    
</table>   
</div>

<?php include 'stickyFooter.php'; ?>

<style>
.social-dropdown {
    width: 360px;
}
</style>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

</body>
</html>