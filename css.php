<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
<link rel="icon" href="img/favicon.ico" type="image/x-icon">

<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/component.css?version=1.0.3">
<link rel="stylesheet" type="text/css" href="css/main.css?version=1.0.1">
<link rel="stylesheet" type="text/css" href="css/lightslider.css?version=1.0.1">
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css?version=1.0.1">
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="css/style.css?version=1.9.3">
