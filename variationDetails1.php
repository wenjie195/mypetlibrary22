<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

//require_once dirname(__FILE__) . '/classes/Product1.php';
require_once dirname(__FILE__) . '/classes/Variation2.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    echo "<script type='text/javascript'>window.parent.location='cart.php'</script>";
}

if(isset($_GET['id'])){
    $referrerUidLink = $_GET['id'];
}
else{
    echo "error";
}

//$products = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
//$products = getProduct($conn);
$variation = getVariation($conn);

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($variation,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($variation);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!--header('Location: ./cart.php');-->
<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Mypetslibrary" />
<title>Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary" />
<meta name="description" content="Mypetslibrary" />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">


<div class="variation-big-div text-center width100 overflow">
    <?php
        $conn = connDB();
            if($variation)
            {
                for($cnt = 0;$cnt < count($variation) ;$cnt++)
                {
                    if($variation[$cnt]->getUid()==$referrerUidLink)
                    {
                    ?>

                                    <img src="<?php echo "uploads/".$variation[$cnt]->getVariationImage() ?>" class="variation-img2" alt="<?php echo $variation[$cnt]->getVariation();?>" title="<?php echo $variation[$cnt]->getVariation();?>" />
                        
                            <p class="variation-name"><?php echo $variation[$cnt]->getVariation();?></p>
                           
                            <p class="variation-price">
                                RM<?php echo $variation[$cnt]->getVariationPrice();?>.00
                            </p>


                           
                                
                                    <form method="POST">
                                  
                                            <?php echo $productListHtml; ?>
                                                                                                
                                            <button class="peach-button clean ow-iframe-btn">Add to Cart</button>
                                     
                                    </form>
                     
                    <?php
                    }
                }
            }
    ?>
</div>

<div class="clear"></div>

<style>
.social-dropdown {
    width: 360px;
}
.green-footer{
	display:none;}
</style>
<?php include 'js.php'; ?>



<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

</body>
</html>
