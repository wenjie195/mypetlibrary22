<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$timestamp = time();

include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploads_old/'.$timestamp.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {

        $name = "";
        if(isset($Row[0]))
        {
          $name = mysqli_real_escape_string($conn,$Row[1]);
        }
        $price = "";
        if(isset($Row[0]))
        {
          $price = mysqli_real_escape_string($conn,$Row[2]);
        }
        $sku = "";
        if(isset($Row[0]))
        {
          $sku = mysqli_real_escape_string($conn,$Row[3]);
        }
        $age = "";
        if(isset($Row[0]))
        {
          $age = mysqli_real_escape_string($conn,$Row[4]);
        }
        $gender = "";
        if(isset($Row[0]))
        {
          $gender = mysqli_real_escape_string($conn,$Row[5]);
        }

        $vaccinated = "";
        if(isset($Row[0]))
        {
          $vaccinated = mysqli_real_escape_string($conn,$Row[6]);
        }
        $dewormed = "";
        if(isset($Row[0]))
        {
          $dewormed = mysqli_real_escape_string($conn,$Row[7]);
        }
        $color = "";
        if(isset($Row[0]))
        {
          $color = mysqli_real_escape_string($conn,$Row[8]);
        }

        $status = "";
        if(isset($Row[0]))
        {
          $status = mysqli_real_escape_string($conn,$Row[9]);
        }
        $seller = "";
        if(isset($Row[0]))
        {
          $seller = mysqli_real_escape_string($conn,$Row[10]);
        }
        $type = "";
        if(isset($Row[0]))
        {
          $type = mysqli_real_escape_string($conn,$Row[11]);
        }
        $breed = "";
        if(isset($Row[0]))
        {
          $breed = mysqli_real_escape_string($conn,$Row[12]);
        }

        $slug = "";
        if(isset($Row[0]))
        {
          $slug = mysqli_real_escape_string($conn,$Row[13]);
        }
        $link = "";
        if(isset($Row[0]))
        {
          $link = mysqli_real_escape_string($conn,$Row[14]);
        }
        $size = "";
        if(isset($Row[0]))
        {
          $size = mysqli_real_escape_string($conn,$Row[15]);
        }
        $details = "";
        if(isset($Row[0]))
        {
          $details = mysqli_real_escape_string($conn,$Row[16]);
        }

        $uid = md5(uniqid());

        if (!empty($name) || !empty($price) || !empty($sku) || !empty($age) || !empty($gender) || !empty($vaccinated) || !empty($dewormed) || !empty($color) || !empty($status) || !empty($seller) || !empty($type) || !empty($breed) || !empty($slug) || !empty($link) || !empty($size) || !empty($detials) )
        {
          $query = "INSERT INTO pet_details (uid,name,price,sku,age,gender,vaccinated,dewormed,color,status,seller,type,breed,slug,link,size,details) 
                      VALUES ('".$uid."','".$name."','".$price."','".$sku."','".$age."','".$gender."','".$vaccinated."','".$dewormed."','".$color."','".$status."','".$seller."','".$type."','".$breed."','".$slug."','".$link."','".$size."','".$details."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            // echo "<script>alert('Excel File Uploaded !!');window.location='../mypetlibrary22/adminAddKittenExcel_tested.php'</script>";

            $query = "INSERT INTO kitten (uid,name,price,sku,age,gender,vaccinated,dewormed,color,status,seller,breed,slug,link,size,details) 
            VALUES ('".$uid."','".$name."','".$price."','".$sku."','".$age."','".$gender."','".$vaccinated."','".$dewormed."','".$color."','".$status."','".$seller."','".$breed."','".$slug."','".$link."','".$size."','".$details."') ";
            $result = mysqli_query($conn, $query);
            if (! empty($result))
            {
              echo "<script>alert('Excel File Uploaded !!');window.location='../mypetlibrary22/adminAddKittenExcel_tested.php'</script>";
            }
            else 
            {
              echo "<script>alert('Fail to upload excel file !!');window.location='../mypetlibrary22/adminAddKittenExcel_tested.php'</script>";
            }

          }
          else 
          {
            echo "<script>alert('Fail to upload excel file !!');window.location='../mypetlibrary22/adminAddKittenExcel_tested.php'</script>";
          }
        }
      }
    }
  }
  else
  {
    echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../mypetlibrary22/adminAddKittenExcel_tested.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
  <meta property="og:title" content="Add Seller | MPL" />
  <title>Add Seller | MPL</title>
  <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
  <?php include 'css.php'; ?>
</head>


<body class="body">

<?php include 'header.php'; ?>
<div class="next-to-sidebar"> 

  <div class="width100 same-padding menu-distance min-height2 text-center">
  	<h1 class="h1-title green-text">Add Seller</h1>
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
     <input type="file" name="file" id="file" class="choose-file-input" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean blue-gradient-button">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>

<style>
.import-li{
color:#bf1b37;
background-color:white;}
.import-li .hover1a{
display:none;}
.import-li .hover1b{
display:block;}
</style>

<?php include 'js.php'; ?>

<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>

</body>
</html>