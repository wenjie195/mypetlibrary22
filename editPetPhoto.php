<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>MyPetsLibrary | Admin Dashboard</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="E0MmLZFb9bFIyyycHFsCRDVMsXMgRMlRW3Fe6Zk9">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
    </script>
    <!--end::Web font -->

    <!--begin::Global Theme Styles -->
    <link href="https://myadmin.mypetslibrary.com/css/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="https://myadmin.mypetslibrary.com/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="https://myadmin.mypetslibrary.com/css/custom-theme.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->
    <!--begin::Page Vendors Styles -->
    <link href="https://myadmin.mypetslibrary.com/css/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <!-- Vendor CSS -->
   
    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet">  

    <link href="https://cdnjs.cloudflare.com/ajax/libs/venobox/1.8.5/venobox.css" rel="stylesheet">
  
    <!--end::Page Vendors Styles -->
    <link rel="shortcut icon" href="https://myadmin.mypetslibrary.com/img/logo/favicon.ico" />

    <!-- add before </body> -->
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
    <meta name="csrf-token" content="E0MmLZFb9bFIyyycHFsCRDVMsXMgRMlRW3Fe6Zk9">
    
</head>
<!-- end::Head -->    <!-- begin::Body -->
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <!-- BEGIN: Header -->
<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
    <div class="m-container m-container--fluid m-container--full-height">
        <div class="m-stack m-stack--ver m-stack--desktop">

            <!-- BEGIN: Brand -->
            <div class="m-stack__item m-brand  m-brand--skin-dark ">
                <div class="m-stack m-stack--ver m-stack--general">
                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
                        <a href="https://myadmin.mypetslibrary.com/dashboard" class="m-brand__logo-wrapper">
                            <img alt="" src="https://myadmin.mypetslibrary.com/img/logo/MPL.png" />
                        </a>
                    </div>
                    <div class="m-stack__item m-stack__item--middle m-brand__tools">

                        <!-- BEGIN: Left Aside Minimize Toggle -->
                        <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                            <span></span>
                        </a>

                        <!-- END -->

                        <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                        <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                            <span></span>
                        </a>

                        <!-- END -->

                        <!-- BEGIN: Topbar Toggler -->
                        <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                            <i class="flaticon-more"></i>
                        </a>

                        <!-- BEGIN: Topbar Toggler -->
                    </div>
                </div>
            </div>

            <!-- END: Brand -->
            <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">



                <!-- BEGIN: Topbar -->
                <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                    <div class="m-stack__item m-topbar__nav-wrapper">
                        <ul class="m-topbar__nav m-nav m-nav--inline">       
                            <li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">
                                <a href="#" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-nav__link-text">
                                        <img class="m-topbar__language-selected-img" src="https://myadmin.mypetslibrary.com/img/language_flag/020-flag.svg">
                                    </span>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__header m--align-center" style="background: url(https://myadmin.mypetslibrary.com/img/misc/quick_actions_bg.jpg); background-size: cover;">
                                            <span class="m-dropdown__header-subtitle">Select your language</span>
                                        </div>
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav m-nav--skin-light">
                                                    <li class="m-nav__item m-nav__item--active">
                                                        <a href="#" class="m-nav__link m-nav__link--active">

                                                            <span class="m-nav__link-icon"><img class="m-topbar__language-img" src=" https://myadmin.mypetslibrary.com/img/language_flag/020-flag.svg"></span>
                                                            <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">ENG</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="#" class="m-nav__link">
                                                            <span class="m-nav__link-icon"><img class="m-topbar__language-img" src=" https://myadmin.mypetslibrary.com/img/language_flag/015-china.svg"></span>
                                                            <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">CN</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                             m-dropdown-toggle="click">
                                <a href="#" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-topbar__userpic">
                                        <img src="https://myadmin.mypetslibrary.com/img/profile/profile_placeholder.png" class="m--img-rounded m--marginless" alt="" />
                                    </span>
                                    <span class="m-topbar__username m--hide">Nick</span>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__header m--align-center" style="background: url(https://myadmin.mypetslibrary.com/img/misc/user_profile_bg.jpg); background-size: cover;">
                                            <div class="m-card-user m-card-user--skin-dark">
                                                <div class="m-card-user__pic">
                                                    <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt="" />

                                                    <!--
            <span class="m-type m-type--lg m--bg-danger"><span class="m--font-light">S<span><span>
            -->
                                                </div>
                                                <div class="m-card-user__details">
                                                    <span class="m-card-user__name m--font-weight-500">mypetslibrary</span>
                                                    <a href="" class="m-card-user__email m--font-weight-300 m-link"><span class="__cf_email__" data-cfemail="3f5e5b5256517f52464f5a4b4c53565d4d5e4d46115c5052">[email&#160;protected]</span></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav m-nav--skin-light">
                                                    <li class="m-nav__section m--hide">
                                                        <span class="m-nav__section-text">Section</span>
                                                    </li>
                                                
                    
                                                    <li class="m-nav__separator m-nav__separator--fit">
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="https://myadmin.mypetslibrary.com/dashboard" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-info"></i>
                                                            <span class="m-nav__link-text">FAQ</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="https://myadmin.mypetslibrary.com/dashboard" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                            <span class="m-nav__link-text">Support</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__separator m-nav__separator--fit">
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="https://myadmin.mypetslibrary.com/logout" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- END: Topbar -->
            </div>
        </div>
    </div>
</header>
<!-- END: Header -->            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                <div class='notifications top-right'></div>
                <!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

                                                                  <li class="m-menu__item" aria-haspopup="true">
                  
                    <a href="https://myadmin.mypetslibrary.com/dashboard" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-line-graph"></i>
                        <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Dashboard</span>
                        </span>
                        </span> 
                        </i>
                    </a>
                </li>
                                              
                  

                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-network"></i>
                        <span class="m-menu__link-text">Seller</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i></a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/sellers/list" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Sellers Listing</span></a></li>
                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/sellers/create" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Create Seller</span></a></li>
                                                        </ul>
                        </div>
                        </li>

                                              
                  

                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-tabs"></i>
                        <span class="m-menu__link-text">Category</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i></a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/category/list" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Category Listing</span></a></li>
                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/category/create" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Create Category</span></a></li>
                                                        </ul>
                        </div>
                        </li>

                                              
                  

                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-interface-9"></i>
                        <span class="m-menu__link-text">Breed</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i></a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/breed/list" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Breed Listing</span></a></li>
                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/breed/create" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Create Breed</span></a></li>
                                                        </ul>
                        </div>
                        </li>

                                              
                  

                                            <li class=" m-menu__item m-menu__item--submenu m-menu__item--open m-menu__item--expanded m-menu__item--hover" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-cart"></i>
                        <span class="m-menu__link-text">Pets</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i></a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/pets/list" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Pets Listing</span></a></li>
                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/pets/create" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Create Pet</span></a></li>
                                                        </ul>
                        </div>
                        </li>

                                              
                  

                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-suitcase"></i>
                        <span class="m-menu__link-text">Feature Pets</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i></a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/feature/list" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Feature Listing</span></a></li>
                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/feature/create" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Create Feature Pet</span></a></li>
                                                        </ul>
                        </div>
                        </li>

                                              
                  

                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-list-3"></i>
                        <span class="m-menu__link-text">Reviews</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i></a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/reviews/list/pending" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Pending Approval</span></a></li>
                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/reviews/list/approved" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Approved Listing</span></a></li>
                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/reviews/list/rejected" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Rejected Listing</span></a></li>
                                                        </ul>
                        </div>
                        </li>

                                              
                  

                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-calendar"></i>
                        <span class="m-menu__link-text">Blogs</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i></a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/blogs/list" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Blog Listing</span></a></li>
                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/blogs/create" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Create Blog</span></a></li>
                                                        </ul>
                        </div>
                        </li>

                                              
                  

                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-users-1"></i>
                        <span class="m-menu__link-text">Users</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i></a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                                                                                <li class="m-menu__item " aria-haspopup="true">
                                                                <a href="https://myadmin.mypetslibrary.com/users/list" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Listing</span></a></li>
                                                        </ul>
                        </div>
                        </li>

                            

          

        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->                     <div class="m-grid__item m-grid__item--fluid m-wrapper">                    
                    <div class="m-grid__item m-grid__item--fluid m-wrapper">
    
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Pets Detail</h3>

            </div>
            <div>
                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                        <i class="la la-plus m--hide"></i>
                        <i class="la la-ellipsis-h"></i>
                    </a>

            
                </div>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">

                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Add Pets
                                </h3>
                            </div>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="https://myadmin.mypetslibrary.com/pets/create" id="newSellerForm" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="E0MmLZFb9bFIyyycHFsCRDVMsXMgRMlRW3Fe6Zk9">                        <div class="m-portlet__body">
                            <input name="pet_id" type="hidden" value="4088">
                            <div class="form-group m-form__group m--margin-top-10">
                                <div class="alert m-alert m-alert--default" role="alert">
                                    Pets show in front end web. All * are required field
                                </div>
                            </div>

                            <div class="form-group m-form__group ">
                                <label for="exampleInputEmail1" >Pet Name *</label>
                                <input type="text" class="form-control m-input" name="pet_name" id="pet_name" placeholder="Pet name" value="Miniature Schnauzer Puppy For Sale">
                                <span class="m-form__help">Pet name</span>
                            </div>

                            <div class="form-group m-form__group  ">
                                <label for="exampleInputEmail1">SKU</label>
                                <input type="text" class="form-control m-input" name="pet_sku" id="pet_sku" placeholder="SKU number" value="JUN-MP-28">
                                <span class="m-form__help">SKU no</span>
                            </div>

                            <div class="form-group m-form__group ">
                                <label for="exampleInputEmail1" >Pet Slug *</label>
                                <input type="text" class="form-control m-input" name="slug" id="slug" placeholder="Slug" value="miniature-schnauzer-puppy-for-sale--jun-mp-28" disabled>
                                <span class="m-form__help">Unique Slug for URL: Best practice name-sometext * avoid white space</span>
                            </div>
                            
                            <div class="form-group m-form__group  ">
                                <label for="exampleInputEmail1">Pet Price</label>
                                <input type="text" class="form-control m-input" name="pet_price" id="pet_price" placeholder="Pet price" value="2XXX">
                                <span class="m-form__help">Pricing for the pet</span>
                            </div>



                            <div class="form-group m-form__group  ">
                                <label for="exampleInputEmail1">Pet age (in months) *</label>
                                <input type="text" class="form-control m-input" name="pet_age" id="pet_age" placeholder="Pet age" value="2 months">
                                <span class="m-form__help">Pet age</span>
                            </div>

                      <!--       <div class="form-group m-form__group  ">
                                <label >additional informaion *</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <textarea class="form-control m-input summernote" id="summernote_hide" name="other_info" value=""></textarea>
                                </div>
                            </div>
 -->
                            <div class="form-group m-form__group  ">
                                <label >more informaion *</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <textarea class="form-control m-input summernote" id="summernote_hide_two" name="more_info" value=""></textarea>
                                </div>
                            </div>

                            <div class="form-group m-form__group">
                                <label for="exampleSelect1">Vaccinated Status *</label>
                                <select class="form-control m-input m-input--square" id="pet_vaccinated" name="pet_vaccinated">
                                    <option value="1"  selected >YES</option>
                                    <option value="0"   >NO</option>
                                </select>
                            </div>

                            <div class="form-group m-form__group">
                                <label for="exampleSelect1">dewormed Status</label>
                                <select class="form-control m-input m-input--square" id="pet_dewormed" name="pet_dewormed">
                                    <option value="1" selected>YES</option>
                                    <option value="0" >NO</option>
                                </select>
                            </div>

                            <div class="form-group m-form__group">
                                <label for="exampleSelect1">gender</label>
                                <select class="form-control m-input m-input--square" id="pet_dewormed" name="pet_gender">
                                    <option value="1" selected>Male</option>
                                    <option value="0" >Female</option>
                                </select>
                            </div>

                            <div class="form-group m-form__group  ">
                                <label for="exampleInputEmail1">Pet color *</label>
                                <input type="text" class="form-control m-input" name="pet_color" id="pet_color" placeholder="Pet color" value="Salt &amp; Pepper">
                                <span class="m-form__help">Pet color</span>
                            </div>

                            <div class="form-group m-form__group">
                                <label>Pet Size *</label>
                                    <div class="m-radio-list">                                
                                        <label class="m-radio m-radio--bold m-radio--state-success"> 
                                            <input type="radio" name="rad_pet_size" value="1" checked > Small
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--bold m-radio--state-success">
                                            <input type="radio" name="rad_pet_size" value="2" > Medium
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--bold m-radio--state-success">
                                            <input type="radio" name="rad_pet_size" value="3" > Large
                                            <span></span>
                                        </label>
                                    </div>
                            <span class="m-form__help">Pet Size</span>
                            </div>

                            <div class="form-group m-form__group  ">
                                <label for="exampleInputEmail1">Video URL *</label>
                                <input type="text" class="form-control m-input" name="pet_video_url" id="pet_video_url" placeholder="Video URL" value="https://player.vimeo.com/video/431528497">
                                <span class="m-form__help">Video URLr</span>
                            </div>
                            
                             <div class="form-group m-form__group  ">
                                <label for="exampleInputEmail1">Publish Date*</label>
                                <input type="text" class="form-control" name="published_date" id="published_date" readonly="" placeholder="Select date" value="2020-06-23 00:00:00">
                            </div>

                            <div class="form-group m-form__group">
                                <label for="exampleSelect1">Publish Status</label>
                                <select class="form-control m-input m-input--square" id="exampleSelect1" name="pet_publish_status">
                                    <option value="1" selected>Publish</option>
                                    <option value="0" >Suspend</option>
                                </select>
                            </div>
 
                            <div class="form-group m-form__group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="d-block">Add Pets Images</label>
                                        <button type="button" class="btn btn-brand" id="UploadBtn">
                                            Browse
                                        </button>
                                    </div>
                                 
                                    <div class="col-md-9">
                                        <div id="UploadPreview" class="row align-items-end">
                                                                                            <div class="upload-preview col-md-3 m--margin-top-10" id="Upload_">
                                                    <input type="hidden" name="upload[0][project_id]" value="4088" />
                                                    <input type="hidden" name="upload[0][asset_id]" value="18814" />
                                                    <input type="hidden" name="upload[0][file]" value="" class="upload_file" />
                                                    <input type="hidden" name="upload[0][delete]" value="0" class="upload_delete" />

                                                    <img src="https://mypetslibrary.s3.ap-southeast-1.amazonaws.com/images/pets/2020_06_2315929124800.jpg" alt="preview" style="width: 100%; max-width: 500px" />
                                                    <div class="upload-preview-actions m--margin-top-5">
                                                        <a href="#" class="image-edit m-link m-link--state m-link--brand" style="margin: 0 5px;">Edit</a>
                                                        <a href="#" class="image-delete m-link m-link--state m-link--danger" style="margin: 0 5px;">Delete</a>
                                                    </div>
                                                    <div class="upload-preview-actions-delete m--margin-top-5">
                                                        <span class="m--font-danger" style="margin: 0 5px;">DELETED</span>
                                                        <a href="#" class="image-undo m-link m-link--state m-link--success" style="margin: 0 5px;">Undo</a>
                                                    </div>
                                                </div>
                                                                                            <div class="upload-preview col-md-3 m--margin-top-10" id="Upload_">
                                                    <input type="hidden" name="upload[1][project_id]" value="4088" />
                                                    <input type="hidden" name="upload[1][asset_id]" value="18815" />
                                                    <input type="hidden" name="upload[1][file]" value="" class="upload_file" />
                                                    <input type="hidden" name="upload[1][delete]" value="0" class="upload_delete" />

                                                    <img src="https://mypetslibrary.s3.ap-southeast-1.amazonaws.com/images/pets/2020_06_2315929124801.jpg" alt="preview" style="width: 100%; max-width: 500px" />
                                                    <div class="upload-preview-actions m--margin-top-5">
                                                        <a href="#" class="image-edit m-link m-link--state m-link--brand" style="margin: 0 5px;">Edit</a>
                                                        <a href="#" class="image-delete m-link m-link--state m-link--danger" style="margin: 0 5px;">Delete</a>
                                                    </div>
                                                    <div class="upload-preview-actions-delete m--margin-top-5">
                                                        <span class="m--font-danger" style="margin: 0 5px;">DELETED</span>
                                                        <a href="#" class="image-undo m-link m-link--state m-link--success" style="margin: 0 5px;">Undo</a>
                                                    </div>
                                                </div>
                                                                                            <div class="upload-preview col-md-3 m--margin-top-10" id="Upload_">
                                                    <input type="hidden" name="upload[2][project_id]" value="4088" />
                                                    <input type="hidden" name="upload[2][asset_id]" value="18816" />
                                                    <input type="hidden" name="upload[2][file]" value="" class="upload_file" />
                                                    <input type="hidden" name="upload[2][delete]" value="0" class="upload_delete" />

                                                    <img src="https://mypetslibrary.s3.ap-southeast-1.amazonaws.com/images/pets/2020_06_2315929124802.jpg" alt="preview" style="width: 100%; max-width: 500px" />
                                                    <div class="upload-preview-actions m--margin-top-5">
                                                        <a href="#" class="image-edit m-link m-link--state m-link--brand" style="margin: 0 5px;">Edit</a>
                                                        <a href="#" class="image-delete m-link m-link--state m-link--danger" style="margin: 0 5px;">Delete</a>
                                                    </div>
                                                    <div class="upload-preview-actions-delete m--margin-top-5">
                                                        <span class="m--font-danger" style="margin: 0 5px;">DELETED</span>
                                                        <a href="#" class="image-undo m-link m-link--state m-link--success" style="margin: 0 5px;">Undo</a>
                                                    </div>
                                                </div>
                                                                                            <div class="upload-preview col-md-3 m--margin-top-10" id="Upload_">
                                                    <input type="hidden" name="upload[3][project_id]" value="4088" />
                                                    <input type="hidden" name="upload[3][asset_id]" value="18817" />
                                                    <input type="hidden" name="upload[3][file]" value="" class="upload_file" />
                                                    <input type="hidden" name="upload[3][delete]" value="0" class="upload_delete" />

                                                    <img src="https://mypetslibrary.s3.ap-southeast-1.amazonaws.com/images/pets/2020_06_2315929124803.jpg" alt="preview" style="width: 100%; max-width: 500px" />
                                                    <div class="upload-preview-actions m--margin-top-5">
                                                        <a href="#" class="image-edit m-link m-link--state m-link--brand" style="margin: 0 5px;">Edit</a>
                                                        <a href="#" class="image-delete m-link m-link--state m-link--danger" style="margin: 0 5px;">Delete</a>
                                                    </div>
                                                    <div class="upload-preview-actions-delete m--margin-top-5">
                                                        <span class="m--font-danger" style="margin: 0 5px;">DELETED</span>
                                                        <a href="#" class="image-undo m-link m-link--state m-link--success" style="margin: 0 5px;">Undo</a>
                                                    </div>
                                                </div>
                                                                                    </div>
                                    </div>


                                </div>
                                
                                <input type="file" id="UploadInput" style="position: absolute; opacity: 0; left: -9999px;" multiple />
                            </div>

                            <div class="form-group m-form__group">
                                <label>is sold *</label>
                                    <div class="m-radio-list">                                
                                        <label class="m-radio m-radio--bold m-radio--state-success">
                                            <input type="radio" name="rad_sold_status" value="0" checked="checked"> Available
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--bold m-radio--state-danger">
                                            <input type="radio" name="rad_sold_status" value="1" checked=""> Sold
                                            <span></span>
                                        </label>
                                    </div>
                            <span class="m-form__help">Account status , open or suspended</span>
                            </div>

                            <div class="form-group m-form__group">
                                <label>Pet Category</label>
                                <select class="form-control m-input m-input--square" id="custom_category" name="custom_category">
                                        <option value="">--Select--</option>
                                                                          
                                                                                     <option value="1" selected="selected">Puppies</option>
                                                                                                                  
                                                                                     <option value="2">Kittens</option>
                                                                                                            </select>
                            </div>
                     
                            <div class="form-group m-form__group">
                                <label>Pet Breed</label>
                                <select class="custombreed form-control m-input m-input--square" id="custom_breed" name="custom_breed">
                                                                                                            <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="44" selected="selected">Schnauzer</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                                                                <option value="">--Select--</option>
                                                                                                    </select>
                            </div>

                            <div class="form-group m-form__group">
                                <label>Seller</label>
                                <select class="form-control m-input m-input--square" id="seller_id" name="seller_id">
                                        <option value="">--Select--</option>
                                                                                                                        <option value="1">A Famosa Pet House</option>
                                                                                                                                                                <option value="2">Albipetzone</option>
                                                                                                                                                                <option value="3">Ardonia Marketing</option>
                                                                                                                                                                <option value="4">AZ Pets Empire</option>
                                                                                                                                                                <option value="5">D&amp;K Pet Centre</option>
                                                                                                                                                                <option value="6">Dorzwer Pet Shop</option>
                                                                                                                                                                <option value="7">Durain Sam</option>
                                                                                                                                                                <option value="8">Fur</option>
                                                                                                                                                                <option value="9">Furrbulus</option>
                                                                                                                                                                <option value="10">Happiness Pet House</option>
                                                                                                                                                                <option value="11">Happy Paws</option>
                                                                                                                                                                <option value="12">Happy Pets Mart</option>
                                                                                                                                                                <option value="13">Harmony Pet House</option>
                                                                                                                                                                <option value="14">Infinite Bully</option>
                                                                                                                                                                <option value="15">J Puppies Corner</option>
                                                                                                                                                                <option value="16">J2 Pet House</option>
                                                                                                                                                                <option value="17">Joe&#039;s Pet</option>
                                                                                                                                                                <option value="18">Klang Pet House</option>
                                                                                                                                                                <option value="19">Kooi Lim Pet Kingdom</option>
                                                                                                                                                                <option value="20">L &amp; Lee Pets</option>
                                                                                                                                                                <option value="21">LEC Pets World</option>
                                                                                                                                                                <option value="22">Liewworldkenn</option>
                                                                                                                                                                <option value="23">LK Pom</option>
                                                                                                                                                                <option value="24">Love Nest Pet Shop</option>
                                                                                                                                                                <option value="25">Lucky 19 Pet</option>
                                                                                                                                                                <option value="26" selected="selected">May Pet Station</option>
                                                                                                                                                                <option value="27">Mr JJ Pet Centre</option>
                                                                                                                                                                <option value="28">Pandora Pet Shop</option>
                                                                                                                                                                <option value="29">Pawfurry</option>
                                                                                                                                                                <option value="30">Pet Store 1393</option>
                                                                                                                                                                <option value="31">PetsValley</option>
                                                                                                                                                                <option value="32">Petswow Dreamland</option>
                                                                                                                                                                <option value="33">Puppy Asia</option>
                                                                                                                                                                <option value="34">Pure Pretty</option>
                                                                                                                                                                <option value="35">Sam Chong</option>
                                                                                                                                                                <option value="36">Shun Shun</option>
                                                                                                                                                                <option value="37">The Love Ones</option>
                                                                                                                                                                <option value="38">The One Kennel - Precious Pets</option>
                                                                                                                                                                <option value="39">The Pet Shop</option>
                                                                                                                                                                <option value="40">Dept Pet Grooming House</option>
                                                                                                                                                                <option value="41">Potato&#039;s Pets World</option>
                                                                                                                                                                <option value="53">Paw &amp; Tail</option>
                                                                                                                                                                <option value="54">Tommy Pet Station</option>
                                                                                                                                                                <option value="55">Pets Avenue</option>
                                                                                                                                                                <option value="56">Win Win Pet House</option>
                                                                                                                                                                <option value="57">Yuniken</option>
                                                                                                                                                                <option value="58">Catrosaz Cattery</option>
                                                                                                                                                                <option value="59">Little Asia Pet Station</option>
                                                                                                                                                                <option value="60">R.I.A World Kennel Pet Shop</option>
                                                                                                                                                                <option value="61">Baby Pet Hub</option>
                                                                                                                                                                <option value="62">Gold Star Beauty Petstore - You &amp; Me Pets</option>
                                                                                                                                                                <option value="63">Pet N Go Cat Centre</option>
                                                                                                            </select>
                            </div>

 
                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button id="createSubmit" type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>

        </div>
    </div>
</div>

<link href="https://myadmin.mypetslibrary.com/css/croppie.css" rel="stylesheet" />
<style>
    #UploadPreview .upload-preview{
        position: relative;
    }
    #UploadPreview .upload-preview.sortable-ghost{
        opacity: .3;
    }
    #UploadPreview .upload-preview .upload-preview-actions-delete{
        display: none;
    }
    #UploadPreview .upload-preview img{
        transition: opacity .3s;
    }
    #UploadPreview .upload-preview.__deleted img{
        opacity: .2;
    }
    #UploadPreview .upload-preview.__deleted .upload-preview-actions{
        display: none;
    }
    #UploadPreview .upload-preview.__deleted .upload-preview-actions-delete{
        display: block;
    }
</style>


<div class="modal" id="UploadModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="CroppieEl"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-brand" id="CroppieBtn" >
                    Save
                </button>
            </div>
        </div>
    </div>
</div>

<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/tmpl" id="UploadTmpl">
<div class="upload-preview col-md-3 m--margin-top-10" id="Upload_{unique_id}">
    <input type="hidden" name="upload[{index}][id]" value="" />
    <input type="hidden" name="upload[{index}][file]" value="{src}" class="upload_file" />
    <input type="hidden" name="upload[{index}][delete]" value="0" class="upload_delete" />
    <img src="{src}" alt="preview" style="width: 100%;" />
    <div class="upload-preview-actions m--margin-top-5">
        <a href="#" class="image-edit m-link m-link--state m-link--brand" style="margin: 0 5px;">Edit</a>
        <a href="#" class="image-delete m-link m-link--state m-link--danger" style="margin: 0 5px;">Delete</a>
    </div>
    <div class="upload-preview-actions-delete m--margin-top-5">
        <span class="m--font-danger" style="margin: 0 5px;">DELETED</span>
        <a href="#" class="image-undo m-link m-link--state m-link--success" style="margin: 0 5px;">Undo</a>
    </div>
</div>
</script>

<script type="text/javascript" src="https://myadmin.mypetslibrary.com/js/sortable.js"></script>
<script type="text/javascript" src="https://myadmin.mypetslibrary.com/js/croppie.min.js"></script>


                </div>
            </div>
            <!-- end:: Body -->
 
        </div>
        <!-- end:: Page -->
        <!-- begin::Scroll Top -->
        <div id="m_scroll_top" class="m-scroll-top">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->
        <!--begin::Global Theme Bundle -->
        <script src="https://myadmin.mypetslibrary.com/js/vendors.bundle.js" type="text/javascript"></script>
        <script src="https://myadmin.mypetslibrary.com/js/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Global Theme Bundle -->
        <!--end::Page Vendors -->
        <!--begin::Page Scripts -->
        <script src="https://myadmin.mypetslibrary.com/js/page/dashboard.js" type="text/javascript"></script>
        <script src="https://myadmin.mypetslibrary.com/js/bootstrap-notify.js" type="text/javascript"></script>
        <script src="https://myadmin.mypetslibrary.com/js/custom.js" type="text/javascript"></script>
        <script src="https://myadmin.mypetslibrary.com/js/sortable.js" type="text/javascript" ></script>
        <script src="https://myadmin.mypetslibrary.com/js/croppie.min.js"  type="text/javascript" ></script>
        <script src="https://myadmin.mypetslibrary.com/js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="https://myadmin.mypetslibrary.com/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <!--end::Page Scripts -->

        <!-- Universal Flash For Bootsrap Notify Plugin -->           
        <script>
                    </script>    
        <!-- End Universal Flash -->
        <!-- Start Summernote Options -->
        <script type="text/javascript">
            $(".summernote").summernote({
                        height:150,
                        toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link', 'picture']],
                ['help', ['help']]
              ]
            ,disableDragAndDrop :true,
            popover: {
             image: [],
             link: [],
             air: []
           }
        });

        FilePond.registerPlugin(FilePondPluginImagePreview);
        FilePond.registerPlugin(FilePondPluginFileEncode);
        FilePond.setOptions({
            instantUpload: false,
        });
        const inputElement = document.getElementById('pondInput');
        const pond = FilePond.create( inputElement ,{
            maxFiles: 1,
            allowBrowse: true,
            allowFileEncode:true,
            // server: {
            //     url: '/upload',
            //     process: {
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         }
            //     }
            // }
        });

        // $('#createSubmit').click(function(){ //listen for submit event
        //     console.log('hi form did submit' + pond.getFiles());
        //     $("#pondInput").val(function() {
        //             return pond.getFiles();
        //      });
        //     return true;
        // }); 
        $(document).ready(function(){
            console.log('document rdy');
                $('.venobox').venobox(); 
                $('#custom_category').select2();
                $('#custom_pet_id').select2();
                $('#custom_state').select2();
                $('#custom_breed').select2();
                $('#custom_category').on('select2:select',function(e){

                    var data = e.target.value;
                     
                    $.ajax({
                        type:'POST',
                        url:'/breed/ajax',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: {id:data},
                        success:function(result){
                               
                                $("#custom_breed").empty();
                                
                                var dbSelect = $('#custom_breed');
                                var newOptions = '<option value="">-- Select --</option>';

                                dbSelect.empty();
                         
                                for (var i = 0; i < result.length; i++) {
                                    newOptions += '<option value="'+ result[i].id + '">'+ result[i].text + '</option>';
                                }


                                dbSelect.html(newOptions);
                                dbSelect.select2();

                          
                        }
                    }); 

                });




                 // Upload handler
                var $uploadBtn = $('#UploadBtn');
                var $uploadPreview = $('#UploadPreview');
                var $uploadInput = $('#UploadInput');
                var $uploadTmpl = $('#UploadTmpl');
                var $uploadModal = $('#UploadModal');
                
                $uploadBtn.on('click', function() {
                    console.log('did press add images');
                    $uploadInput.trigger('click');
                });

                $uploadInput.on('change', function(e) {
                    var files = this.files;

                    if (files) {
                        for (var i = 0, f; f = files[i]; i++) {
                            readFile(f, function(resp) {
                                var tmpl = $uploadTmpl.html()
                                            .replace(/{src}/g, resp)
                                            .replace(/{index}/g, $uploadPreview.children().length)
                                            .replace(/{unique_id}/g, new Date().getTime());
                                $(tmpl).appendTo($uploadPreview);
                            });
                        }
                    }
                });

                $uploadPreview
                    .on('click', '.image-edit', function(e) {
                        e.preventDefault();
                        var src = $(this).closest('.upload-preview').children('img').attr('src');
                        $croppieTarget = $(this).closest('.upload-preview');

                        $croppie.croppie('bind', {
                            url: src
                        });

                        $uploadModal.modal({
                            keyboard: false
                        });
                    })
                    .on('click', '.image-delete', function(e) {
                        e.preventDefault();
                        $(this).closest('.upload-preview').addClass('__deleted')
                            .children('.upload_delete').val(1);
                    })
                    .on('click', '.image-undo', function(e) {
                        e.preventDefault();
                        $(this).closest('.upload-preview').removeClass('__deleted')
                            .children('.upload_delete').val(0);
                    });


                // croppie
                var $croppieEl = $('#CroppieEl');
                var $croppieBtn = $('#CroppieBtn');
                var $croppieTarget = null;

                var $croppie = $croppieEl.croppie({
                    enableExif: true,
                    enforceBoundary : true,
                    mouseWheelZoom: false,
                    viewport: {
                        width: 500,
                        height: 500
                    },
                    boundary: {
                        width: 500,
                        height: 500
                    }
                });

                $croppieBtn.on('click', function() {
                    if ($croppieTarget) {
                        $croppie.croppie('result', {
                            type: 'base64',
                            size: 'original',
                            quality: 0.8,
                            format: 'jpeg',
                            backgroundColor : "#ffffff"
                        }).then(function (resp) {
                            // update target value
                            $croppieTarget.children('img').attr('src', resp);
                            $croppieTarget.children('.upload_file').val(resp);

                            // hide modal
                            $uploadModal.modal('hide');
                        });
                    }
                });

                // enable sortable
                var sortable = new Sortable($uploadPreview[0], {
                    onEnd: function(e) {
                        // re-order childs index
                        $uploadPreview.children().each(function(index) {
                            $(this).find('input[type=hidden]').each(function() {
                                var name = $(this).attr('name').replace(/\[[0-9]+\]/, '[' + index + ']');
                                $(this).attr('name', name);
                            });
                        });
                    }
                });


                // methods
                function readFile(file, cb) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        cb(e.target.result);
                    };
                    reader.readAsDataURL(file);
                }



    
        });
        </script>
        <!-- End Summernote Options -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/venobox/1.8.5/venobox.js"></script>
        <script type="text/javascript" src="https://myadmin.mypetslibrary.com/js/select2.js"></script>
        <!-- end::Body -->
    </body>

    <!-- begin::Footer -->
    <footer class="m-grid__item     m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                    <span class="m-footer__copyright">
                        2019 &copy; mypetslibrary.com
                    </span>
                </div>

                <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                    <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                        <li class="m-nav__item m-nav__item">
                            <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                                <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
<!-- end::Footer -->
</html>