<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Variation.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Slider.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$slider = getSlider($conn," WHERE status = 'Show' ");

// $puppies = getPuppy($conn, "WHERE featured_seller = 'Yes' ORDER BY date_created DESC LIMIT 10 ");
// $kittens = getKitten($conn, "WHERE featured_seller = 'Yes' ORDER BY date_created DESC LIMIT 10 ");

$puppies = getPuppy($conn, "WHERE featured_seller = 'Yes' AND status != 'Deleted' ORDER BY date_created DESC LIMIT 10 ");
$kittens = getKitten($conn, "WHERE featured_seller = 'Yes' AND status != 'Deleted' ORDER BY date_created DESC LIMIT 10 ");
$products = getProduct($conn, "WHERE featured_product = 'Yes' ORDER BY date_created DESC LIMIT 10 ");
$variationDetails = getVariation($conn);
$seller = getSeller($conn , "WHERE featured_seller = 'Yes' AND account_status = 'Active' ORDER BY date_created DESC LIMIT 10 ");
$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Online Pet Store | Mypetslibrary" />
<title>Online Pet Store | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">

<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
 <link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'header.php'; ?>
<div class="overflow container12">
    <div id="jssor_1" style="position:relative;left:0px;width:649px;height:250px;overflow:hidden;visibility:hidden;" class="menu-distance">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:649px;height:250px;text-align:center;background-color:rgba(0,0,0,0.7);">
           
        </div>

        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:649px;height:250px;overflow:hidden;">
            <?php
            $conn = connDB();
            if($slider)
            {
                for($cnt = 0;$cnt < count($slider) ;$cnt++)
                {
                ?>
                    <!-- <a href="https://pariepets.com/cat.html" target="_blank"> -->
                    <a href="<?php echo $slider[$cnt]->getLink();?>" target="_blank">
                        <div>
                          <img data-u="image" src="uploadsSlider/<?php echo $slider[$cnt]->getImgName();?>" class="pointer opacity-hover" />
                        </div>
                    </a>
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>
        </div>

        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
</div>
<div class="clear"></div>

<div class="overflow">
  <div class="width100 small-padding">
    <h1 class="user-title left-align-title slider-h1">Puppies</h1>
    <a href="malaysia-cute-puppy-dog.php" class="right-align-link view-a light-green-a hover-a slider-h1-a">View More</a>
  </div>

  <div class="clear"></div>
    <div class="width100 small-padding glider-contain">
        <div class="glider glider1">
      <?php
      $conn = connDB();
      if($puppies)
      {
        // echo $aaaa = count($puppies);
        for($cnt = 0;$cnt < count($puppies) ;$cnt++)
        {
        ?>

          <!-- <a href='puppyDetails.php?id=<?php echo $puppies[$cnt]->getUid();?>'> -->
          <a href='puppyDogForSale.php?id=<?php echo $puppies[$cnt]->getUid();?>'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class="width100 white-bg image-box-size">
                <img src="uploads/<?php echo $puppies[$cnt]->getDefaultImage();?>" alt="<?php echo $puppies[$cnt]->getBreed();?>" title="<?php echo $puppies[$cnt]->getBreed();?>" class="width100 two-border-radius">
              </div>
                        <?php 
                            $status = $puppies[$cnt]->getStatus();
                            if($status == 'Sold')
                            {
                            ?>
                                <div class="sold-label">Sold</div>
                            <?php
                            }
                            else
                            {}
                        ?>

              <div class="width100 product-details-div">

                <?php 
                  $pupGender = $puppies[$cnt]->getGender();
                  if($pupGender == 'Female')
                  {
                    $puppyGender = 'F';
                  }
                  elseif($pupGender == 'Male')
                  {
                    $puppyGender = 'M';
                  }
                ?>

                <p class="width100 text-overflow slider-product-name"><?php echo $puppies[$cnt]->getBreed();?> | <?php echo $puppyGender ;?></p>
                <p class="slider-product-name text-center">
                  RM<?php $length = strlen($puppies[$cnt]->getPrice());?>
                  <?php 
                    if($length == 2)
                    {
                      $hiddenPrice = "X";
                    }
                    elseif($length == 3)
                    {
                      $hiddenPrice = "XX";
                    }
                    elseif($length == 4)
                    {
                      $hiddenPrice = "XXX";
                    }
                    elseif($length == 5)
                    {
                      $hiddenPrice = "XXXX";
                    }
                    elseif($length == 6)
                    {
                      $hiddenPrice = "XXXXX";
                    }
                    elseif($length == 7)
                    {
                      $hiddenPrice = "XXXXXX";
                    }
                  ?>
                  <?php echo substr($puppies[$cnt]->getPrice(),0,1); echo $hiddenPrice;?>
                </p>
              </div>
            </div>
          </a>

        <?php
        }
        ?>
      <?php
      }
      $conn->close();
      ?>
        </div>
    </div>



</div>

<div class="clear"></div>

<div class="overflow grey-bg">
    <div class="width100 small-padding">
    <h1 class="user-title left-align-title slider-h1">Kittens</h1>
    <a href="malaysia-cute-kitten-cat.php" class="right-align-link view-a light-green-a hover-a slider-h1-a">View More</a>
    </div>
    <div class="clear"></div>
    <div class="width100 small-padding glider-contain">
    <div class="glider glider2">

    <?php
      $conn = connDB();
      if($kittens)
      {
        for($cntAA = 0;$cntAA < count($kittens) ;$cntAA++)
        {
        ?>
          <!-- <a href='kittenDetails.php?id=<?php //echo $kittens[$cntAA]->getUid();?>'> -->
          <a href='kittenCatForSale.php?id=<?php echo $kittens[$cntAA]->getUid();?>'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class="width100 white-bg image-box-size">
                <img src="uploads/<?php echo $kittens[$cntAA]->getDefaultImage();?>" alt="<?php echo $kittens[$cntAA]->getBreed();?>" title="<?php echo $kittens[$cntAA]->getBreed();?>" class="width100 two-border-radius">
              </div>
                         <?php 
                            $statusB = $kittens[$cntAA]->getStatus();
                            if($statusB == 'Sold')
                            {
                            ?>
                                <div class="sold-label">Sold</div>
                            <?php
                            }
                            else
                            {}
                        ?>

              <div class="width100 product-details-div">

                <?php 
                  $kitGender = $kittens[$cntAA]->getGender();
                  if($kitGender == 'Female')
                  {
                    $kittenGender = 'F';
                  }
                  elseif($kitGender == 'Male')
                  {
                    $kittenGender = 'M';
                  }
                ?>

                <p class="width100 text-overflow slider-product-name"><?php echo $kittens[$cntAA]->getBreed();?> | <?php echo $kittenGender ;?></p>
                <p class="slider-product-name text-center">
                  RM<?php $lengthK = strlen($kittens[$cntAA]->getPrice());?>
                  <?php 
                    if($lengthK == 2)
                    {
                      $hiddenPriceK = "X";
                    }
                    elseif($lengthK == 3)
                    {
                      $hiddenPriceK = "XX";
                    }
                    elseif($lengthK == 4)
                    {
                      $hiddenPriceK = "XXX";
                    }
                    elseif($lengthK == 5)
                    {
                      $hiddenPriceK = "XXXX";
                    }
                    elseif($lengthK == 6)
                    {
                      $hiddenPriceK = "XXXXX";
                    }
                    elseif($lengthK == 7)
                    {
                      $hiddenPriceK = "XXXXXX";
                    }
                  ?>
                  <?php echo substr($kittens[$cntAA]->getPrice(),0,1); echo $hiddenPriceK;?>
                </p>
              </div>
            </div>
          </a>
        <?php
        }
        ?>
      <?php
      }
      $conn->close();
      ?>


    </div>
</div>
</div>
<div class="clear"></div>
    <div class="width100 small-padding">
    	<h1 class="user-title left-align-title slider-h1">Products</h1>
    </div>
    <div class="clear"></div>
    <div class="width100 small-padding glider-contain ow-product-gilder">
    <div class="glider glider3">

    <!--<?php
      $conn = connDB();
      $max =0;
      $min =0;
      if($products && $variationDetails)
      {
        for($cntPro = 0;$cntPro < count($products) ;$cntPro++)
        {
          $min_count=0;
          for($count = 0;$count < count($variationDetails) ;$count++)
          {
            if($products[$cntPro]->getUid() == $variationDetails[$count]->getProductId())
            {
              if( $max < $variationDetails[$count]->getVariationPrice()){
                $max = $variationDetails[$count]->getVariationPrice() ;
              }else{
                  $max = $max ;
              }
            
              if($min_count == 0){
                  $min = $variationDetails[$count]->getVariationPrice();
              }
              else{
                if($min < $variationDetails[$count]->getVariationPrice()){
                    $min = $min ;
                }else{
                    $min = $variationDetails[$count]->getVariationPrice() ;
                }
              }
              $min_count++;
            }
          }
          ?>
        
          <a href='productDetails.php?id=<?php echo $products[$cntPro]->getUid();?>'>
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg"> 
                  <img src="uploads/<?php echo $products[$cntPro]->getImageOne();?>" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                </div>
                <div class="width100 product-details-div product-index-big-div">
                  <p class="width100 text-overflow slider-product-name"><?php echo $products[$cntPro]->getName();?></p>
                  
                  <?php if( $max == $min){
                  ?> 
                    <p class="slider-product-name">RM<?php echo $max;?><p>
                  <?php
                  }
                  else{
                  ?>
                    <p class="slider-product-name">RM<?php echo $min;?> - RM<?php echo $max;?><p>
                  <?php
                  }
                  ?>
                </div>
              </div>
        </a> 
            
        <?php
        $max =0;
        $min =0;
        }
        ?>
      <?php
      }
      $conn->close();
      ?>
      
      -->

          	
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/milk-for-puppy.png"  class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">PARIE Goat Milk Powder for Puppies</p>

                    <p class="slider-product-name">Coming Soon</p>


                </div>
              </div>
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/milk-for-kitten.png" class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">PARIE Goat Milk Powder for Kittens & Cats</p>

                    <p class="slider-product-name">Coming Soon</p>


                </div>
              </div>
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/product-1.jpg" class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">Pedigree Dentastix (For 3 - 12 Months)</p>

                    <p class="slider-product-name">Coming Soon<p>


                </div>
              </div>
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/product-2.jpg" class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">Pedigree Daily Dentastix</p>

                    <p class="slider-product-name">Coming Soon</p>


                </div>
              </div>              
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/product-3.jpg" class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">Pedigree Dentastix For Medium Size Dog</p>

                    <p class="slider-product-name">Coming Soon</p>


                </div>
              </div>      
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/product-4.jpg" class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Daily Oral Care</p>

                    <p class="slider-product-name">Coming Soon</p>


                </div>
              </div>                
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/product-5.jpg" class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">Pedigree Adult Complete Nutrition</p>

                    <p class="slider-product-name">Coming Soon</p>


                </div>
              </div>       
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/product-6.jpg" class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">Pedigree Schmackos</p>

                    <p class="slider-product-name">Coming Soon</p>


                </div>
              </div>                 
              <div class="shadow-white-box product-box opacity-hover">
                <div class="width100 white-bg image-box-size"> 
                  <img src="img/product-6.jpg" class="width100 two-border-radius">
                </div>
                <div class="sold-label coming-label">Coming Soon</div>
                <div class="width100 product-details-div">
                  <p class="width100 text-overflow slider-product-name">Pedigree Small Dog Complete Nutrition</p>

                    <p class="slider-product-name">Coming Soon</p>


                </div>
              </div>               
              
                              
  </div>
</div>
<div class="clear"></div>


<div class="width100 small-padding">

  <h1 class="green-text user-title left-align-title">Our Partners</h1>
  
  <div class="clear"></div>

  <div class="index-partner-big-div2">
		<img src="img/partner-section.jpg" class="width100 partner-section" alt="Partner" title="Partner">
       <!--<//?php
    $conn = connDB();
    if($seller)
    {
      for($cntAA = 0;$cntAA < count($seller) ;$cntAA++)
      {
      ?>
        <a href='petSellerDetails.php?id=<?php echo $seller[$cntAA]->getSlug();?>' class="opacity-hover">
          <img src="uploads/<?php echo $seller[$cntAA]->getCompanyLogo();?>" alt="<?php echo $seller[$cntAA]->getCompanyName();?>" title="<?php echo $seller[$cntAA]->getCompanyName();?>" class="partner-img">
        </a> 
      <//?php
      }
      ?>
    <//?php
    }
    $conn->close();
    ?>
  </div>
  
  <div class="width100">
    	<a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner1.png" alt="Partner Name" title="Partner" class="partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner2.png" alt="Partner Name" title="Partner" class="partner-img second-partner-img"></a>
    	<a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner3.png" alt="Partner Name" title="Partner" class="partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner4.png" alt="Partner Name" title="Partner" class="partner-img second-partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner5.png" alt="Partner Name" title="Partner" class="partner-img"></a>
    </div>
    <div class="width100">
    	<a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner1.png" alt="Partner Name" title="Partner" class="partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner2.png" alt="Partner Name" title="Partner" class="partner-img second-partner-img"></a>
    	<a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner3.png" alt="Partner Name" title="Partner" class="partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner4.png" alt="Partner Name" title="Partner" class="partner-img second-partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner5.png" alt="Partner Name" title="Partner" class="partner-img"></a>
    </div>    -->
	</div>
    <div class="clear"></div>

    <a href="pet-seller-grooming-delivery-hotel.php"><div class="green-button mid-btn-width green-gradient-btn">View All</div></a>

    <div class="clear"></div>

    <div class="mini-height30 width100"></div>
</div>

<div class="clear"></div>
<div class="width100 same-padding happy-bg">
    <div class="left-adopt-div ow-join-width">
    	<h1 class="adopt-h1"><img src="img/join-title.png" class="title-png" alt="Join Our Platform" title="Join Our Platform"></h1>
        <p class="adopt-p text-shadow">Become a <b class="white-bold">pet seller</b> in Mypetslibrary to let more pet lovers know about your existence.</p>
        
        <a href="#contact"><div class="adopt-btn ow-width green-gradient-btn">Send Us A Message</div></a>
    </div>
</div>
<div class="clear"></div>
<div class="width100 same-padding adopt-bg">
    <div class="left-adopt-div">
    	<h1 class="adopt-h1"><img src="img/adoption.png" class="title-png" alt="Pet Adoption" title="Pet Adoption"></h1>
        <p class="adopt-p adopt1 text-shadow">Adopt a pet and give it a home,<br>it will be love you back unconditionally.</p>
        <p class="adopt-p adopt2 text-shadow">Adopt a pet and give it a home, it will be love you back unconditionally.</p>
        <a href="malaysia-animal-pet-adoption.php"><div class="adopt-btn green-gradient-btn">Adopt a Pet</div></a>
    </div>
</div>
<div class="clear"></div>

<div class="grey-bg width100 same-padding overflow">

    <h1 class="green-text user-title left-align-title">Pawsome Tips</h1>
    <a href="malaysia-pet-blog.php" class="right-align-link view-a light-green-a hover-a blog-view-a">View More</a>
    
    <div class="clear"></div>
	<div class="blog-padding-big-div">
    <?php
    $conn = connDB();
    if($articles)
    {
    for($cnt = 0;$cnt < count($articles) ;$cnt++)
    {
    ?>
      <a href='pet-blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="opacity-hover">
        <div class="two-div-width shadow-white-box blog-box opacity-hover">
          <div class="left-img-div2">
            <img src="uploadsArticle/<?php echo $articles[$cnt]->getTitleCover();?>" class="width100" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>">
          </div>
          <div class="right-content-div3">
            <h3 class="article-title text-overflow">
              <?php echo $articles[$cnt]->getTitle();?>
            </h3>
            <p class="date-p">
              <?php echo $date = date("d-m-Y",strtotime($articles[$cnt]->getDateCreated()));?>
            </p>
            <p class="right-content-p">
              <?php echo $description = $articles[$cnt]->getKeywordOne();?>
            </p>
          </div>
        </div>
      </a>
    <?php
    }
    ?>
    <?php
    }
    $conn->close();
    ?>
	</div>
</div>

<div class="clear"></div>

<div class="width100 same-padding overflow" id="contact">
    <h1 class="green-text user-title left-align-title">Contact Us</h1>
  <a href="contactus.php" class="right-align-link view-a light-green-a hover-a blog-view-a">Read More</a>
	<div class="clear"></div>
	<div class="width100 text-center">
   	  <img src="img/mypetslibrary2.png" class="middle-logo" alt="Mypetslibrary" title="Mypetslibrary">
    </div>
  <div class="clear"></div>
    <p class="center-content">
    	Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide.
        <br>
        Email: <b>mypetslibrary@gmail.com</b> 
    </p>
    <div class="contact-div">
        <form id="contactform" method="post" action="index.php" class="form-class extra-margin">
                      
                      <input type="text" name="name" placeholder="Name" class="input-name clean" required >
                      
                      <input type="email" name="email" placeholder="Email" class="input-name clean" required >                  
                      
                      <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean" required >
    
    
                                      
                      <textarea name="comments" placeholder="Message" class="input-name input-message clean" ></textarea>
                      <div class="clear"></div>
                      <input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required><p class="opt-msg left"> I want to be updated with more information about your company's news and future promotions</p>
                      <div class="clear"></div>
                      <input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required><p class="opt-msg left"> I just want to be contacted based on my request/inquiry</p>
                      <div class="clear"></div>
                       <div class="width100 text-center">
                      <input type="submit" name="send_email_button" value="SEND US A MESSAGE" class="form-submit mid-btn-width green-button white-text clean pointer">
                      </div>
					   <div class="clear"></div>
					  <p class="privacy-p"><a href="termsPrivacy.php" target="_blank" class="dark-green-a">Terms & Privacy</a></p>
                </form>   
                <div class="width100 bottom-spacing"></div> 
                <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>


  <style type="text/css">

    * {
      box-sizing: border-box;
    }

	div{outline:none !important;}


	.home-a .hover1a{
		display:none !important;}
	.home-a .hover1b{
		display:inline-block !important;}
		
	@media (max-width: 800px){
	.slick-slide {
		margin: 0px 8px;
	}		
	}
  </style>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
    <script src="js/glider.js"></script>
    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider1').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider1').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider1').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider1').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider1'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,


               
             
        
        });
      });
    </script>

    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider2').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider2').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider2').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider2').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider2'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,


               
             
        
        });
      });
    </script>
    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider3').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider3').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider3').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider3').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider3'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,


               
             
        
        });
      });
    </script>  
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com";
    $email_subject = "Contact Form via Mypetslibrary website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Succesfully !!"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "There are no referrer with this email ! Please register again";
            $messageType = "No such username ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            // $messageType = "User password does not match";
            // $messageType = "Wrong password !!";
            $messageType = "Wrong phone number<br>Please try again!!";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Registration of new user failed!";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "User does not exist! Try to login again";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Admin does not exist! Try to login again";
        }

        else if($_GET['type'] == 8)
        {
            // $messageType = "Account Terminated <br> Please contact admin !";
            $messageType = "Account Banned <br> Please contact admin !";
        }

        else if($_GET['type'] == 9)
        {
            $messageType = "Account Deleted !!";
        }

        else if($_GET['type'] == 10)
        {
            $messageType = "Fail to register !!";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>