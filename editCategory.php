<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$categoryDetails = getCategory($conn," WHERE status = ? ",array("status"),array("Available"),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Category | Mypetslibrary" />
<title>Edit Category | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
        <h1 class="green-text h1-title">Edit Category</h1>
        <div class="green-border"></div>
    </div>

    <div class="border-separation">

    <div class="clear"></div>

        <!-- <form method="POST" action="utilities/editColorFunction.php"> -->
        <form method="POST" action="utilities/editCategoryFunction.php">

            <?php
            if(isset($_POST['category_id']))
            {
                $conn = connDB();
                $categoryDetails = getCategory($conn,"WHERE id = ? ", array("id") ,array($_POST['category_id']),"i");
            ?>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Category Name*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Category Name" value="<?php echo $categoryDetails[0]->getName();?>" name="update_category_name" id="update_category_name" required>       
                </div>

                <div class="clear"></div>

                <input type="hidden" value="<?php echo $categoryDetails[0]->getId();?>" name="category_id" id="category_id" required>   
                <input type="hidden" value="<?php echo $categoryDetails[0]->getStatus();?>" name="category_status" id="category_status">

            <?php
            }
            ?>

            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

 		<!-- <form>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Category Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Category Name" required name="" value>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Status*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Available</option>
                <option>Not Available</option>
            </select>     
        </div>        
        <div class="clear"></div>
      
        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form> -->

    </div>
    
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>