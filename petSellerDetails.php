<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Services.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$seller = getSeller($conn);
$petriRate = getServices($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$sellerDetails = getSeller($conn,"WHERE slug = ? ", array("slug") ,array($_GET['id']),"s");
if($sellerDetails)
{
for($cnt = 0;$cnt < count($sellerDetails) ;$cnt++)
{
?>
    <meta property="og:image" content="https://ichibangame.com/mypetslibrary/uploads/<?php echo $sellerDetails[$cnt]->getCompanyLogo();?>" /> 
    <?php include 'meta.php'; ?>

    <meta property="og:url" content="https://ichibangame.com/mypetslibrary/petSellerDetails.php?id=<?php echo $sellerDetails[$cnt]->getSlug();?>" />
    <link rel="canonical" href="https://ichibangame.com/mypetslibrary/petSellerDetails.php?id=<?php echo $sellerDetails[$cnt]->getSlug();?>" />

    <meta property="og:title" content="<?php echo $sellerDetails[$cnt]->getCompanyName();?> | Mypetslibrary" />
    <title><?php echo $sellerDetails[$cnt]->getCompanyName();?> | Mypetslibrary</title>
    <meta property="og:description" content="Mypetslibrary - <?php echo $sellerDetails[$cnt]->getCompanyName();?>" />
    <meta name="description" content="Mypetslibrary - <?php echo $sellerDetails[$cnt]->getCompanyName();?>" />
    <meta name="keywords" content="<?php echo $sellerDetails[$cnt]->getCompanyName();?>, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, malaysia, online pet store,马来西亚,上网买宠物, etc">
    <meta property="og:image" content="https://ichibangame.com/mypetslibrary/uploads/<?php echo $sellerDetails[$cnt]->getCompanyLogo();?>" /> 
<?php
}
?>
<?php
}
?>
<?php
}
?>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 menu-distance3 same-padding min-height2 pet-seller-all-div call-min-height pet-seller-padding">

	<div class="big-info-div width100 overflow seller-big-info">

        <?php
        if(isset($_GET['id']))
        {
        $conn = connDB();
        // $sellerDetails = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
        $sellerDetails = getSeller($conn,"WHERE slug = ? ", array("slug") ,array($_GET['id']),"s");
            if($sellerDetails)
            {
                for($cnt = 0;$cnt < count($sellerDetails) ;$cnt++)
                {
                ?>

                <div class="left-image-div">
                    <div class="item">            
                        <div class="clearfix">
                            <ul id="image-gallery" class="gallery list-unstyled cS-hidden">

                                <li data-thumb="uploads/<?php echo $sellerDetails[$cnt]->getCompanyLogo();?>" class="pet-slider-li"> 
                                    <img src="uploads/<?php echo $sellerDetails[$cnt]->getCompanyLogo();?>" class="pet-slider-img"  alt="<?php echo $sellerDetails[$cnt]->getCompanyName();?>" title="<?php echo $sellerDetails[$cnt]->getCompanyName();?>"/>
                                </li>
                                <?php
                                    $compPic = $sellerDetails[$cnt]->getCompanyPic();
                                    if($compPic != '')
                                    {
                                    ?>
                                        <li data-thumb="uploads/<?php echo $sellerDetails[$cnt]->getCompanyPic();?>" class="pet-slider-li"> 
                                            <img src="uploads/<?php echo $sellerDetails[$cnt]->getCompanyPic();?>" class="pet-slider-img" alt="<?php echo $sellerDetails[$cnt]->getCompanyName();?>" title="<?php echo $sellerDetails[$cnt]->getCompanyName();?>"/>
                                        </li>
                                    <?php
                                    }
                                    else
                                    {}
                                ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
        
                <div class="right-content-div2 seller-big-info">
                    
                <h1 class="green-text pet-name"><?php echo $sellerDetails[$cnt]->getCompanyName();?></h1>

                <?php $sellerName = $sellerDetails[$cnt]->getUid();?>

                <?php
                if($uid != "")
                {
                ?>

                    <div class="left-address-info">
                        <p class="green-text breed-p"><?php echo $sellerDetails[$cnt]->getAddress();?>, <?php echo $sellerDetails[$cnt]->getState();?></p>
                        <?php
                            $sellerContact = $sellerDetails[$cnt]->getContactNo();
                            $str1 = $sellerContact;
                            $sellerPhone = str_replace( 'https://api.whatsapp.com/send?phone=', '', $str1);
                        ?>
                        <p class="green-text breed-p"><a href="tel:+<?php echo $sellerPhone;?>" class="contact-icon hover1"><?php echo $sellerPhone;?></a></p>
                    </div>

                    <div class="right-info-div no-sms-div">
                        <a href="tel:+<?php echo $sellerPhone;?>" class="contact-icon hover1 three-button-width">
                            <img src="img/call.png" class="hover1a" alt="Call" title="Call">
                            <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
                        </a>

                        <a onclick="window.open(this.href); return false;" href="<?php echo $whatsappLink = $sellerDetails[$cnt]->getContactNo();?>" class="contact-icon hover1 three-button-width second-a">
                            <img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                            <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
                        </a>  
                    </div>

                <?php
                }
                else
                {
                ?>

                <div class="left-address-info">
                    <p class="green-text breed-p"><?php echo $sellerDetails[$cnt]->getAddress();?>, <?php echo $sellerDetails[$cnt]->getState();?></p>
                    <p class="green-text breed-p"><a class="contact-icon hover1 open-login"></a></p>
                </div>

                <div class="right-info-div no-sms-div">
                    <a class="contact-icon hover1 three-button-width open-login">
                        <img src="img/call.png" class="hover1a" alt="Call" title="Call">
                        <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
                    </a>

                    <a class="contact-icon hover1 three-button-width second-a open-login">
                        <img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                        <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
                    </a>  
                </div>

                <?php
                }
                ?>
                
                <div class="clear"></div>

                    <a href='petSellerReview.php?id=<?php echo $sellerDetails[$cnt]->getUid();?>'>
                        <div class="review-div hover1 pet-seller-review-div">
                            <div class="seller-review1">
                                <p class="left-review-p grey-p">Reviews</p>
                                <!-- <p class="left-review-mark">4/5</p> -->
                                <p class="left-review-mark">
                                    <!-- <?php //echo $display = $sellerDetails[$cnt]->getRating();?> / 5 -->

                                    <?php 
                                        $display = $sellerDetails[$cnt]->getRating();
                                        if ($display == 0)
                                        {
                                            echo '-';
                                        }
                                        elseif ($display > 0)
                                        {
                                            echo $display ;
                                            echo " / 5 " ;
                                        }
                                    ?>

                                </p>
                                <p class="right-review-star seller-review2">
                                    <!-- <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                    <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star"> -->

                                    <?php
                                        if ($display == 1)
                                        {
                                            echo '<img src="img/yellow-star.png" class="star-img">';
                                        }
                                        else if ($display == 2)
                                        {
                                            echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">';
                                        }
                                        else if ($display == 3)
                                        {
                                            echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                                '<img src="img/yellow-star.png" class="star-img">';
                                        }
                                        else if ($display == 4)
                                        {
                                            echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                                '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">';
                                        }
                                        else if ($display == 5)
                                        {
                                            echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                            '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                            '<img src="img/yellow-star.png" class="star-img">';
                                        }
                                        else if ($display == 0)
                                        {   }
                                    ?> 

                                </p>
                            </div>
                            <div class="seller-review3">
                                <p class="right-arrow">
                                    <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                                    <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                                </p>
                            </div>		
                        </div>
                    </a>

                    <div class="clear"></div>

                    <div class="pet-details-div">
                        <table class="pet-table">
                            <tr>
                                <td class="grey-p">Type of Breed</td>
                                <td class="grey-p">:</td>
                                <!-- <td>Pomeranian, Pembroke Welsh Corgi, French Bulldog</td> -->
                                <td><?php echo $sellerDetails[$cnt]->getBreedType();?></td>
                            </tr>
                            <tr>
                                <td class="grey-p">Experience</td>
                                <td class="grey-p">:</td>
                                <!-- <td>10 Years</td> -->
                                <td><?php echo $sellerDetails[$cnt]->getExperience();?> Year(s)</td>
                            </tr>  
                            <tr>
                                <td class="grey-p">Joined Date</td>
                                <td class="grey-p">:</td>
                                <!-- <td>1/1/2019</td> -->
                                <td>
                                    <?php echo $date = date("d-m-Y",strtotime($sellerDetails[$cnt]->getDateCreated()));?>
                                </td>
                            </tr>
                            <tr>
                                <td class="grey-p">Services</td>
                                <td class="grey-p">:</td>
                                <!-- <td>Puppy Seller, Delivery Services, Pet Grooming</td> -->
                                <td>
        
                                    
                                    <?php 
                                    if ($sellerDetails[$cnt]->getServices()) 
                                    {
                                        $petriRateExp = explode(",",$sellerDetails[$cnt]->getServices());
                                        for ($i=0; $i <count($petriRateExp) ; $i++)
                                        {
                                            $petriSelectionDet = getServices($conn," WHERE id =?",array("id"),array($petriRateExp[$i]), "s");
                                            if ($petriSelectionDet) 
                                            {
                                            ?>
                                                <?php echo $petriSelectionDet[0]->getServiceType();?>,
                                            <?php
                                            }
                                        }
                                    }
                                    ?>

                                </td>
                            </tr>  
                            <tr>
                                <td class="grey-p">Other Info</td>
                                <td class="grey-p">:</td>
                                <!-- <td>www.facebook.com/xxxxx</td> -->
                                <td>
									<p class="four-details-p"><?php echo $sellerDetails[$cnt]->getOtherInfo();?></p>
                                    <p class="four-details-p"><?php echo $sellerDetails[$cnt]->getInfoTwo();?></p>
                                    <p class="four-details-p"><?php echo $sellerDetails[$cnt]->getInfoThree();?></p>
                                    <p class="four-details-p"><?php echo $sellerDetails[$cnt]->getInfoFour();?></p>
                                
                                
                                
                                </td>
                            </tr>                                                                                                         
                        </table>
                    </div>  
                            
                </div>

                <?php
                }
                ?>
            <?php
            }
            ?>
        <?php
        }
        ?>

    </div>

    <div class="clear"></div>
	<div class="seller-big-info">
    <h1 class="green-text seller-h1">Seller Listings</h1>

	<div class="clear"></div>    

        <div class="width103">

            <?php
            $conn = connDB();
            
            $sellerPets = getPetsDetails($conn,"WHERE seller = ? ", array("seller") ,array($sellerName),"s");
            if($sellerPets)
            {
                for($cntAA = 0;$cntAA < count($sellerPets) ;$cntAA++)
                {
                ?>

                <!-- <a href='petsDetails.php?id=<?php //echo $sellerPets[$cntAA]->getUid();?>'> -->
                <a href='petsForSale.php?id=<?php echo $sellerPets[$cntAA]->getUid();?>'>
                    <div class="shadow-white-box four-box-size opacity-hover">
                       <div class="square">     
                        <div class="width100 white-bg content">
                            <img src="uploads/<?php echo $sellerPets[$cntAA]->getDefaultImage();?>" alt="<?php echo $sellerPets[$cntAA]->getBreed();?>" title="<?php echo $sellerPets[$cntAA]->getBreed();?>" class="width100 two-border-radius">
                        </div>
                       </div>           
                        <?php 
                            $status = $sellerPets[$cntAA]->getStatus();
                            if($status == 'Sold')
                            {
                            ?>
                                <div class="sold-label sold-label3">Sold</div>
                            <?php
                            }
                            else
                            {}
                        ?>

                        <div class="width100 product-details-div">

                                <?php 
                                    $petGender = $sellerPets[$cntAA]->getGender();
                                    if($petGender == 'Female')
                                    {
                                          $petsGender = 'F';
                                    }
                                    elseif($petGender == 'Male')
                                    {
                                          $petsGender = 'M';
                                    }
                                ?>

                                <p class="width100 text-overflow slider-product-name"><?php echo $sellerPets[$cntAA]->getBreed();?> | <?php echo $petsGender ;?></p>
                                <p class="slider-product-name">
                                    RM<?php $length = strlen($sellerPets[$cntAA]->getPrice());?>
                                    <?php 
                                    if($length == 2)
                                    {
                                    $hiddenPrice = "X";
                                    }
                                    elseif($length == 3)
                                    {
                                    $hiddenPrice = "XX";
                                    }
                                    elseif($length == 4)
                                    {
                                    $hiddenPrice = "XXX";
                                    }
                                    elseif($length == 5)
                                    {
                                    $hiddenPrice = "XXXX";
                                    }
                                    elseif($length == 6)
                                    {
                                    $hiddenPrice = "XXXXX";
                                    }
                                    elseif($length == 7)
                                    {
                                    $hiddenPrice = "XXXXXX";
                                    }
                                    ?>
                                    <?php echo substr($sellerPets[$cntAA]->getPrice(),0,1); echo $hiddenPrice;?>
                                </p>
                        </div>
                    </div>
                </a> 

                <?php
                }
                ?>
            <?php
            }
            ?>

        </div>
      </div>                         
</div>

<div class="clear"></div>



<div class="sticky-distance2 width100 call-sticky-distance"></div>

<?php
if($uid != "")
{
?>

    <div class="sticky-call-div">
        <table class="width100 sticky-table">
            <tbody>
                <tr>
                    <td class="call-td">
                        <a  href="tel:+<?php echo $sellerPhone;?>" class="text-center clean transparent-button same-text" >
                            Call
                        </a>
                    </td>
                    <!--<td>
                        <button class="text-center clean transparent-button same-text">
                            SMS
                        </button> 
                    </td>-->
                    <td class="whatsapp-td">
                        <a onclick="window.open(this.href); return false;" href="<?php echo $whatsappLink;?>" class="text-center clean transparent-button same-text">
                            Whatsapp
                        </a>  
                    </td>
                </tr>
            </tbody>    
        </table>   
    </div>

<?php
}
else
{
?>

    <div class="sticky-call-div">
        <table class="width100 sticky-table">
            <tbody>
                <tr>
                    <td class="call-td open-login">
                        <a class="text-center clean transparent-button same-text" >
                            Call
                        </a>
                    </td>

                    <td class="whatsapp-td open-login">
                        <a class="text-center clean transparent-button same-text">
                            Whatsapp
                        </a>  
                    </td>
                </tr>
            </tbody>    
        </table>   
    </div>

<?php
}
?>

<?php include 'stickyFooter.php'; ?>
<?php include 'js.php'; ?>
</body>
</html>