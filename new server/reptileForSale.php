<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$reptile = getReptile($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>

<?php 
// Program to display URL of current page. 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https"; 
else
$link = "http"; 

// Here append the common URL characters. 
$link .= "://"; 

// Append the host(domain name, ip) to the URL. 
$link .= $_SERVER['HTTP_HOST']; 

// Append the requested resource location to the URL 
$link .= $_SERVER['REQUEST_URI']; 

if(isset($_GET['id']))
{
    $referrerUidLink = $_GET['id'];
    // echo $referrerUidLink;
}
else 
{
    $referrerUidLink = "";
    // echo $referrerUidLink;
}
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$petsDetails = getReptile($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
// $petsDetails = $puppiesUid[0];
?>
<?php
}
?>

<?php
if($petsDetails)
{
for($cnt = 0;$cnt < count($petsDetails) ;$cnt++)
{
?>
<meta property="og:image" content="https://ichibangame.com/mypetslibrary/uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" /> 
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://ichibangame.com/mypetslibrary/reptileForSale.php?id=<?php echo $petsDetails[$cnt]->getUid();?>" />
<link rel="canonical" href="https://ichibangame.com/mypetslibrary/reptileForSale.php?id=<?php echo $petsDetails[$cnt]->getUid();?>" />
<meta property="og:title" content="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale | Mypetslibrary" />
<title><?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale | Mypetslibrary</title>
<meta property="og:description" content="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale.  Mypetslibrary online pet store in Malaysia." />
<meta name="description" content="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale.  Mypetslibrary online pet store in Malaysia." />
<meta name="keywords" content="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>), Reptile For Sale, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, malaysia, online pet store,马来西亚,上网买宠物, etc">
<meta property="og:image" content="https://ichibangame.com/mypetslibrary/uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" /> 
<?php
}
?>
<?php
}
?>

<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>


<div class="width100 menu-distance3 same-padding min-height2 ow-pet-details-div">

	<div class="left-image-div">


        <?php
        if($petsDetails)
        {
        for($cnt = 0;$cnt < count($petsDetails) ;$cnt++)
        {
        ?>

<div class="item">            
            <div class="clearfix">
                <ul id="image-gallery" class="gallery list-unstyled cS-hidden pet-ul">
                <?php
                if($petsDetails[$cnt]->getImageSix())
                {
                ?>
                    <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                    </li>
                    <?php 
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageOne()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                        </li>
                    <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageTwo()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageThree()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageThree();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageThree();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageFour()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageFour();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageFour();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                    <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageFive()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageFive();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageFive();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                    <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageSix()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageSix();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageSix();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                    <?php
                    }
                }elseif($petsDetails[$cnt]->getImageFive())
                {
                ?>
                    <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                    </li>
                    <?php 
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageOne()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                        </li>
                    <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageTwo()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageThree()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageThree();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageThree();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageFour()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageFour();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageFour();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                    <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageFive()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageFive();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageFive();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                    <?php
                    }
                }else if($petsDetails[$cnt]->getImageFour())
                {
                ?>
                    <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                    </li>
                    <?php 
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageOne()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                        </li>
                    <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageTwo()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageThree()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageThree();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageThree();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageFour()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageFour();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageFour();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                    <?php
                    }
                }else if($petsDetails[$cnt]->getImageThree())
                {
                ?>
                    <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                    </li>
                    <?php 
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageOne()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                        </li>
                    <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageTwo()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageThree()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageThree();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageThree();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                }else if($petsDetails[$cnt]->getImageTwo())
                {
                ?>
                    <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                    </li>
                    <?php 
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageOne()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                        </li>
                    <?php
                    }
                    if($petsDetails[$cnt]->getDefaultImage()==$petsDetails[$cnt]->getImageTwo()){
                    }
                    else{
                    ?>
                        <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-li"> 
                            <img src="uploads/<?php echo $petsDetails[$cnt]->getImageTwo();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale"  />
                        </li>
                        <?php
                    }
                }else if($petsDetails[$cnt]->getImageOne())
                {
                ?>
                    <li data-thumb="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $petsDetails[$cnt]->getDefaultImage();?>" class="pet-slider-img" alt="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" title="<?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale" />
                    </li>
                <?php
                }
                ?>

                         
                      
                    <li data-thumb="img/video.jpg" class="pet-slider-li"> 
                        <iframe src="https://player.vimeo.com/video/<?php echo $petsDetails[$cnt]->getLink();?>" class="video-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </li>
                </ul>
            </div>
        </div>

            <?php 
                $status = $petsDetails[$cnt]->getStatus();
                if($status == 'Sold')
                {
                ?>
                    <div class="sold-label detail-sold-label">Sold</div>
                <?php
                }
                else
                {}
            ?>
        
        <?php
        }
        ?>
        <?php
        }
        ?>

    </div>
    
    <div class="right-content-div2">

        <?php
        if($petsDetails)
        {
        for($cnt = 0;$cnt < count($petsDetails) ;$cnt++)
        {
        ?>
            <p class="green-text breed-p"><?php echo $petsDetails[$cnt]->getBreed();?></p>
            <?php $reptileUid = $petsDetails[$cnt]->getUid();?>
            <h1 class="green-text pet-name"><?php echo $petsDetails[$cnt]->getColor();?> <?php echo $petsDetails[$cnt]->getBreed();?> (<?php echo $petsDetails[$cnt]->getGender();?>) Reptile For Sale</h1>
            <p class="price-p2">
                RM<?php $length = strlen($petsDetails[$cnt]->getPrice());?>
                <?php 
                if($length == 2)
                {
                $hiddenPrice = "X";
                }
                elseif($length == 3)
                {
                $hiddenPrice = "XX";
                }
                elseif($length == 4)
                {
                $hiddenPrice = "XXX";
                }
                elseif($length == 5)
                {
                $hiddenPrice = "XXXX";
                }
                elseif($length == 6)
                {
                $hiddenPrice = "XXXXX";
                }
                elseif($length == 7)
                {
                $hiddenPrice = "XXXXXX";
                }
                ?>
                <?php echo substr($petsDetails[$cnt]->getPrice(),0,1); echo $hiddenPrice;?>
            </p>

            <?php $sellerUid =  $petsDetails[$cnt]->getSeller();?>

        <?php
        }
        ?>
        <?php
        }
        ?>

        <?php
        if($uid != "")
        {
        ?>

            <?php 
                $sellerUid;
                $conn = connDB();

                $sellerRows = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($sellerUid),"s");
                $sellerData = $sellerRows[0];
                $contact = $sellerData->getContactNo();
                $sellerPhone = str_replace( 'https://api.whatsapp.com/send?phone=', '', $contact);
            ?>

            <div class="right-info-div">
                <a href="tel:+<?php echo $sellerPhone;?>" class="contact-icon hover1">
                    <img src="img/call.png" class="hover1a" alt="Call" title="Call">
                    <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
                </a>

                <a onclick="window.open(this.href); return false;" href="<?php echo $contact; ?>&text=https://ichibangame.com/mypetslibrary/reptileForSale.php?id=<?php echo $reptileUid ;?>" class="contact-icon hover1 mid-whatsapp">
                    <img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                    <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
                </a>  

                <a class="contact-icon hover1 last-contact-icon open-social">
                    <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                    <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
                </a>
            </div>

        <?php
        }
        else
        {
        ?>

            <div class="right-info-div">
                <a class="contact-icon hover1 open-login">
                    <img src="img/call.png" class="hover1a" alt="Call" title="Call">
                    <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
                </a>

                <a class="contact-icon hover1 mid-whatsapp open-login">
                    <img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                    <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
                </a>  

                <a class="contact-icon hover1 last-contact-icon open-social">
                    <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                    <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
                </a>
            </div>

        <?php
        }
        ?>

        <div class="clear"></div>
            
            <?php
            if($petsDetails)
            {
            for($cnt = 0;$cnt < count($petsDetails) ;$cnt++)
            {
            ?>

            <div class="pet-details-div">
            	<table class="pet-table">
                	<tr>
                    	<td class="grey-p">SKU</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getSKU();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Gender</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getGender();?></td>
                    </tr>  
                	<tr>
                    	<td class="grey-p">Age</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getAge();?> old</td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Size</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getSize();?></td>
                    </tr>  
                	<tr>
                    	<td class="grey-p">Color</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getColor();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Vaccinated</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getVaccinated();?></td>
                    </tr> 
                	<tr>
                    	<td class="grey-p">Dewormed</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getDewormed();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Additional Information</td>
                        <td class="grey-p">:</td>
                        <td>
						<p class="four-details-p"><?php echo $petsDetails[$cnt]->getDetails();?></p>
                        <p class="four-details-p"><?php echo $petsDetails[$cnt]->getDetailsTwo();?></p>
                        <p class="four-details-p"><?php echo $petsDetails[$cnt]->getDetailsThree();?></p>
                        <p class="four-details-p"><?php echo $petsDetails[$cnt]->getDetailsFour();?></p>
                        
                        </td>
                    </tr>                	
                    <tr>
                    	<td class="grey-p">Published Date</td>
                        <td class="grey-p">:</td>
                        <td><?php echo date("d-m-Y",strtotime($petsDetails[$cnt]->getDateCreated()));?></td>
                    </tr>                    
                    <tr>
                    	<td class="grey-p">Location</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getLocation();?></td>
                    </tr>                    
                    <tr>
                    	<td class="grey-p">Delivery Service</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $petsDetails[$cnt]->getFeature();?></td>
                    </tr>                                                                                                                                        
                </table>
            </div>
                
            <h1 class="green-text seller-h1">Seller</h1>

            <?php 
            $sellerName =  $petsDetails[$cnt]->getSeller();
            $conn = connDB();

            // $sellDetails = getSeller($conn,"WHERE company_name = ? ", array("company_name") ,array($sellerName),"s");
            // $sellData = $sellDetails[0];
            // $sellUid = $sellData->getUid();

            $sellDetails = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($sellerName),"s");
            $sellData = $sellDetails[0];
            $sellUid = $sellData->getUid();
            $sellSlug = $sellData->getSlug();

            $comDetails = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($sellUid),"s");
            $comData = $comDetails[0];
            $reviewHeaderAA = $comData->getCompanyLogo();

            $compDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($sellUid),"s");
            $compData = $compDetails[0];
            $reviewHeader = $compData->getName();
            ?>

            <div class="seller-profile-div">
                <a href='petSellerDetails.php?id=<?php echo $sellSlug;?>' class="opacity-hover">
                    <img src="uploads/<?php echo $reviewHeaderAA;?>"  class="seller-profile" alt="Pet Seller" title="Pet Seller">
                </a>
            </div>

            <div class="right-seller-details">
                <a href='petSellerDetails.php?id=<?php echo $sellSlug;?>' class="opacity-hover">
                    <h1 class="seller-name-h1"><?php echo $reviewHeader;?></h1>
                </a>
                <p class="left-review-p grey-p pet-details-p-css">Reviews</p>

                <!-- <p class="left-review-mark pet-details-p-css"><?php //echo $display = $sellData->getRating();?>/5</p> -->
                <p class="left-review-mark pet-details-p-css">
                    <?php 
                        $display = $sellData->getRating();
                        if ($display == 0)
                        {
                            echo '-';
                        }
                        elseif ($display > 0)
                        {
                            echo $display ;
                            echo " / 5 " ;
                        }
                    ?>
                </p>

                <p class="right-review-star seller-profile-star pet-details-star">
                    <?php
                        if ($display == 1)
                        {
                            echo '<img src="img/yellow-star.png" class="star-img">';
                        }
                        else if ($display == 2)
                        {
                            echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">';
                        }
                        else if ($display == 3)
                        {
                            echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                '<img src="img/yellow-star.png" class="star-img">';
                        }
                        else if ($display == 4)
                        {
                            echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                                '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">';
                        }
                        else if ($display == 5)
                        {
                            echo '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                            '<img src="img/yellow-star.png" class="star-img">','<img src="img/yellow-star.png" class="star-img">',
                            '<img src="img/yellow-star.png" class="star-img">';
                        }
                        else if ($display == 0)
                        {   }
                    ?> 
                </p>

            </div>   

            <?php
            }
            ?>
            <?php
            }
            ?>

    </div>
</div>

<div class="clear"></div>



<div class="sticky-distance2 width100 call-sticky-distance"></div>

<?php
if($uid != "")
{
?>

    <?php
        $conn = connDB();
        $sellerRows = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($sellerUid),"s");
        $sellerData = $sellerRows[0];
        $contact = $sellerData->getContactNo();
        $sellerPhone = str_replace( 'https://api.whatsapp.com/send?phone=', '', $contact);
    ?>

    <div class="sticky-call-div">
        <table class="width100 sticky-table">
            <tbody>
                <tr>
                    <td class="call-td">
                        <a  href="tel:+<?php echo $sellerPhone;?>" class="text-center clean transparent-button same-text" >
                            Call
                        </a>
                    </td>
                    <!--<td>
                        <button class="text-center clean transparent-button same-text">
                            SMS
                        </button> 
                    </td>-->
                    <td class="whatsapp-td">
                        <a onclick="window.open(this.href); return false;" href="<?php echo $contact;?>" class="text-center clean transparent-button same-text">
                            Whatsapp
                        </a>  
                    </td>
                </tr>
            </tbody>    
        </table>   
    </div>

<?php
}
else
{
?>

    <div class="sticky-call-div">
        <table class="width100 sticky-table">
            <tbody>
                <tr>
                    <td class="call-td open-login">
                        <a class="text-center clean transparent-button same-text" >
                            Call
                        </a>
                    </td>

                    <td class="whatsapp-td open-login">
                        <a class="text-center clean transparent-button same-text">
                            Whatsapp
                        </a>  
                    </td>
                </tr>
            </tbody>    
        </table>   
    </div>

<?php
}
?>

<?php include 'stickyFooter.php'; ?>

<style>
.social-dropdown {
    width: 360px;
}
</style>
<?php include 'js.php'; ?>
</body>
</html>