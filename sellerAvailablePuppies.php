<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
// $sellerName = $userData->getName();
$sellerName = $userData->getUid();


// $availablePetsDetails = getPuppy($conn, "WHERE status = 'Available' ");
if (isset($_GET['search'])) {
$availablePetsDetails = getPuppy($conn, "WHERE seller =? AND status ='Available' AND name =?",array("seller,name"),array($sellerName,$_GET['search']),"ss");
}else {
$availablePetsDetails = getPuppy($conn, "WHERE seller =? AND status ='Available' ",array("seller"),array($sellerName),"s");
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Available Puppies | Mypetslibrary" />
<title>Available Puppies | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="left-h1-div">
            <h1 class="green-text h1-title">All Puppies</h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
          <?php //echo $_SERVER["PHP_SELF"] ?>
            <form action="search/searchFunction.php" method="post">
                    <?php
                    if (isset($_GET['search'])) {
                      ?>
                      <input class="line-input clean" type="text" value="<?php echo $_GET['search'] ?>" name="search" placeholder="Search">
                      <?php
                    }else {
                      ?>
                      <input class="line-input clean" type="text" name="search" placeholder="Search">
                      <?php
                    }
                     ?>
                     <!-- SKU and pet name-->
                     <input type="hidden" name="location" value="<?php echo $_SERVER["PHP_SELF"] ?>">
                     <button class="search-btn hover1 clean" type="submit">
                           <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                           <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                     </button>
              </form>
        </div>
        <div class="right-add-div">
        	<a href="sellerAddPuppy.php"><div class="green-button white-text puppy-button">Add a Puppy</div></a>
        </div>
    </div>


	<div class="clear"></div>
    <div class="width100 ship-bottom-div">
                <p class="review-product-name margin-bottom0"><a href="sellerAllPuppies.php" class="green-a">All</a> | <a href="sellerPendingPuppies.php" class="green-a">Pending</a> | Available | <a href="sellerSoldPuppies.php" class="green-a">Sold</a> | <a href="sellerRejectedPuppies.php" class="green-a">Rejected</a></p>
     </div>
    <div class="clear"></div>
        <div class="puppy-button-div">
            <a href="puppyBreed.php"><div class="green-button white-text breed-button puppy-button mid-breeed-button">Breed List</div></a>
            <a href="puppyColor.php"><div class="green-button white-text breed-button color-button puppy-button">Color List</div></a>
        </div>
    <div class="clear"></div>
	<div class="width100 scroll-div border-separation2">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Name</th>
                    <th>SKU</th>
                    <th>Added On</th>
                    <th>Status</th>
                    <th>Details</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    if($availablePetsDetails)
                    {
                        for($cnt = 0;$cnt < count($availablePetsDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $availablePetsDetails[$cnt]->getName();?></td>
                                <td><?php echo $availablePetsDetails[$cnt]->getSku();?></td>
                                <td><?php echo $availablePetsDetails[$cnt]->getDateCreated();?></td>
                                <td><?php echo $availablePetsDetails[$cnt]->getStatus();?></td>

                                <td>
                                    <form method="POST" action="sellerEditPets.php" class="hover1">
                                        <button class="clean hover1 img-btn" type="submit" name="pet_uid" value="<?php echo $availablePetsDetails[$cnt]->getUid();?>">
                                            <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <!-- <form method="POST" action="#" class="hover1">
                                        <button class="clean hover1 img-btn" type="submit" name="puppy_uid" value="<?php //echo $availablePetsDetails[$cnt]->getUid();?>">
                                            <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                            <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                        </button>
                                    </form> -->

                                    <form method="POST" action="utilities/sellerDeletePetsFunction.php" class="hover1">
                                        <input class="input-name clean" type="hidden" value="Puppy"  name="pets_type" id="pets_type" readonly>

                                        <button class="clean hover1 img-btn" type="submit" name="pets_uid" value="<?php echo $availablePetsDetails[$cnt]->getUid();?>">
                                            <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                            <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                        </button>
                                    </form>

                                </td>

                            </tr>
                        <?php
                        }
                    }
                ?>
            </tbody>

        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>



<?php include 'js.php'; ?>
</body>
</html>
