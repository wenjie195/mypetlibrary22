<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['usertype'] == 0)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/mypetslibrary.png" class="logo-img opacity-hover" alt="MyPetsLibrary" title="MyPetsLibrary"></a>
                    </div>
                    <div class="right-menu-div float-right text-menu-right">
                        <a href="adminDashboard.php" class="menu-padding black-to-white text-menu-a mob-no">
                            Dashboard
                        </a>                     
                        <div class="dropdown hover1 menu-margin mob-no">            
                            <a   class="menu-padding hover1 black-to-white text-menu-a">
                                Homepage <img src="img/dropdown2.png" class="dropdown-img3 hover1a"><img src="img/dropdown.png" class="dropdown-img3 hover1b">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        
                                        <!--<p class="dropdown-p"><a href="homeSettings.php"  class="menu-padding dropdown-a">Homepage Settings</a></p>-->
                                        <p class="dropdown-p"><a href="slider.php"  class="menu-padding dropdown-a">Slider</a></p>
                                        <p class="dropdown-p"><a href="featuredProducts.php"  class="menu-padding dropdown-a">Featured Products</a></p>
                                        <p class="dropdown-p"><a href="featuredPets.php"  class="menu-padding dropdown-a">Featured Pets</a></p>
                                        <!--<p class="dropdown-p"><a href="featuredPartners.php"  class="menu-padding dropdown-a">Featured Partners</a></p>-->                                
                            </div>                
                        </div>
                        <!--
                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/seller.png" class="menu-img hover1a" alt="Seller" title="Seller">
                                        <img src="img/seller2.png" class="menu-img hover1b" alt="Seller" title="Seller">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="seller.php"  class="menu-padding dropdown-a">All Sellers</a></p>
                                        <p class="dropdown-p"><a href="addSeller.php"  class="menu-padding dropdown-a">Add Sellers</a></p>
                                        <p class="dropdown-p"><a href="partnerServices.php"  class="menu-padding dropdown-a">Seller Services</a></p>
                            </div>
                        </div>  -->
                        <div class="dropdown hover1 mob-no">
                            <a  class="menu-margin menu-padding hover1 black-to-white  text-menu-a">
                                        Pets <img src="img/dropdown2.png" class="dropdown-img3 hover1a"><img src="img/dropdown.png" class="dropdown-img3 hover1b">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <!--<p class="dropdown-p"><a href="petSummary.php"  class="menu-padding dropdown-a">Pet Summary</a></p>-->
                                        <p class="dropdown-p"><a href="addPuppy.php"  class="menu-padding dropdown-a">Add Puppy</a></p>
                                        <p class="dropdown-p"><a href="allPuppies.php"  class="menu-padding dropdown-a">View Puppies</a></p>
                                        <p class="dropdown-p"><a href="addKitten.php"  class="menu-padding dropdown-a">Add Kitten</a></p>
                                        <p class="dropdown-p"><a href="allKittens.php"  class="menu-padding dropdown-a">View Kittens</a></p>
                                        <p class="dropdown-p"><a href="addReptile.php"  class="menu-padding dropdown-a">Add Reptile</a></p>
                                        <p class="dropdown-p"><a href="allReptiles.php"  class="menu-padding dropdown-a">View Reptile</a></p>
                                        <!--<p class="dropdown-p"><a href="pendingPets.php"  class="menu-padding dropdown-a">Pending Pets</a></p>-->
                                        <!--<p class="dropdown-p"><a href="pendingPetsPhoto.php"  class="menu-padding dropdown-a">Pending Pet Photos</a></p>-->
                            </div>
                        </div>     
                               
                        <div class="dropdown hover1 mob-no">
                            <a  class="menu-margin menu-padding hover1 black-to-white  text-menu-a">
                                        Products <img src="img/dropdown2.png" class="dropdown-img3 hover1a"><img src="img/dropdown.png" class="dropdown-img3 hover1b">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        
                                        <p class="dropdown-p"><a href="addProduct.php"  class="menu-padding dropdown-a">Add New Product</a></p>
                                        <p class="dropdown-p"><a href="paymentVerification.php"  class="menu-padding dropdown-a">Shipping</a></p>
                                        
                                        <p class="dropdown-p"><a href="allProducts.php"  class="menu-padding dropdown-a">All Products</a></p>
										<p class="dropdown-p"><a href="productSummary.php"  class="menu-padding dropdown-a">Product Summary</a></p>
                                        <!--<p class="dropdown-p"><a href="category.php"  class="menu-padding dropdown-a">Category</a></p>
                                        <p class="dropdown-p"><a href="brand.php"  class="menu-padding dropdown-a">Brand</a></p>-->
                            </div>
                        </div>              
                        <!--<div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/review.png" class="menu-img hover1a" alt="Review" title="Review">
                                        <img src="img/review2.png" class="menu-img hover1b" alt="Review" title="Review">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="reviewSummary.php"  class="menu-padding dropdown-a">Review Summary</a></p>
                                        <p class="dropdown-p"><a href="pendingReview.php"  class="menu-padding dropdown-a">Pending Reviews</a></p>
                                        <p class="dropdown-p"><a href="approvedReview.php"  class="menu-padding dropdown-a">Approved Reviews</a></p>
                                        <p class="dropdown-p"><a href="rejectedReview.php"  class="menu-padding dropdown-a">Rejected Reviews</a></p>
                                        <p class="dropdown-p"><a href="reportedReview.php"  class="menu-padding dropdown-a">Reported Reviews</a></p>
                            </div>
                        </div>       -->           
                        <!--<div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/article.png" class="menu-img hover1a" alt="Article" title="Article">
                                        <img src="img/article2.png" class="menu-img hover1b" alt="Article" title="Article">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="blogSummary.php"  class="menu-padding dropdown-a">Blog Summary</a></p>
                                        <p class="dropdown-p"><a href="pendingArticle.php"  class="menu-padding dropdown-a">Pending Article</a></p>
                                        <p class="dropdown-p"><a href="approvedArticle.php"  class="menu-padding dropdown-a">Approved Article</a></p>
                                        <p class="dropdown-p"><a href="rejectedArticle.php"  class="menu-padding dropdown-a">Rejected Article</a></p>
                                        <p class="dropdown-p"><a href="reportedArticle.php"  class="menu-padding dropdown-a">Reported Article</a></p>
                                        <p class="dropdown-p"><a href="addArticle.php"  class="menu-padding dropdown-a">Write New Article</a></p>                                
                            </div>
                        </div>  -->
                        <a href="seller.php" class="menu-padding black-to-white menu-margin text-menu-a mob-no">
                            Seller
                        </a>                            
                        <a href="allUsers.php" class="menu-padding black-to-white menu-margin text-menu-a mob-no">
                            User
                        </a>  
                         <a href="reviewSummary.php" class="menu-padding black-to-white menu-margin text-menu-a mob-no">
                            Review
                        </a>                                                   
                         <a href="blogSummary.php" class="menu-padding black-to-white menu-margin text-menu-a mob-no">
                            Blog
                        </a>                 
                        <div class="dropdown hover1 mob-no">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/settings.png" class="menu-img hover1a" alt="Settings" title="Settings">
                                        <img src="img/settings2.png" class="menu-img hover1b" alt="Settings" title="Settings">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="adminChangeEmail.php"  class="menu-padding dropdown-a">Change Email</a></p>
                                        <p class="dropdown-p"><a href="adminChangePassword.php"  class="menu-padding dropdown-a">Change Password</a></p>
                                        <p class="dropdown-p"><a href="logoutAdminSeller.php"  class="menu-padding dropdown-a">Logout</a></p>  
                            </div>
                        </div>                  
                            
                            
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                        <li><a href="adminDashboard.php">Dashboard</a></li>
                                        <li><a href="homeSettings.php">Homepage Settings</a></li>
                                        <li><a href="petSummary.php" class="mini-li">Pet Summary</a></li>
                                        <li><a href="seller.php" class="mini-li">All Sellers</a></li>
                                        <li><a href="allUsers.php"  class="mini-li">All Users</a></li>  
                                      
                                        <li><a href="productSummary.php" class="mini-li">Product Summary</a>                                                  
                                        <li><a href="reviewSummary.php"  class="mini-li">Review Summary</a></li>   
                                        <li><a href="blogSummary" class="mini-li">Blog Summary</a></li>
                                                                         
                                        <li><a href="adminChangeEmail.php" class="mini-li">Change Email</a></li>
                                        <li><a href="adminChangePassword.php"  class="mini-li">Change Password</a></li>                                                                      
                                        <li  class="last-li"><a href="logoutAdminSeller.php">Logout</a></li>
                                </ul>
                            </div><!-- /dl-menuwrapper -->                
                        
                                                
                    </div>
                </div>

        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color logged-menu" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/mypetslibrary.png" class="logo-img opacity-hover" alt="MyPetsLibrary" title="MyPetsLibrary"></a>
                    </div>
                    <div class="right-menu-div float-right text-menu-right">
                    <!--<a href="favourite.php" class="menu-padding text-menu-a hover1">
                            <img src="img/favourite.png" class="menu-icon hover1a" alt="Favourite" title="Favourite">
                            <img src="img/favourite2.png" class="menu-icon hover1b" alt="Favourite" title="Favourite">
                        </a>            
                        <a href="cart.php" class="menu-padding menu-margin text-menu-a hover1">
                            <img src="img/cart.png" class="menu-icon hover1a" alt="Cart" title="Cart">
                            <img src="img/cart2.png" class="menu-icon hover1b" alt="Cart" title="Cart">
                        </a> -->			
                        <a href="index.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Home
                        </a>   
                        <a href="profile.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Profile
                        </a> 
                        <a href="malaysia-cute-puppy-dog.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Puppies
                        </a>   
                        <a href="malaysia-cute-kitten-cat.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Kittens
                        </a>                 
                       <!--<a href="malaysia-cute-reptiles.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Reptiles
                        </a>-->    
                        <a href="malaysia-pet-food-toy-product.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Products
                        </a>              
 						<a href="pet-seller-grooming-delivery-hotel.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Partners
                        </a>                       
                        <a href="malaysia-pet-blog.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Tips
                        </a>
                        <a href="contactus.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            About Us
                        </a>                  
                        <a href="logout.php"  class="menu-padding black-to-white menu-margin text-menu-a">
                            Logout
                        </a>                                                                  
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                <!--    <div class="overflow three-menu-container">
                                        <div class="menu-three-div">
                                            <a href="cart.php" class="opacity-hover">
                                                <img src="img/cart2.png" alt="Cart" title="Cart" class="menu-three-img">
                                                <div class="red-dot"><p class="red-dot-p">10</p></div>
                                            </a>
                                        </div>
                                        <div class="menu-three-div mid-menu-three-div">
                                            <a href="toShip.php" class="opacity-hover">
                                                <img src="img/shipping2.png" alt="To Ship" title="To Ship" class="menu-three-img">
                                                <div class="red-dot"><p class="red-dot-p">10</p></div>
                                            </a>
                                        </div>  
                                        <div class="menu-three-div">
                                            <a href="toReceive.php" class="opacity-hover">
                                                <img src="img/receive2.png" alt="To Receive" title="To Receive" class="menu-three-img">
                                                <div class="red-dot"><p class="red-dot-p">10</p></div>
                                            </a>
                                        </div>                                
                                                                    
                                    </div>-->
                                        <li><a href="profile.php" class="mini-li">Profile</a></li>
                                        <li><a href="index.php" class="mini-li">Home</a></li>
                                        <li><a href="malaysia-cute-puppy-dog.php" class="mini-li">Puppies</a></li>                                  
                                        <li><a href="malaysia-cute-kitten-cat.php" class="mini-li">Kittens</a></li>
                                        <!--<li><a href="malaysia-cute-reptiles.php"  class="mini-li">Reptiles</a></li>-->                                  
                                        <li><a href="malaysia-pet-food-toy-product.php"  class="mini-li">Products</a></li>  
										<li><a href="pet-seller-grooming-delivery-hotel.php"  class="mini-li">Partners</a></li>
                                        <li><a href="malaysia-pet-blog.php" class="mini-li">Pawsome Tips</a></li>
                                        <li><a href="contactus.php" class="mini-li">About Us</a></li>
                                        <li><a href="logout.php" class="mini-li">Logout</a></li>
                                </ul>
                            </div><!-- /dl-menuwrapper -->                
                        
                                                
                    </div>
                </div>

        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 2)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/mypetslibrary.png" class="logo-img opacity-hover" alt="MyPetsLibrary" title="MyPetsLibrary"></a>
                    </div>
                    <div class="right-menu-div float-right">
                
                            <a href="sellerDashboard.php"   class="menu-padding hover1">
                                <img src="img/home.png" class="menu-img hover1a" alt="Dashboard" title="Dashboard">
                                <img src="img/home2.png" class="menu-img hover1b" alt="Dashboard" title="Dashboard">
                            </a>


                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/seller.png" class="menu-img hover1a" alt="Seller" title="Seller">
                                        <img src="img/seller2.png" class="menu-img hover1b" alt="Seller" title="Seller">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="profilePreview.php"  class="menu-padding dropdown-a">Profile</a></p>
                                        <p class="dropdown-p"><a href="editSellerProfile.php"  class="menu-padding dropdown-a">Edit Profile</a></p>
                            </div>
                        </div>  
                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/pet.png" class="menu-img hover1a" alt="Pet" title="Pet">
                                        <img src="img/pet2.png" class="menu-img hover1b" alt="Pet" title="Pet">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="sellerPetSummary.php"  class="menu-padding dropdown-a">Pet Summary</a></p>
                                       
                                        <p class="dropdown-p"><a href="sellerAllPuppies.php"  class="menu-padding dropdown-a">All Puppies</a></p>
                                        <p class="dropdown-p"><a href="sellerAllKittens.php"  class="menu-padding dropdown-a">All Kittens</a></p>
                                        <p class="dropdown-p"><a href="sellerAllReptiles.php"  class="menu-padding dropdown-a">All Reptiles</a></p>
                                        <p class="dropdown-p"><a href="sellerPendingPets.php"  class="menu-padding dropdown-a">Pending Pets</a></p>
                            </div>
                        </div>                 
                                    
                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/article.png" class="menu-img hover1a" alt="Article" title="Article">
                                        <img src="img/article2.png" class="menu-img hover1b" alt="Article" title="Article">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="sellerBlogSummary.php"  class="menu-padding dropdown-a">Blog Summary</a></p>
                                        <p class="dropdown-p"><a href="sellerPendingArticle.php"  class="menu-padding dropdown-a">Pending Article</a></p>
                                        <p class="dropdown-p"><a href="sellerApprovedArticle.php"  class="menu-padding dropdown-a">Approved Article</a></p>
                                        <p class="dropdown-p"><a href="sellerRejectedArticle.php"  class="menu-padding dropdown-a">Rejected Article</a></p>
                                        <p class="dropdown-p"><a href="sellerReportedArticle.php"  class="menu-padding dropdown-a">Reported Article</a></p>
                                        <p class="dropdown-p"><a href="sellerAddArticle.php"  class="menu-padding dropdown-a">Write New Article</a></p> 
                            </div>
                        </div>                    
                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/settings.png" class="menu-img hover1a" alt="Settings" title="Settings">
                                        <img src="img/settings2.png" class="menu-img hover1b" alt="Settings" title="Settings">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <!-- <p class="dropdown-p"><a href="adminChangeEmail.php"  class="menu-padding dropdown-a">Change Email</a></p>
                                        <p class="dropdown-p"><a href="adminChangePassword.php"  class="menu-padding dropdown-a">Change Password</a></p> -->
                                        <p class="dropdown-p"><a href="sellerChangeEmail.php"  class="menu-padding dropdown-a">Change Email</a></p>
                                        <p class="dropdown-p"><a href="sellerChangePassword.php"  class="menu-padding dropdown-a">Change Password</a></p>
                                        <p class="dropdown-p"><a href="logoutAdminSeller.php"  class="menu-padding dropdown-a">Logout</a></p> 
                            </div>
                        </div>                  
                            
                            
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                        <li><a href="sellerDashboard.php">Dashboard</a></li>
                                        <li><a href="profilePreview.php" class="mini-li">Profile</a></li>
                                        <li><a href="sellerPetSummary.php" class="mini-li">Pet Summary</a></li>
                                        
                                        <li><a href="sellerBlogSummary" class="mini-li">Blog Summary</a></li>                           
                                        <!-- <li><a href="adminChangeEmail.php" class="mini-li">Change Email</a></li>
                                        <li><a href="adminChangePassword.php"  class="mini-li">Change Password</a></li>           -->
                                        <li><a href="sellerChangeEmail.php" class="mini-li">Change Email</a></li>
                                        <li><a href="sellerChangePassword.php"  class="mini-li">Change Password</a></li>                                                               
                                        <li  class="last-li"><a href="logoutAdminSeller.php">Logout</a></li>
                                </ul>
                            </div><!-- /dl-menuwrapper -->                
                        
                                                
                    </div>
                </div>

        </header>

    <?php
    }
    ?>

<?php
}
else
{
?>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v6.0&appId=2101026363514817&autoLogAppEvents=1"></script>
    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
                <div class="float-left left-logo-div">
                    <a href="index.php"><img src="img/mypetslibrary.png" class="logo-img opacity-hover" alt="MyPetsLibrary" title="MyPetsLibrary"></a>
                </div>
                <div class="right-menu-div float-right text-menu-right">
                <a href="index.php" class="menu-padding black-to-white text-menu-a">
                        Home
                    </a>            
                    <a href="malaysia-cute-puppy-dog.php" class="menu-padding black-to-white menu-margin text-menu-a">
                        Puppies
                    </a> 
                    <a href="malaysia-cute-kitten-cat.php" class="menu-padding black-to-white menu-margin text-menu-a">
                        Kittens
                    </a> 					
                       <!--<a href="malaysia-cute-reptiles.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Reptiles
                        </a>-->    
                        <a href="malaysia-pet-food-toy-product.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Products
                        </a>              
 						<a href="pet-seller-grooming-delivery-hotel.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Partners
                        </a> 
                    <a href="malaysia-pet-blog.php" class="menu-padding black-to-white menu-margin text-menu-a">
                        Tips 
                    </a>
                    <a href="contactus.php" class="menu-padding black-to-white menu-margin text-menu-a">
                        About Us
                    </a>                
                    <a class="menu-padding black-to-white open-signup menu-margin text-menu-a">
                        Sign Up
                    </a>
                    <a  class="menu-padding black-to-white open-login menu-margin text-menu-a">
                        Login
                    </a>                                                                  
                        <div id="dl-menu" class="dl-menuwrapper">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                                    <li><a class="open-signup mini-li">Sign Up</a></li>
                                    <li><a class="open-login mini-li">Login</a></li>
                                    <li><a href="index.php" class="mini-li">Home</a></li>
                                    <li><a href="malaysia-cute-puppy-dog.php" class="mini-li">Puppies</a></li>                                  
                                    <li><a href="malaysia-cute-kitten-cat.php" class="mini-li">Kittens</a></li>
                                    <li><a href="malaysia-cute-reptiles.php"  class="mini-li">Reptiles</a></li>                                 
                                        <!--<li><a href="malaysia-cute-reptiles.php"  class="mini-li">Reptiles</a></li>-->                                  
                                        <li><a href="malaysia-pet-food-toy-product.php"  class="mini-li">Products</a></li>  
										<li><a href="pet-seller-grooming-delivery-hotel.php"  class="mini-li">Partners</a></li>
                                    <li><a href="malaysia-pet-blog.php" class="mini-li">Pawsome Tips</a></li>
                                    <li><a href="contactus.php" class="mini-li">About Us</a></li>
                            </ul>
                        </div><!-- /dl-menuwrapper -->                
                    
                                            
                </div>
            </div>

    </header>

<?php
}
?>