<!DOCTYPE html>
<!-- saved from url=(0045)https://myadmin.mypetslibrary.com/pets/create -->
<html lang="en" class="wf-poppins-n3-active wf-poppins-n4-active wf-poppins-n5-active wf-poppins-n6-active wf-poppins-n7-active wf-roboto-n3-active wf-roboto-n4-active wf-roboto-n5-active wf-roboto-n6-active wf-roboto-n7-active wf-active"><!-- begin::Head --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <title>MyPetsLibrary | Admin Dashboard</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
<link rel="stylesheet" type="text/css" href="css/filepond-plugin-image-preview.css">
<link rel="stylesheet" type="text/css" href="css/filepond.css">
<?php include 'css.php'; ?>
<script src="js/filepond-plugin-image-preview.js" type="text/javascript"></script>
<script src="js/filepond-plugin-file-encode.js" type="text/javascript"></script>
<script src="js/filepond.js" type="text/javascript"></script>    
  
    <!--end::Page Vendors Styles -->
    <link rel="shortcut icon" href="https://myadmin.mypetslibrary.com/img/logo/favicon.ico">



    
</head>
<!-- end::Head -->    <!-- begin::Body -->
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default m-scroll-top--shown">
 <?php include 'js.php'; ?>       <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">


 

                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="https://myadmin.mypetslibrary.com/pets/create" id="newSellerForm" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="r8o7TQ652NCbHi9H7Pjxo8zKEFzlqNpTDDAXI04S">                        <div class="m-portlet__body">

                           
                         

                            <div class="form-group m-form__group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="d-block">Add Pets Images</label>
                                        <button type="button" class="btn btn-brand" id="UploadBtn">
                                            Browse
                                        </button>
                                    </div>
                                    <div class="col-md-9">
                                        <div id="UploadPreview" class="row align-items-end"></div>
                                    </div>
                                </div>
                                <input type="file" id="UploadInput" style="position: absolute; opacity: 0; left: -9999px;" multiple>
                            </div>

                            <div class="form-group m-form__group">
                                <label>is sold *</label>
                                    <div class="m-radio-list">                                
                                        <label class="m-radio m-radio--bold m-radio--state-success">
                                            <input type="radio" name="rad_sold_status" value="0" checked="checked"> Available
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--bold m-radio--state-danger">
                                            <input type="radio" name="rad_sold_status" value="1"> Sold
                                            <span></span>
                                        </label>
                                    </div>
                            <span class="m-form__help">Account status , open or suspended</span>
                            </div>

                        
                     
 
                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button id="createSubmit" type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>

        </div>
    </div>
</div>

<link href="css/croppie.css" rel="stylesheet">
<style>
    #UploadPreview .upload-preview{
        position: relative;
    }
    #UploadPreview .upload-preview.sortable-ghost{
        opacity: .3;
    }
    #UploadPreview .upload-preview .upload-preview-actions-delete{
        display: none;
    }
    #UploadPreview .upload-preview img{
        transition: opacity .3s;
    }
    #UploadPreview .upload-preview.__deleted img{
        opacity: .2;
    }
    #UploadPreview .upload-preview.__deleted .upload-preview-actions{
        display: none;
    }
    #UploadPreview .upload-preview.__deleted .upload-preview-actions-delete{
        display: block;
    }
</style>


<div class="modal" id="UploadModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="CroppieEl" class="croppie-container"><div class="cr-boundary" aria-dropeffect="none" style="width: 500px; height: 500px;"><img class="cr-image" alt="preview" aria-grabbed="false"><div class="cr-viewport cr-vp-square" tabindex="0" style="width: 500px; height: 500px;"></div><div class="cr-overlay"></div></div><div class="cr-slider-wrap"><input class="cr-slider" type="range" step="0.0001" aria-label="zoom"></div></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-brand" id="CroppieBtn">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/tmpl" id="UploadTmpl">
<div class="upload-preview col-md-3 m--margin-top-10" id="Upload_{unique_id}">
    <input type="hidden" name="upload[{index}][id]" value="" />
    <input type="hidden" name="upload[{index}][file]" value="{src}" class="upload_file" />
    <input type="hidden" name="upload[{index}][delete]" value="0" class="upload_delete" />
    <img src="{src}" alt="preview" style="width: 100%;" />
    <div class="upload-preview-actions m--margin-top-5">
        <a href="#" class="image-edit m-link m-link--state m-link--brand" style="margin: 0 5px;">Edit</a>
        <a href="#" class="image-delete m-link m-link--state m-link--danger" style="margin: 0 5px;">Delete</a>
    </div>
    <div class="upload-preview-actions-delete m--margin-top-5">
        <span class="m--font-danger" style="margin: 0 5px;">DELETED</span>
        <a href="#" class="image-undo m-link m-link--state m-link--success" style="margin: 0 5px;">Undo</a>
    </div>
</div>
</script>

<script type="text/javascript" src="js/sortable.js"></script>
<script type="text/javascript" src="js/croppie.min.js"></script>


<script type="text/javascript">
    
    function copyPetSlugValue() {
        var x = document.getElementById("pet_name").value;
        var y = document.getElementById("pet_sku").value;
        var z = x + '-' + y;

        document.getElementById("slug").value = z.replace(/ /g,"-").toLowerCase();
    }
</script>
                </div>
            </div>
            <!-- end:: Body -->
 
        </div>
        <!-- end:: Page -->

        <!-- end::Scroll Top -->
        <!--begin::Global Theme Bundle -->
        <script src="js/vendors.bundle.js" type="text/javascript"></script>
        <script src="js/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Global Theme Bundle -->
        <!--end::Page Vendors -->

        <script src="js/sortable.js" type="text/javascript"></script>
        <script src="js/croppie.min.js" type="text/javascript"></script>

        <!--end::Page Scripts -->

  
        <!-- End Universal Flash -->
        <!-- Start Summernote Options -->
        <script type="text/javascript">
          

        FilePond.registerPlugin(FilePondPluginImagePreview);
        FilePond.registerPlugin(FilePondPluginFileEncode);
        FilePond.setOptions({
            instantUpload: false,
        });
        const inputElement = document.getElementById('pondInput');
        const pond = FilePond.create( inputElement ,{
            maxFiles: 1,
            allowBrowse: true,
            allowFileEncode:true,
            // server: {
            //     url: '/upload',
            //     process: {
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         }
            //     }
            // }
        });

        // $('#createSubmit').click(function(){ //listen for submit event
        //     console.log('hi form did submit' + pond.getFiles());
        //     $("#pondInput").val(function() {
        //             return pond.getFiles();
        //      });
        //     return true;
        // }); 
      

                 // Upload handler
                var $uploadBtn = $('#UploadBtn');
                var $uploadPreview = $('#UploadPreview');
                var $uploadInput = $('#UploadInput');
                var $uploadTmpl = $('#UploadTmpl');
                var $uploadModal = $('#UploadModal');
                
                $uploadBtn.on('click', function() {
                    console.log('did press add images');
                    $uploadInput.trigger('click');
                });

                $uploadInput.on('change', function(e) {
                    var files = this.files;

                    if (files) {
                        for (var i = 0, f; f = files[i]; i++) {
                            readFile(f, function(resp) {
                                var tmpl = $uploadTmpl.html()
                                            .replace(/{src}/g, resp)
                                            .replace(/{index}/g, $uploadPreview.children().length)
                                            .replace(/{unique_id}/g, new Date().getTime());
                                $(tmpl).appendTo($uploadPreview);
                            });
                        }
                    }
                });

                $uploadPreview
                    .on('click', '.image-edit', function(e) {
                        e.preventDefault();
                        var src = $(this).closest('.upload-preview').children('img').attr('src');
                        $croppieTarget = $(this).closest('.upload-preview');

                        $croppie.croppie('bind', {
                            url: src
                        });

                        $uploadModal.modal({
                            keyboard: false
                        });
                    })
                    .on('click', '.image-delete', function(e) {
                        e.preventDefault();
                        $(this).closest('.upload-preview').addClass('__deleted')
                            .children('.upload_delete').val(1);
                    })
                    .on('click', '.image-undo', function(e) {
                        e.preventDefault();
                        $(this).closest('.upload-preview').removeClass('__deleted')
                            .children('.upload_delete').val(0);
                    });


                // croppie
                var $croppieEl = $('#CroppieEl');
                var $croppieBtn = $('#CroppieBtn');
                var $croppieTarget = null;

                var $croppie = $croppieEl.croppie({
                    enableExif: true,
                    enforceBoundary : true,
                    mouseWheelZoom: false,
                    viewport: {
                        width: 500,
                        height: 500
                    },
                    boundary: {
                        width: 500,
                        height: 500
                    }
                });

                $croppieBtn.on('click', function() {
                    if ($croppieTarget) {
                        $croppie.croppie('result', {
                            type: 'base64',
                            size: 'original',
                            quality: 0.8,
                            format: 'jpeg',
                            backgroundColor : "#ffffff"
                        }).then(function (resp) {
                            // update target value
                            $croppieTarget.children('img').attr('src', resp);
                            $croppieTarget.children('.upload_file').val(resp);

                            // hide modal
                            $uploadModal.modal('hide');
                        });
                    }
                });

                // enable sortable
                var sortable = new Sortable($uploadPreview[0], {
                    onEnd: function(e) {
                        // re-order childs index
                        $uploadPreview.children().each(function(index) {
                            $(this).find('input[type=hidden]').each(function() {
                                var name = $(this).attr('name').replace(/\[[0-9]+\]/, '[' + index + ']');
                                $(this).attr('name', name);
                            });
                        });
                    }
                });


                // methods
                function readFile(file, cb) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        cb(e.target.result);
                    };
                    reader.readAsDataURL(file);
                }



    
  
        </script>


        <!-- end::Body -->


<!-- end::Footer -->
</body></html>