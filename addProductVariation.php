<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/Brand.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$newProductUid = $_SESSION['newProduct_uid'];

$conn = connDB();

$categoryDetails = getCategory($conn);
$brandDetails = getBrand($conn);

$productDetails = getProduct($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add New Product | Mypetslibrary" />
<title>Add New Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add Product Variation</h1>
            <div class="green-border"></div>
    </div>
    <div class="border-separation">
        <div class="clear"></div>
        <form action="utilities/registerVariationFunction.php" method="POST" enctype="multipart/form-data">
        <?php
        if($newProductUid){
            $conn = connDB();
            // $product = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_POST['newProduct_uid']),"s");
            $product = getProduct($conn);
            for($cnt = 1;$cnt < count($product) ;$cnt++){
                if($product[$cnt]->getUid() == $newProductUid){
                ?>
                    <input class="clean white-input two-box-input" type="hidden" id="update_product_uid" name="update_product_uid" value="<?php echo $product[$cnt]->getUid();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_category" name="update_category" value="<?php echo $product[$cnt]->getCategory();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_brand" name="update_brand" value="<?php echo $product[$cnt]->getBrand();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_name" name="update_name" value="<?php echo $product[$cnt]->getName();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_sku" name="update_sku" value="<?php echo $product[$cnt]->getSku();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_slug" name="update_slug" value="<?php echo $product[$cnt]->getSlug();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_animal_type" name="update_animal_type" value="<?php echo $product[$cnt]->getAnimalType();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_expiry_date" name="update_expiry_date" value="<?php echo $product[$cnt]->getExpiryDate();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_status" name="update_status" value="<?php echo $product[$cnt]->getStatus();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_description" name="update_description" value="<?php echo $product[$cnt]->getDescription();?>">
                    <input class="clean white-input two-box-input" type="hidden" id="update_keyword_one" name="update_keyword_one" value="<?php echo $product[$cnt]->getKeywordOne();?>">
                    <?php
                }
            }
        }
        ?> 
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation Name" name="register_variation" id="register_variation_one" required>
        </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price*</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_price" id="register_variation_one_price" required>
            </div>
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock*</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_stock" id="register_variation_one_stock" required>
            </div>
            <!-- <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo*</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image" id="register_variation_one_image" accept="image/*" class="margin-bottom10" required/></p>
            </div> -->
        <div class="clear"></div>

        <!-- <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 2 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 2 Name" name="register_variation[]" id="register_variation_two">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_price[]" id="register_variation_two_price">
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_stock[]" id="register_variation_two_stock">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image[]" id="register_variation_two_image" accept="image/*" class="margin-bottom10" /></p>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 3 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 3 Name" name="register_variation[]" id="register_variation_three">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_price[]" id="register_variation_three_price">
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_stock[]" id="register_variation_three_stock">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image[]" id="register_variation_three_image" accept="image/*"  class="margin-bottom10"/></p>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 4 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 4 Name" name="register_variation[]" id="register_variation_four">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_price[]" id="register_variation_four_price">
        </div>
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_stock[]" id="register_variation_four_stock">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image[]" id="register_variation_four_image" accept="image/*" class="margin-bottom10" /></p>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-p admin-top-p slug-p">Free Gift</p>
            <select class="input-name clean admin-input" name="register_free_gift" id="register_free_gift" onchange='FreeGift(this.value);' required> 
                <option>Free Gift Availability</option>  
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
        </div>
        <div class="dual-input second-dual-input" name="fg_img" id="fg_img" style='display:none;'>
            <p class="input-top-p admin-top-p">Upload Free Gift Image</p>
            <p class="margin-bottom30"><input id="file-upload" type="file" name="register_freeGift_img" id="register_freeGift_img" accept="image/*" class="margin-bottom10" /></p>
        </div>
        <div class="clear"></div>
        <div class="dual-input" name="fg_desc" id="fg_desc" style='display:none;'>
        	<p class="input-top-p admin-top-p">Free Gift Description</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Free Gift Description" name="register_freeGift_desc" id="register_freeGift_desc">
        </div>
        <div class="clear"></div>
        <div class="width100 overflow text-center">
        	<div class="green-button white-text clean2 edit-1-btn margin-auto ow-peach-button" onclick="showVariation();this.style.visibility= 'hidden';">Add More Variation</div>
        </div>
        <div class="clear"></div> 
        </div>-->

        <div class="width100 overflow text-center">
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new product!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Registration of new seller failed!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

<script type="text/javascript">
function FreeGift(val){
 var element=document.getElementById('fg_img');
 var elementA=document.getElementById('fg_desc');
 if(val=='Free Gift Availability'||val=='yes'){
   element.style.display='block';
   elementA.style.display='block';
 }
 else {
   element.style.display='none';
 }
}

</script> 

</body>
</html>
