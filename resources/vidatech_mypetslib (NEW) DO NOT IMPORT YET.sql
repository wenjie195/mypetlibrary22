-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2020 at 10:46 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `seo_title` text DEFAULT NULL,
  `article_link` text DEFAULT NULL,
  `keyword_one` text DEFAULT NULL,
  `keyword_two` text DEFAULT NULL,
  `title_cover` text DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `img_four_source` text DEFAULT NULL,
  `img_five_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `article_link`, `keyword_one`, `keyword_two`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `paragraph_four`, `image_four`, `paragraph_five`, `image_five`, `img_cover_source`, `img_one_source`, `img_two_source`, `img_three_source`, `img_four_source`, `img_five_source`, `author`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '9892e3e88f07ccbd84cff19b8a99751e', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'Think Thrice Before Getting A Pet', 'ThinkThriceBeforeGettingAPet-think-thrice-before-getting-a-pet', 'think-thrice-before-getting-a-pet', ' Are you googling Puppy For Sale, Puppy Seller or Cat or Dog Breeder to get yourself a puppy or kitten as pet right now? One moment please, why do you want to get a pet at the first place? Because they are fluffy, cuddly and cute? Well, we can’t deny that', 'Puppy For Sale,Puppy Seller,Cat or Dog Breeder,toy poodle puppy,', '9892e3e88f07ccbd84cff19b8a99751eblog-picture.jpg', '<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Are you googling &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Puppy For Sale</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo;, &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Puppy Seller</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo; or &ldquo;</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Cat or Dog Breeder</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&rdquo; to get yourself a puppy or kitten as pet right now? One moment please, why do you want to get a pet at the first place? Because they are fluffy, cuddly and cute? Well, we can&rsquo;t deny that this is normally what we thought as first reason as who can resist a </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>toy poodle puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> that resembles a tiny teddy bear!</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/ZNeG7pYfhQLKnh3XqURvYiDm36BO_lemAqauO3Hq12MbcH1slc2hUqIKXN6uwteghzo3s2xAFGxpSCKkybKf6Mglepx6kWEIQRptp6suJWXtdAyunyc1OZ1w67B6UFc799xXtdM1\" style=\"height:531px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">If that&rsquo;s your only reason to own a pet because they are adorable at puppy or kitten stage, do think twice because you need to accept the fact that they will eventually grow up and become mature. Despite your lovely puppy and kitten growing up healthily is a good thing, but some people just feeling &ldquo;disappointed&rdquo; that their pet is no longer the mini fluffy ball like they used to be, the expenses doubled because they eat more and requires regular grooming etc. This is the stage where some of these &ldquo;disappointed&rdquo; owners intend to find a new home for their pet.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/uPgP4OHuMNIYmV1t_B0UPsB_-z5d9TcjC2BssuXLiY48fty_3CqE1GW95Aqx3nQn1NKO0aQYmd_XZQybVg72jEHeSxPkkpB56e_M1Jou_a1jqvgDKkbVZWTWW0dEfSUBH9Bgjt_B\" style=\"height:465px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">&nbsp;Before you look for </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><strong>dog breeders</strong></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"> or hanging around </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><strong>pet shops in Malaysia</strong></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">, do think thrice can you handle the responsibility of taking care of your pet for the rest of his life?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">1)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do you have the patience to train them to pee at the right place without being frustrated?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">2)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do you have the time to give them some precious attention and a short walk everyday?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">3)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Are you willing to spend on his food and medical fees?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">4)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Do your family members have any objections in getting a pet?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">5)</span></span></span><span style=\"font-size:6.999999999999999pt\"><span style=\"font-family:\'Times New Roman\'\"><span style=\"color:#333333\">&nbsp; &nbsp; &nbsp; </span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">Most importantly, whether you can ACCEPT THAT THEY WILL GROW UP EVENTUALLY?</span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/QCWDTNDhp7jYedKEnwxbPRJf8qYWtrrv84zKXtPVCxhiI4NolgvGx2QYrGJRv329BkfgRz7ySUTiJz0f9vMNtIgqGFk5Et3vSm0GYQ536JWZXJzqoR4AtIARBnT2b8o1UK54sCIa\" style=\"height:352px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">&nbsp;If you are all set and can carry these huge responsibilities, as well as handling grown-up version of your pet, then CONGRATULATIONS, you are ready for a puppy or kitten!</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh3.googleusercontent.com/bR0O2pBc7-qlSQEVzJKMwpHmbEClWm8l8cus534hdRT7qoDuAp4DA1_B-8HDpmZrqAfYcFzyye4wZEfswOKRBdiX4Fh0WSMiz8gmBwfKdVsuDrJM0vrv9WR5ZdTRXDoUUb6enbWq\" style=\"height:351px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Now, here&rsquo;s the tough part, where to look for reputable </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>kitten and puppy seller</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">, where to search for your preferred breed? For example, </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>British Shorthair Kitten, Persian Kitten, Frenchie Puppy, Corgi Puppy, Toy Poodle Puppy </strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">and others etc. Are you looking for the one that really caught your attention and fall in love with him at the first sight? Wish you have more options? Or you are afraid of dealing with scammers who disappeared after receiving your deposit? Are you worried that the pets you brought home could be problematic and having health issues?&nbsp; Put these worries away, because&hellip;&hellip;.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/KipEBPcGjCKhWy4WvTK4h3-YzxcE1aO4SWN87sOHwU_OfWivL7quajv12_WBtV5-hgF8474olC3-CqVdEGEBQebUbF_vrtjHp10bnj6f6RGHvyQ_sBFQ8K1jZl3p8XWaBz_PW55j\" style=\"height:401px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">TA-DAA! The pawsome platform is here &ndash; MYPETSLIBRARY.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh6.googleusercontent.com/ZfSeAHb6tPg_HxU0zIPj9q07XQh2uxdvHhvce2vLaG_frVHxtp9n7Ro6LxWPtgPFoiKY9XiMD-vCsy0PAJzs2aPKuafLyX_l592HWqbNiBqC9AQBdp_6LFAJOtKMI_rRf3ExylAU\" style=\"height:235px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">Mypetslibrary is the 1</span></span></span></span><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">st</span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> Online Pet Store Platform featuring pets and gathers trusted </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet sellers</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> across Malaysia. In Mypetslibrary, they offer variety breeds of </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>kittens and puppies for sale </strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">by these </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet sellers</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> and </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>pet shops</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">. Rest assured, they are well known and have up to 5 years experiences. You can simply browse for your favourite </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Pomeranian puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> in </span></span></span></span><a href=\"http://www.mypetslibrary.com/\" style=\"text-decoration:none\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#2e6f63\"><span style=\"background-color:#ffffff\">www.mypetslibrary.com</span></span></span></span></a><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\">, scrolling at their full set of pictures and videos, and contact the seller instantly if you fall in love with the </span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"><strong>Pomeranian puppy</strong></span></span></span></span><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\"><span style=\"background-color:#ffffff\"> at first glance and decided to give him a forever loving home. Hence, if you browse for your new pet in Mypetslibrary, there will no worries of dealing with irresponsible sellers, scammers or having limited choice of your favourite breed.</span></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/PwDLiTHQ-pjMAjdr_467rH7x9W9k3sCsgzppYkRCejsOHvWSnSP72q8NzcLpmpt6lopFdtjnixhAOx1NbAudoFoNWiRTXrN3AVY8nUnXaiQbXllTQdMIYsGsRHnolsnY03j6_tcQ\" style=\"height:340px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">If you are not sure on which type of breed suits your lifestyle and home environment, or you are a new paw daddy &amp; mummy, and in need of some advice on how to handle your new family member, you may look for Mypetslibrary&rsquo;s customer service helpline, say HELLO and let&rsquo;s have some Pawtalk. They will answer your inquiries well for sure.&nbsp;</span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:14.5pt\"><span style=\"font-family:Arial\"><span style=\"color:#333333\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In general, looking after a pet may be a big responsibility, but when you consider about their lovely companionship, how they never let you being alone, the fun and laughter they brought to your family, these pets will make all that hard work of yours worthwhile. </span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh4.googleusercontent.com/qD0SOc1YpGWZx9kT37lU_BqiJCDUHKOPy6GVeQ7T5Q_oeNoATi61AUL3vqUr-dgOM934rQIsEImuj5gGgR84wLJWh96aCbeSuU1Ay2QJtdHkaMPQezE20lXd1D-bxPxsvW96DVUM\" style=\"height:172px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:11pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\"><img src=\"https://lh5.googleusercontent.com/GS1fK8WRdJ9wge_GzmdW1TGunhbH38z5EXZCSlm1UTCw1cUAvUyL55Hcl5RPF5hSBktEqp184E6p2rnVbi5EX-rBy53PCUu3DmP0WvLfpGLxxoVBDbm2e81b7yIGdzDG0h4BPPxT\" style=\"height:409px; width:624px\" /></span></span></span></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-05-12 03:22:25', '2020-07-02 08:31:27');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', 'user', 'CIMB Bank', 'User', '123321', 'Active', '2020-05-28 05:03:34', '2020-05-28 05:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Brand 1', 'Available', '2020-04-21 10:20:15', '2020-04-21 10:20:15'),
(2, 'Brand 2', 'Available', '2020-04-21 10:22:14', '2020-04-21 10:22:14'),
(3, 'Brand 3', 'Available', '2020-04-21 10:22:50', '2020-04-21 10:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `breed`
--

CREATE TABLE `breed` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `breed`
--

INSERT INTO `breed` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(9, 'Lizard', 'Available', 3, '2020-04-21 09:25:51', '2020-04-21 09:25:51'),
(10, 'Frog', 'Available', 3, '2020-04-21 09:28:28', '2020-04-21 09:28:28'),
(11, 'Akita', 'Available', 1, '2020-06-24 04:10:25', '2020-06-24 04:10:25'),
(12, 'Alaskan Husky', 'Available', 1, '2020-06-24 04:10:35', '2020-06-24 04:10:35'),
(13, 'Alaskan Malamute', 'Available', 1, '2020-06-24 04:11:03', '2020-06-24 04:11:03'),
(14, 'American Bully', 'Available', 1, '2020-06-24 04:11:25', '2020-06-24 04:11:25'),
(15, 'Bassett Hound', 'Available', 1, '2020-06-24 04:11:31', '2020-06-24 04:11:31'),
(16, 'Beagle', 'Available', 1, '2020-06-24 04:11:37', '2020-06-24 04:11:37'),
(17, 'Bernese Mountain Dog', 'Available', 1, '2020-06-24 04:11:45', '2020-06-24 04:11:45'),
(18, 'Bichon Frise', 'Available', 1, '2020-06-24 04:11:50', '2020-06-24 04:11:50'),
(19, 'Border Collie', 'Available', 1, '2020-06-24 04:11:56', '2020-06-24 04:11:56'),
(20, 'Bull Terrier', 'Available', 1, '2020-06-24 04:12:02', '2020-06-24 04:12:02'),
(21, 'Bulldog', 'Available', 1, '2020-06-24 04:12:07', '2020-06-24 04:12:07'),
(22, 'Cane Corso', 'Available', 1, '2020-06-24 04:12:14', '2020-06-24 04:12:14'),
(23, 'Caucasian Shepherd', 'Available', 1, '2020-06-24 04:12:21', '2020-06-24 04:12:21'),
(24, 'Cavalier King Charles Spaniel', 'Available', 1, '2020-06-24 04:12:29', '2020-06-24 04:12:29'),
(25, 'Chihuahua', 'Available', 1, '2020-06-24 04:12:37', '2020-06-24 04:12:37'),
(26, 'Chow Chow', 'Available', 1, '2020-06-24 04:12:42', '2020-06-24 04:12:42'),
(27, 'Cocker Spaniel', 'Available', 1, '2020-06-24 04:12:48', '2020-06-24 04:12:48'),
(28, 'Dachshund', 'Available', 1, '2020-06-24 04:12:55', '2020-06-24 04:12:55'),
(29, 'Dalmatian', 'Available', 1, '2020-06-24 04:13:01', '2020-06-24 04:13:01'),
(30, 'Dobermann', 'Available', 1, '2020-06-24 04:13:09', '2020-06-24 04:13:09'),
(31, 'English Bulldog', 'Available', 1, '2020-06-24 04:13:16', '2020-06-24 04:13:16'),
(32, 'French Bulldog', 'Available', 1, '2020-06-24 04:13:35', '2020-06-24 04:13:35'),
(33, 'German Shepherd', 'Available', 1, '2020-06-24 04:13:42', '2020-06-24 04:13:42'),
(34, 'Giant Poodle', 'Available', 1, '2020-06-24 04:13:50', '2020-06-24 04:13:50'),
(35, 'Golden Retriever', 'Available', 1, '2020-06-24 04:13:56', '2020-06-24 04:13:56'),
(36, 'Great Dane', 'Available', 1, '2020-06-24 04:14:01', '2020-06-24 04:14:01'),
(37, 'Jack Russell', 'Available', 1, '2020-06-24 04:14:07', '2020-06-24 04:14:07'),
(38, 'Labrador', 'Available', 1, '2020-06-24 04:14:13', '2020-06-24 04:14:13'),
(39, 'Maltese', 'Available', 1, '2020-06-24 04:14:18', '2020-06-24 04:14:18'),
(40, 'Maltipoo', 'Available', 1, '2020-06-24 04:14:28', '2020-06-24 04:14:28'),
(41, 'Miniature poodle', 'Available', 1, '2020-06-24 04:14:34', '2020-06-24 04:14:34'),
(42, 'Mix Breed', 'Available', 1, '2020-06-24 04:14:40', '2020-06-24 04:14:40'),
(43, 'Morkie', 'Available', 1, '2020-06-24 04:14:44', '2020-06-24 04:14:44'),
(44, 'Old English Sheepdog', 'Available', 1, '2020-06-24 04:14:50', '2020-06-24 04:14:50'),
(45, 'Papillon', 'Available', 1, '2020-06-24 04:14:55', '2020-06-24 04:14:55'),
(46, 'Pekingese', 'Available', 1, '2020-06-24 04:15:00', '2020-06-24 04:15:00'),
(47, 'Pinscher', 'Available', 1, '2020-06-24 04:15:08', '2020-06-24 04:15:08'),
(48, 'Pitbull', 'Available', 1, '2020-06-24 04:15:14', '2020-06-24 04:15:14'),
(49, 'Pomapoo', 'Available', 1, '2020-06-24 04:15:20', '2020-06-24 04:15:20'),
(50, 'Pomeranian', 'Available', 1, '2020-06-24 04:15:27', '2020-06-24 04:15:27'),
(51, 'Pomsky', 'Available', 1, '2020-06-24 04:15:32', '2020-06-24 04:15:32'),
(52, 'Pug', 'Available', 1, '2020-06-24 04:15:37', '2020-06-24 04:15:37'),
(53, 'Rottweiler', 'Available', 1, '2020-06-24 04:15:42', '2020-06-24 04:15:42'),
(54, 'Saint Bernard', 'Available', 1, '2020-06-24 04:15:47', '2020-06-24 04:15:47'),
(55, 'Samoyed', 'Available', 1, '2020-06-24 04:15:52', '2020-06-24 04:15:52'),
(56, 'Schnauzer', 'Available', 1, '2020-06-24 04:15:59', '2020-06-24 04:15:59'),
(57, 'Shar Pei', 'Available', 1, '2020-06-24 04:16:05', '2020-06-24 04:16:05'),
(58, 'Shetland Sheepdog', 'Available', 1, '2020-06-24 04:16:11', '2020-06-24 04:16:11'),
(59, 'Shiba Inu', 'Available', 1, '2020-06-24 04:16:18', '2020-06-24 04:16:18'),
(60, 'Shih Tzu', 'Available', 1, '2020-06-24 04:16:24', '2020-06-24 04:16:24'),
(61, 'Siberian Husky', 'Available', 1, '2020-06-24 04:16:30', '2020-06-24 04:16:30'),
(62, 'Silky Terrier', 'Available', 1, '2020-06-24 04:16:37', '2020-06-24 04:16:37'),
(63, 'Standard poodle', 'Available', 1, '2020-06-24 04:16:43', '2020-06-24 04:16:43'),
(64, 'Teacup Pomeranian', 'Available', 1, '2020-06-24 04:16:49', '2020-06-24 04:16:49'),
(65, 'Teacup poodle', 'Available', 1, '2020-06-24 04:16:54', '2020-06-24 04:16:54'),
(66, 'Tiny Poodle', 'Available', 1, '2020-06-24 04:16:59', '2020-06-24 04:16:59'),
(67, 'Toy poodle', 'Available', 1, '2020-06-24 04:17:05', '2020-06-24 04:17:05'),
(68, 'Welsh Corgi', 'Available', 1, '2020-06-24 04:17:43', '2020-06-24 04:17:43'),
(69, 'West Highland Terrier', 'Available', 1, '2020-06-24 04:17:49', '2020-06-24 04:17:49'),
(70, 'Wooly Husky', 'Available', 1, '2020-06-24 04:17:56', '2020-06-24 04:17:56'),
(71, 'Wooly Malamute', 'Available', 1, '2020-06-24 04:18:02', '2020-06-24 04:18:02'),
(72, 'Yorkshire Terrier', 'Available', 1, '2020-06-24 04:18:09', '2020-06-24 04:18:09'),
(73, 'American Curl', 'Available', 2, '2020-06-24 04:19:47', '2020-06-24 04:19:47'),
(74, 'American Shorthair', 'Available', 2, '2020-06-24 04:19:52', '2020-06-24 04:19:52'),
(75, 'Bengal', 'Available', 2, '2020-06-24 04:19:58', '2020-06-24 04:19:58'),
(76, 'British Longhair', 'Available', 2, '2020-06-24 04:20:03', '2020-06-24 04:20:03'),
(77, 'British Shorthair', 'Available', 2, '2020-06-24 04:20:09', '2020-06-24 04:20:09'),
(78, 'Domestic Longhair', 'Available', 2, '2020-06-24 04:20:14', '2020-06-24 04:20:14'),
(79, 'Domestic Shorthair', 'Available', 2, '2020-06-24 04:20:20', '2020-06-24 04:20:20'),
(80, 'Exotic Shorthair', 'Available', 2, '2020-06-24 04:20:29', '2020-06-24 04:20:29'),
(81, 'Himalayan', 'Available', 2, '2020-06-24 04:20:33', '2020-06-24 04:20:33'),
(82, 'Mainecoon', 'Available', 2, '2020-06-24 04:20:40', '2020-06-24 04:20:40'),
(83, 'Minuet', 'Available', 2, '2020-06-24 04:20:45', '2020-06-24 04:20:45'),
(84, 'Mix Breed', 'Available', 2, '2020-06-24 04:20:51', '2020-06-24 04:20:51'),
(85, 'Munchkin', 'Available', 2, '2020-06-24 04:20:56', '2020-06-24 04:20:56'),
(86, 'Persian', 'Available', 2, '2020-06-24 04:21:01', '2020-06-24 04:21:01'),
(87, 'Ragdoll', 'Available', 2, '2020-06-24 04:21:06', '2020-06-24 04:21:06'),
(88, 'Scottishfold', 'Available', 2, '2020-06-24 04:21:12', '2020-06-24 04:21:12'),
(89, 'Scottishstraight', 'Available', 2, '2020-06-24 04:21:19', '2020-06-24 04:21:19'),
(90, 'Sphynx', 'Available', 2, '2020-06-24 04:21:24', '2020-06-24 04:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Grooming', 'Available', '2020-04-21 10:24:39', '2020-04-21 10:24:39'),
(2, 'Accessory', 'Available', '2020-04-21 10:25:19', '2020-04-21 10:25:19'),
(3, 'Food', 'Available', '2020-04-21 10:25:33', '2020-04-21 10:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(4, 'Grey', 'Available', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51'),
(5, 'White', 'Available', 2, '2020-04-21 09:02:02', '2020-04-21 09:02:02'),
(6, 'Orange', 'Available', 2, '2020-04-21 09:13:20', '2020-04-21 09:13:20'),
(7, 'Black', 'Available', 3, '2020-04-21 09:15:13', '2020-04-21 09:15:13'),
(8, 'Red', 'Available', 2, '2020-04-21 09:19:46', '2020-04-21 09:19:46'),
(9, 'Green', 'Available', 3, '2020-04-21 09:32:48', '2020-04-21 09:32:48'),
(10, 'orange', 'Available', 3, '2020-05-27 07:11:39', '2020-05-27 07:11:39'),
(11, 'Apricot', 'Available', 1, '2020-07-01 08:14:32', '2020-07-01 08:14:32'),
(12, 'Black', 'Available', 1, '2020-07-01 08:14:44', '2020-07-01 08:14:44'),
(13, 'Black &amp; Tan', 'Available', 1, '2020-07-01 08:14:55', '2020-07-01 08:14:55'),
(14, 'Black &amp; White', 'Available', 1, '2020-07-01 08:15:04', '2020-07-01 08:15:04'),
(15, 'Blue Merle', 'Available', 1, '2020-07-01 08:15:13', '2020-07-01 08:15:13'),
(16, 'Brown', 'Available', 1, '2020-07-01 08:15:21', '2020-07-01 08:15:21'),
(17, 'Copper &amp; White', 'Available', 1, '2020-07-01 08:15:29', '2020-07-01 08:15:29'),
(18, 'Cream', 'Available', 1, '2020-07-01 08:15:38', '2020-07-01 08:15:38'),
(19, 'Cream White', 'Available', 1, '2020-07-01 08:15:45', '2020-07-01 08:15:45'),
(20, 'Fawn', 'Available', 1, '2020-07-01 08:15:52', '2020-07-01 08:15:52'),
(21, 'Fawn &amp; White', 'Available', 1, '2020-07-01 08:16:12', '2020-07-01 08:16:12'),
(22, 'Gold', 'Available', 1, '2020-07-01 08:16:20', '2020-07-01 08:16:20'),
(23, 'Latte', 'Available', 1, '2020-07-01 08:16:26', '2020-07-01 08:16:26'),
(24, 'Light Brown', 'Available', 1, '2020-07-01 08:16:32', '2020-07-01 08:16:32'),
(25, 'Liver &amp; Tan', 'Available', 1, '2020-07-01 08:16:38', '2020-07-01 08:16:38'),
(26, 'Orange', 'Available', 1, '2020-07-01 08:16:46', '2020-07-01 08:16:46'),
(27, 'Parti', 'Available', 1, '2020-07-01 08:16:53', '2020-07-01 08:16:53'),
(28, 'Red/Copper', 'Available', 1, '2020-07-01 08:17:00', '2020-07-01 08:17:00'),
(29, 'Red Merle', 'Available', 1, '2020-07-01 08:17:26', '2020-07-01 08:17:26'),
(30, 'Sable Brown', 'Available', 1, '2020-07-01 08:17:39', '2020-07-01 08:17:39'),
(31, 'Sable Gray', 'Available', 1, '2020-07-01 08:17:46', '2020-07-01 08:17:46'),
(32, 'Sable Orange', 'Available', 1, '2020-07-01 08:17:52', '2020-07-01 08:17:52'),
(33, 'Salt &amp; Pepper', 'Available', 1, '2020-07-01 08:17:59', '2020-07-01 08:17:59'),
(34, 'Silver', 'Available', 1, '2020-07-01 08:18:06', '2020-07-01 08:18:06'),
(35, 'Silver &amp; White', 'Available', 1, '2020-07-01 08:18:11', '2020-07-01 08:18:11'),
(36, 'Super Red', 'Available', 1, '2020-07-01 08:18:17', '2020-07-01 08:18:17'),
(37, 'Tri-Colour', 'Available', 1, '2020-07-01 08:18:25', '2020-07-01 08:18:25'),
(38, 'White', 'Available', 1, '2020-07-01 08:18:32', '2020-07-01 08:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE `credit_card` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` int(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Female', 'Available', 1, '2020-05-29 06:33:21', '2020-05-29 06:33:21'),
(2, 'Male', 'Available', 1, '2020-05-29 06:33:21', '2020-05-29 06:33:21');

-- --------------------------------------------------------

--
-- Table structure for table `kitten`
--

CREATE TABLE `kitten` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kitten`
--

INSERT INTO `kitten` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `featured_seller`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `date_created`, `date_updated`) VALUES
(1, 'ca38d0e08c2bdd15775c9679bf0b6fe0', 'Persian Kitten For Sale (Male)', 'Mar-LAP-09', 'persian-kitten-for-sale', 'cat,cute,', NULL, '1588', '5.5 months', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', 'Yes', 'Persian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'DOB on 15th September 2019', NULL, NULL, NULL, 'https://player.vimeo.com/video/395647814', 'Penang', 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1a.jpg', 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1b.jpg', 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1c.jpg', 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1d.jpg', NULL, NULL, 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1a.jpg', '2020-05-12 01:55:39', '2020-05-12 01:55:39'),
(2, '257999d62e16d7ee7199dfe2bdfc74e4', 'TICA Cert Mainecoon For Sale', 'Feb-CC-01', 'tica-cert-mainecoon-for-sale-feb-cc-012', 'cat,cute,', NULL, '3388', '4 months', '1st Vaccination Done', 'Yes', 'Female', 'White', 'Medium', 'Available', 'Yes', 'Bengal', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'With TICA Cert,  DOB on 27th October 2019,  1st &amp; 2nd Vaccination done', NULL, NULL, NULL, 'https://player.vimeo.com/video/393865853', 'Penang', '257999d62e16d7ee7199dfe2bdfc74e4cat2a.jpg', '257999d62e16d7ee7199dfe2bdfc74e4cat2b.jpg', '257999d62e16d7ee7199dfe2bdfc74e4cat2c.jpg', '257999d62e16d7ee7199dfe2bdfc74e4cat2d.jpg', NULL, NULL, '257999d62e16d7ee7199dfe2bdfc74e4cat2b.jpg', '2020-05-12 01:59:12', '2020-05-12 01:59:12'),
(3, '360eeb4e64cdb0a7f7ad49ad5b7f7636', 'White Persian Kitten (Male) For Sale', 'DEC-AZ-01', 'white-persian-kitten', 'cat,cute,', NULL, '888', '7 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', 'Yes', 'Persian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', '-', NULL, NULL, NULL, 'https://player.vimeo.com/video/380199673', 'Penang', '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3a.jpg', '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3b.jpg', '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3c.jpg', '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3d.jpg', NULL, NULL, '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3a.jpg', '2020-05-12 02:01:49', '2020-05-12 02:01:49'),
(4, '84c6f98aeaf17f239753b3a159e8b182', 'Mix Breed Kitten (Male) For Sale', 'MM', 'Mix-Breed-Kitten-', 'cat,cute,', NULL, '10888', '4 months', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Sold', 'Yes', 'Persian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'Bengal mix Scottish Fold, 2nd vaccination done', NULL, NULL, NULL, 'https://player.vimeo.com/video/353747694', 'Penang', '84c6f98aeaf17f239753b3a159e8b182cat4a.jpg', '84c6f98aeaf17f239753b3a159e8b182cat4b.jpg', '84c6f98aeaf17f239753b3a159e8b182cat4c.jpg', '84c6f98aeaf17f239753b3a159e8b182cat4d.jpg', NULL, NULL, '84c6f98aeaf17f239753b3a159e8b182cat4a.jpg', '2020-05-12 02:08:47', '2020-05-12 02:08:47'),
(5, 'c1b919abd118cd2f798410f8a1ff81d7', 'Cream Mainecoon (Male) For Sale', 'GM', 'Cream-Mainecoon-Male', 'cat,cute,', NULL, '40888', '4 months', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', 'Yes', 'Persian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', '2nd vaccination done', NULL, NULL, NULL, 'https://player.vimeo.com/video/353747635', 'Penang', 'c1b919abd118cd2f798410f8a1ff81d7cat5a.jpg', 'c1b919abd118cd2f798410f8a1ff81d7cat5b.jpg', 'c1b919abd118cd2f798410f8a1ff81d7cat5c.jpg', 'c1b919abd118cd2f798410f8a1ff81d7cat5d.jpg', NULL, NULL, 'c1b919abd118cd2f798410f8a1ff81d7cat5a.jpg', '2020-05-12 02:14:29', '2020-05-12 02:14:29'),
(6, '8e8515b3b7ffe0febf01bb1debd1c986', ' ', 'testt', ' ', ' ', NULL, '1000', '11', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Small', 'Deleted', ' ', 'Domestic Shorthair', '6166a6cda62a6d910af3482e958c8fa2', NULL, '-', NULL, NULL, NULL, '37992769', 'Penang', '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2d.jpg', '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2a.jpg', '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2b.jpg', '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2c.jpg', NULL, NULL, '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2a.jpg', '2020-07-23 09:16:37', '2020-07-23 09:16:37');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pet_details`
--

CREATE TABLE `pet_details` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pet_details`
--

INSERT INTO `pet_details` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `type`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `date_created`, `date_updated`) VALUES
(1, 'fdec7776d2e1598eb361c679ee371dee', 'MKA French Bulldog Puppy For Sale', 'MAY-RIA-01', 'mka-french-bulldog-puppy-for-sale-may-ria-011', 'cute,dog,', NULL, '38888', '2 months', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Available', 'No', 'Akita', 'b312b2a08c82e29a07d4e54daf2b765c', 'Puppy', 'DOB on 10th March 2020', 'With MKA Cert &amp; Microchip', NULL, NULL, '416350014', 'Penang', 'fdec7776d2e1598eb361c679ee371deepup1a.jpg', 'fdec7776d2e1598eb361c679ee371deepup1b.jpg', 'fdec7776d2e1598eb361c679ee371deepup1c.jpg', 'fdec7776d2e1598eb361c679ee371deepup1d.jpg', NULL, NULL, 'fdec7776d2e1598eb361c679ee371deepup1a.jpg', '2020-05-12 01:30:15', '2020-05-12 01:30:15'),
(2, 'ff988fc8a70f8ab9bd93feb848eb7973', 'Alaskan Husky Puppy For Sale (Male)', 'MAY-PF-04', 'alaskan-husky-puppy-for-sale', 'cute,dog,', NULL, '3688', '6 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Large', 'Available', 'Yes', 'Akita', 'b312b2a08c82e29a07d4e54daf2b765c', 'Puppy', 'Alaskan Malamute x Wooly Husky', 'DOB on 20th March 2020', NULL, NULL, '416349175', 'Penang', 'ff988fc8a70f8ab9bd93feb848eb7973pup2a.jpg', 'ff988fc8a70f8ab9bd93feb848eb7973pup2b.jpg', 'ff988fc8a70f8ab9bd93feb848eb7973pup2c.jpg', 'ff988fc8a70f8ab9bd93feb848eb7973pup2d.jpg', NULL, NULL, 'ff988fc8a70f8ab9bd93feb848eb7973pup2a.jpg', '2020-05-12 01:34:18', '2020-05-12 01:34:18'),
(3, '25109f070e5016b8ab6f98cb09a97748', 'Beagle Puppy For Sale (Male)', 'MAY-PW-01', 'beagle-puppy-for-sale-male-may-pw-011', 'cute,dog,', NULL, '1899', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Medium', 'Available', 'Yes', 'Akita', 'b312b2a08c82e29a07d4e54daf2b765c', 'Puppy', '-', NULL, NULL, NULL, '383039356', 'Penang', '25109f070e5016b8ab6f98cb09a97748pup3a.jpg', '25109f070e5016b8ab6f98cb09a97748pup3b.jpg', '25109f070e5016b8ab6f98cb09a97748pup3c.jpg', '25109f070e5016b8ab6f98cb09a97748pup3d.jpg', NULL, NULL, '25109f070e5016b8ab6f98cb09a97748pup3a.jpg', '2020-05-12 01:40:17', '2020-05-12 01:40:17'),
(4, '3a8ce037a5f1913b554a991bf07e64ae', 'MKA Shiba Inu Puppy For Sale (Female)', 'MAY-MP-SI', 'mka-shiba-inu-puppy-for-sale', 'cute,dog,', NULL, '3388', '3 months', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Available', 'Yes', 'Akita', 'b312b2a08c82e29a07d4e54daf2b765c', 'Puppy', 'DOB on 29th January 2020', 'Champion Bloodline', 'With MKA Cert &amp; Microchip', NULL, '414476414', 'Penang', '3a8ce037a5f1913b554a991bf07e64aepup4a.jpg', '3a8ce037a5f1913b554a991bf07e64aepup4b.jpg', '3a8ce037a5f1913b554a991bf07e64aepup4c.jpg', '3a8ce037a5f1913b554a991bf07e64aepup4d.jpg', NULL, NULL, '3a8ce037a5f1913b554a991bf07e64aepup4a.jpg', '2020-05-12 01:45:23', '2020-05-12 01:45:23'),
(5, '2a08fa55361d37df161a9c5841ed3a96', 'Yorkshire Terrier Puppy For Sale (Female)', 'APR-MP-35', 'yorkshire-terrier-puppy-for-sale', 'cute,dog,', NULL, '300', '3 months old', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Small', 'Available', 'Yes', 'Akita', 'b312b2a08c82e29a07d4e54daf2b765c', 'Puppy', 'DOB with 29th January 2020', 'With MKA Cert &amp; Microchip', 'Another side of ears will be rigid standing soon', NULL, '412836212', 'Penang', '2a08fa55361d37df161a9c5841ed3a96pup5a.jpg', '15947770792a08fa55361d37df161a9c5841ed3a96.png', '15947770932a08fa55361d37df161a9c5841ed3a96.png', '2a08fa55361d37df161a9c5841ed3a961590976016pup5b.jpg', NULL, NULL, '2a08fa55361d37df161a9c5841ed3a96pup5a.jpg', '2020-05-12 01:51:16', '2020-05-12 01:51:16'),
(6, 'ca38d0e08c2bdd15775c9679bf0b6fe0', 'Persian Kitten For Sale (Male)', 'Mar-LAP-09', 'persian-kitten-for-sale', 'cat,cute,', NULL, '1588', '5.5 months', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', 'Yes', 'Persian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Kitten', 'DOB on 15th September 2019', NULL, NULL, NULL, '395647814', 'Penang', 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1a.jpg', 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1b.jpg', 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1c.jpg', 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1d.jpg', '1595409177ca38d0e08c2bdd15775c9679bf0b6fe0cat4a.jpg', NULL, 'ca38d0e08c2bdd15775c9679bf0b6fe0cat1a.jpg', '2020-05-12 01:55:39', '2020-05-12 01:55:39'),
(7, '257999d62e16d7ee7199dfe2bdfc74e4', 'TICA Cert Mainecoon For Sale', 'Feb-CC-01', 'tica-cert-mainecoon-for-sale-feb-cc-012', 'cat,cute,', NULL, '3388', '4 months', '2nd Vaccination Done', 'Yes', 'Female', 'White', 'Medium', 'Available', 'Yes', 'Bengal', 'b312b2a08c82e29a07d4e54daf2b765c', 'Kitten', 'With TICA Cert', 'DOB on 27th October 2019', '1st &amp; 2nd Vaccination done', NULL, '393865853', 'Penang', '257999d62e16d7ee7199dfe2bdfc74e4cat2a.jpg', '257999d62e16d7ee7199dfe2bdfc74e4cat2b.jpg', '257999d62e16d7ee7199dfe2bdfc74e4cat2c.jpg', '257999d62e16d7ee7199dfe2bdfc74e4cat2d.jpg', NULL, NULL, '257999d62e16d7ee7199dfe2bdfc74e4cat2b.jpg', '2020-05-12 01:59:12', '2020-05-12 01:59:12'),
(8, '360eeb4e64cdb0a7f7ad49ad5b7f7636', 'White Persian Kitten (Male) For Sale', 'DEC-AZ-01', 'white-persian-kitten', 'cat,cute,', NULL, '888', '7 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Available', 'Yes', 'Persian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Kitten', '-', NULL, NULL, NULL, '380199673', 'Penang', '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3a.jpg', '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3b.jpg', '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3c.jpg', '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3d.jpg', NULL, NULL, '360eeb4e64cdb0a7f7ad49ad5b7f7636cat3a.jpg', '2020-05-12 02:01:49', '2020-05-12 02:01:49'),
(9, '84c6f98aeaf17f239753b3a159e8b182', 'Mix Breed Kitten (Male) For Sale', 'MM', 'Mix-Breed-Kitten-', 'cat,cute,', NULL, '10888', '4 months', '2nd Vaccination Done', 'Yes', 'Male', 'Grey', 'Medium', 'Sold', 'Yes', 'Persian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Kitten', 'Bengal mix Scottish Fold', '2nd vaccination done', NULL, NULL, '353747694', 'Penang', '84c6f98aeaf17f239753b3a159e8b182cat4a.jpg', '84c6f98aeaf17f239753b3a159e8b182cat4b.jpg', '84c6f98aeaf17f239753b3a159e8b182cat4c.jpg', '84c6f98aeaf17f239753b3a159e8b182cat4d.jpg', NULL, NULL, '84c6f98aeaf17f239753b3a159e8b182cat4a.jpg', '2020-05-12 02:08:47', '2020-05-12 02:08:47'),
(10, 'c1b919abd118cd2f798410f8a1ff81d7', 'Cream Mainecoon (Male) For Sale', 'GM', 'Cream-Mainecoon-Male', 'cat,cute,', NULL, '40888', '4 months', '2nd Vaccination Done', 'Yes', 'Male', 'White', 'Medium', 'Available', 'Yes', 'Persian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Kitten', '2nd vaccination done', NULL, NULL, NULL, '353747635', 'Penang', 'c1b919abd118cd2f798410f8a1ff81d7cat5a.jpg', 'c1b919abd118cd2f798410f8a1ff81d7cat5b.jpg', 'c1b919abd118cd2f798410f8a1ff81d7cat5c.jpg', 'c1b919abd118cd2f798410f8a1ff81d7cat5d.jpg', '1595487830c1b919abd118cd2f798410f8a1ff81d7cat2c.jpg', '1595487852c1b919abd118cd2f798410f8a1ff81d7cat1b.jpg', 'c1b919abd118cd2f798410f8a1ff81d7cat5a.jpg', '2020-05-12 02:14:29', '2020-05-12 02:14:29'),
(11, 'b5a2d1ab7c7115cbd5c7fc8382558438', 'Reptile 1', 'reptile1590563604', 'reptile-1-for-sale', 'reptile,', NULL, '1000', '2', '1st Vaccination Done', 'No', 'Male', 'orange', 'Small', 'Available', 'No', 'Lizard', '6e4c10ac188a7bcdf1e42b975a2bb84a', 'Reptile', '-', NULL, NULL, NULL, '392887053', 'Malacca', 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', NULL, NULL, '', '2020-05-27 07:13:24', '2020-05-27 07:13:24'),
(12, 'f0f129ae06aeb98f454eabbb63d76ab6', 'Reptile 2', 'reptile1590563684', 'reptile-2-for-sale', 'reptile, kakaka', NULL, '2088', '2', '1st Vaccination Done', 'No', 'Female', 'Black', 'Medium', 'Available', 'Yes', 'Terrier', '24160b0831be5b60214634d01a9459bc', 'Reptile', '-', NULL, NULL, NULL, '392887053', 'Kuala Lumpur', 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', NULL, NULL, '', '2020-05-27 07:14:44', '2020-05-27 07:14:44'),
(13, 'd3aae8ca5d06379ba61134348690362f', 'Reptile 3', 'reptile1590563843', 'reptile-3-for-sale', 'reptile,', NULL, '2888', '3', '1st Vaccination Done', 'Yes', 'Female', 'Green', 'Small', 'Available', 'No', 'Lizard', 'd13b9e3753d17def166d762326f88610', 'Reptile', '-', NULL, NULL, NULL, '392887053', 'Penang', 'd3aae8ca5d06379ba61134348690362frep3.jpg', 'd3aae8ca5d06379ba61134348690362frep3.jpg', 'd3aae8ca5d06379ba61134348690362frep3.jpg', 'd3aae8ca5d06379ba61134348690362frep3.jpg', NULL, NULL, '', '2020-05-27 07:17:23', '2020-05-27 07:17:23'),
(14, '3f64e4ceac8f57e7490d6bfcfed103ea', 'Reptile 4', 'reptile1590564100', 'reptile-4-for-sale', 'reptile,', NULL, '3088', '2', '1st Vaccination Done', 'Yes', 'Male', 'orange', 'Medium', 'Available', 'Yes', 'Lizard', '6e4c10ac188a7bcdf1e42b975a2bb84a', 'Reptile', '-', NULL, NULL, NULL, '392887053', 'Malacca', '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', NULL, NULL, '', '2020-05-27 07:21:40', '2020-05-27 07:21:40'),
(15, '10da53b8df11f36c68c2c17bb1026222', 'Reptile 5', 'reptile1590564181', 'reptile-5-for-sale', 'reptile, wohooo', NULL, '10888', '2', '1st Vaccination Done', 'Yes', 'Male', 'Green', 'Medium', 'Available', 'Yes', 'Lizard', '24160b0831be5b60214634d01a9459bc', 'Reptile', '-', NULL, NULL, NULL, '392887053', 'Kuala Lumpur', '159547505810da53b8df11f36c68c2c17bb1026222cute-puppy.png', '159547504210da53b8df11f36c68c2c17bb10262221024.png', '159547502610da53b8df11f36c68c2c17bb1026222article.png', '159547498310da53b8df11f36c68c2c17bb1026222adoption-bg-01.jpg', '159547495710da53b8df11f36c68c2c17bb1026222attention.png', '159547496910da53b8df11f36c68c2c17bb1026222attention2.png', '', '2020-05-27 07:23:01', '2020-05-27 07:23:01'),
(16, 'a94d34c97480b8885a287cc543b6f50a', ' ', 'test', ' ', ' ', NULL, '2000', '11', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Deleted', ' ', 'Alaskan Malamute', 'fdf5a4b4889f888d34a2838a42dce7cd', 'puppy', '-', NULL, NULL, NULL, '379922769', 'Penang', '1595495685a94d34c97480b8885a287cc543b6f50apup5d.jpg', '1595495685a94d34c97480b8885a287cc543b6f50apup5c.jpg', '1595495685a94d34c97480b8885a287cc543b6f50apup5a.jpg', '1595495685a94d34c97480b8885a287cc543b6f50apup5b.jpg', NULL, NULL, '1595495685a94d34c97480b8885a287cc543b6f50apup5c.jpg', '2020-07-23 09:14:22', '2020-07-23 09:14:22'),
(17, '8e8515b3b7ffe0febf01bb1debd1c986', ' ', 'testt', ' ', ' ', NULL, '1000', '11', '1st Vaccination Done', 'Yes', 'Male', 'Grey', 'Small', 'Deleted', ' ', 'Domestic Shorthair', '6166a6cda62a6d910af3482e958c8fa2', 'Kitten', '-', NULL, NULL, NULL, '37992769', 'Penang', '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2d.jpg', '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2a.jpg', '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2b.jpg', '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2c.jpg', NULL, NULL, '15954958098e8515b3b7ffe0febf01bb1debd1c986cat2a.jpg', '2020-07-23 09:16:37', '2020-07-23 09:16:37'),
(18, '2ebd1af60651ffdce829fa8d8f309d82', ' ', 'JUL-YM-03', ' ', ' ', NULL, '3000', '7 Weeks', '1st Vaccination Done', 'Yes', 'Female', 'White', 'Small', 'Available', ' ', 'Pomeranian', 'b312b2a08c82e29a07d4e54daf2b765c', 'puppy', 'Come with Microchip No 1204 and MKA Certificate (FROM Malaysia Kennel Association) With Import Taiwan Parent', NULL, NULL, NULL, '440734944', 'Penang', '15955539062ebd1af60651ffdce829fa8d8f309d82dog3.jpg', '15955539072ebd1af60651ffdce829fa8d8f309d82dog1.jpg', '15955539072ebd1af60651ffdce829fa8d8f309d82dog2.jpg', NULL, NULL, NULL, '15955539072ebd1af60651ffdce829fa8d8f309d82dog2.jpg', '2020-07-24 01:24:41', '2020-07-24 01:24:41'),
(19, '1ace82f92431a66e5a92d9492440eda0', ' ', 'JUL-MP-08', ' ', ' ', NULL, '5000', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Copper &amp; White', 'Medium', 'Available', ' ', 'Welsh Corgi', 'b312b2a08c82e29a07d4e54daf2b765c', 'puppy', '', NULL, NULL, NULL, '439881094', 'Penang', '15955540981ace82f92431a66e5a92d9492440eda0doga4.jpg', '15955540981ace82f92431a66e5a92d9492440eda0doga1.jpg', '15955540981ace82f92431a66e5a92d9492440eda0doga2.jpg', '15955540981ace82f92431a66e5a92d9492440eda0doga3.jpg', NULL, NULL, '15955540981ace82f92431a66e5a92d9492440eda0doga1.jpg', '2020-07-24 01:28:10', '2020-07-24 01:28:10'),
(20, '675a0a889779d4fbf779f58760f362ae', ' ', 'JUL-WW-01', ' ', ' ', NULL, '2000', '2 months', '1st Vaccination Done', 'Yes', 'Female', 'White', 'Small', 'Available', ' ', 'Schnauzer', 'b312b2a08c82e29a07d4e54daf2b765c', 'puppy', 'Short Leg', NULL, NULL, NULL, '436506013', 'Penang', '1595554354675a0a889779d4fbf779f58760f362aedogb1.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb2.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb3.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb5.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb4.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb6.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb1.jpg', '2020-07-24 01:32:27', '2020-07-24 01:32:27'),
(21, '90075ab9581eb002175bd9a88febe543', ' ', 'JUL-MP-03', ' ', ' ', NULL, '4000', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Cream', 'Small', 'Available', ' ', 'Dachshund', 'b312b2a08c82e29a07d4e54daf2b765c', 'puppy', 'With MKA Cert &amp; Microchip', NULL, NULL, NULL, '435822780', 'Penang', '159555455890075ab9581eb002175bd9a88febe543dogc1.jpg', '159555455890075ab9581eb002175bd9a88febe543dogc2.jpg', '159555455890075ab9581eb002175bd9a88febe543dogc3.jpg', NULL, NULL, NULL, '159555455890075ab9581eb002175bd9a88febe543dogc1.jpg', '2020-07-24 01:35:48', '2020-07-24 01:35:48'),
(22, 'ca2bf448c82fe6b3e89621145290cd07', ' ', 'asd', ' ', ' ', NULL, '124356', '345', '1st Vaccination Done', 'Yes', 'Male', 'Apricot', 'Large', 'Available', ' ', 'Alaskan Malamute', 'b312b2a08c82e29a07d4e54daf2b765c', 'puppy', '1234', NULL, NULL, NULL, '12345', 'Penang', '1595842510ca2bf448c82fe6b3e89621145290cd071.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd072.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd073.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd074.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd075.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd076.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd075.jpg', '2020-07-27 09:33:37', '2020-07-27 09:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `animal_type` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keyword_one` varchar(255) NOT NULL,
  `featured_product` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `free_gift` varchar(255) NOT NULL,
  `free_gift_img` varchar(255) NOT NULL,
  `free_gift_description` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `category`, `brand`, `name`, `sku`, `slug`, `animal_type`, `expiry_date`, `status`, `description`, `keyword_one`, `featured_product`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `free_gift`, `free_gift_img`, `free_gift_description`, `date_created`, `date_updated`) VALUES
(1, 'd83d2545ed58fb580453e85d6ad6e4ea', 'Food', 'Brand 1', 'Pedigree', '', 'https://www.pedigree.com', 'Puppy', '2021-12-25', 'Available', 'Delicious Food, Dogs Favorite', 'pedigree, dog&#039;s food', 'Yes', 'http://pedigree.com', 'pedigree1.jpg', 'pedigree2.jpg', 'pedigree3.jpg', 'pedigree4.jpg', '', '', '', '2020-06-04 02:31:47', '2020-06-04 02:31:47'),
(2, 'fcdce3745589555162f2479d56924866', 'Food', 'Brand 2', 'Whiskas', '', 'https://www.whiskas.com', 'Kitten', '2021-07-22', 'Available', 'Cats aged 1-6 need lots of playtime and a balanced diet to help them stay lean and healthy. Cats are carnivores while humans omnivores, so cats need two times more protein', 'cat food, whiskas', 'Yes', 'http://whiskas.com', 'product1.jpg', 'product2.jpg', 'product3.jpg', 'product4.jpg', '', '', '', '2020-06-04 02:39:54', '2020-06-04 02:39:54'),
(3, '72de97bd3c37723cfd0425bebd8432b9', 'Accessory', 'Brand 3', 'Cat Wool Toy', '', 'https://www.pets.com', 'Kitten', '', 'Available', 'Made of soft wool material, it is environmentally friendly and non-toxic.When your pet chews or squeezes it, it&#039;s fun.', 'cat toy, wool, soft material', 'Yes', 'http://cattoy.com', 'wool1.jpg', 'wool5.jpg', 'wool3.jpg', 'wool4.jpg', '', '', '', '2020-06-04 02:51:14', '2020-06-04 02:51:14'),
(4, '7a3f87c9d4a98b78597d4310ef91175d', 'Accessory', 'Brand 1', 'Bone Toy', '', 'https://www.toypets.com', 'Puppy', '', 'Available', 'package includes:1 x toy bone 100% brand new, high quality Safe, non-toxic, excellent hardness Effectively clean teeth, design color Daily interactive toy for your dog', 'dog toy, bone', 'Yes', 'http://dogbonetoy.com', 'toy1.jpg', '', '', '', '', '', '', '2020-06-04 02:54:08', '2020-06-04 02:54:08'),
(5, '88ed8f3c474237be8026df5d08e1e4cf', 'Accessory', 'Brand 2', 'Ball Toy for Dog', '', 'https://www.pets.com', 'Puppy', '', 'Available', 'Top selling and most favorite', 'dog toy, ball', 'Yes', 'http://dog.com', 'ball1.jpg', '', '', '', '', '', '', '2020-06-04 02:56:26', '2020-06-04 02:56:26'),
(6, '083aca31db3f2d46204b16c3a6e5f084', 'Food', 'Brand 2', 'Alpo', '', 'https://www.pets.com', 'Puppy', '', 'Available', 'Product Description Features Great meaty taste, Everything your dog needs - protein, antioxidants, calcium, omega 3&amp;6, Complete &amp; balanced for adult dogs Product', 'Alpo, dog&#039;s food', 'Yes', 'http://alpo.com', 'alpo1.jpg', 'alpo2.png', 'alpo3.jpg', 'alpo4.png', '', '', '', '2020-06-04 03:00:26', '2020-06-04 03:00:26');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `totalProductPrice` decimal(50,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `puppy`
--

CREATE TABLE `puppy` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) NOT NULL,
  `img_name` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puppy`
--

INSERT INTO `puppy` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `featured_seller`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `img_name`, `date_created`, `date_updated`) VALUES
(1, 'fdec7776d2e1598eb361c679ee371dee', 'MKA French Bulldog Puppy For Sale', 'MAY-RIA-01', 'mka-french-bulldog-puppy-for-sale-may-ria-011', 'cute,dog,', NULL, '38888', '2 months', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Available', 'No', 'Puddle', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'DOB on 10th March 2020  With MKA Cert &amp; Microchip', NULL, NULL, NULL, 'https://player.vimeo.com/video/416350014', 'Penang', 'fdec7776d2e1598eb361c679ee371deepup1a.jpg', 'fdec7776d2e1598eb361c679ee371deepup1b.jpg', 'fdec7776d2e1598eb361c679ee371deepup1c.jpg', 'fdec7776d2e1598eb361c679ee371deepup1d.jpg', NULL, NULL, 'fdec7776d2e1598eb361c679ee371deepup1a.jpg', '', '2020-05-12 01:30:15', '2020-05-12 01:30:15'),
(2, 'ff988fc8a70f8ab9bd93feb848eb7973', 'Alaskan Husky Puppy For Sale (Male)', 'MAY-PF-04', 'alaskan-husky-puppy-for-sale', 'cute,dog,', NULL, '3688', '6 weeks', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Large', 'Available', 'Yes', 'Terrier', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'Alaskan Malamute x Wooly Husky  DOB on 20th March 2020', NULL, NULL, NULL, 'https://player.vimeo.com/video/416349175', 'Penang', 'ff988fc8a70f8ab9bd93feb848eb7973pup2a.jpg', 'ff988fc8a70f8ab9bd93feb848eb7973pup2b.jpg', 'ff988fc8a70f8ab9bd93feb848eb7973pup2c.jpg', 'ff988fc8a70f8ab9bd93feb848eb7973pup2d.jpg', NULL, NULL, 'ff988fc8a70f8ab9bd93feb848eb7973pup2a.jpg', '1591338241.png', '2020-05-12 01:34:18', '2020-05-12 01:34:18'),
(3, '25109f070e5016b8ab6f98cb09a97748', 'Beagle Puppy For Sale (Male)', 'MAY-PW-01', 'beagle-puppy-for-sale-male-may-pw-011', 'cute,dog,', NULL, '1899', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Black', 'Medium', 'Available', 'Yes', 'Terrier', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'good puppy, nice puppy', NULL, NULL, NULL, 'https://player.vimeo.com/video/383039356', 'Penang', '25109f070e5016b8ab6f98cb09a97748pup3a.jpg', '25109f070e5016b8ab6f98cb09a97748pup3b.jpg', '25109f070e5016b8ab6f98cb09a97748pup3c.jpg', '25109f070e5016b8ab6f98cb09a97748pup3d.jpg', NULL, NULL, '25109f070e5016b8ab6f98cb09a97748pup3a.jpg', '', '2020-05-12 01:40:17', '2020-05-12 01:40:17'),
(4, '3a8ce037a5f1913b554a991bf07e64ae', 'MKA Shiba Inu Puppy For Sale (Female)', 'MAY-MP-SI', 'mka-shiba-inu-puppy-for-sale', 'cute,dog,', NULL, '3388', '3 months', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Available', 'Yes', 'Puddle', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'DOB on 29th January 2020,  Champion Bloodline,   With MKA Cert &amp; Microchip', NULL, NULL, NULL, 'https://player.vimeo.com/video/414476414', 'Penang', '3a8ce037a5f1913b554a991bf07e64aepup4a.jpg', '3a8ce037a5f1913b554a991bf07e64aepup4b.jpg', '3a8ce037a5f1913b554a991bf07e64aepup4c.jpg', '3a8ce037a5f1913b554a991bf07e64aepup4d.jpg', NULL, NULL, '3a8ce037a5f1913b554a991bf07e64aepup4a.jpg', '', '2020-05-12 01:45:23', '2020-05-12 01:45:23'),
(5, '2a08fa55361d37df161a9c5841ed3a96', 'Yorkshire Terrier Puppy For Sale (Female)', 'APR-MP-35', 'yorkshire-terrier-puppy-for-sale', 'cute,dog,', NULL, '300', '3 months old', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Small', 'Available', 'Yes', 'Akita', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'DOB with 29th January 2020,  With MKA Cert &amp; Microchip,  Another side of ears will be rigid standing soon', NULL, NULL, NULL, 'https://player.vimeo.com/video/412836212', 'Penang', '2a08fa55361d37df161a9c5841ed3a96pup5a.jpg', '15947770792a08fa55361d37df161a9c5841ed3a96.png', '15947770932a08fa55361d37df161a9c5841ed3a96.png', '2a08fa55361d37df161a9c5841ed3a961590976016pup5b.jpg', NULL, NULL, '2a08fa55361d37df161a9c5841ed3a96pup5a.jpg', '', '2020-05-12 01:51:16', '2020-05-12 01:51:16'),
(6, 'a94d34c97480b8885a287cc543b6f50a', ' ', 'test', ' ', ' ', NULL, '2000', '11', '1st Vaccination Done', 'Yes', 'Female', 'Brown', 'Medium', 'Deleted', ' ', 'Alaskan Malamute', 'fdf5a4b4889f888d34a2838a42dce7cd', 'No', '-', NULL, NULL, NULL, '379922769', 'Penang', '1595495685a94d34c97480b8885a287cc543b6f50apup5d.jpg', '1595495685a94d34c97480b8885a287cc543b6f50apup5c.jpg', '1595495685a94d34c97480b8885a287cc543b6f50apup5a.jpg', '1595495685a94d34c97480b8885a287cc543b6f50apup5b.jpg', NULL, NULL, '1595495685a94d34c97480b8885a287cc543b6f50apup5a.jpg', NULL, '2020-07-23 09:14:22', '2020-07-23 09:14:22'),
(7, '2ebd1af60651ffdce829fa8d8f309d82', ' ', 'JUL-YM-03', ' ', ' ', NULL, '3000', '7 Weeks', '1st Vaccination Done', 'Yes', 'Female', 'White', 'Small', 'Available', ' ', 'Pomeranian', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'Come with Microchip No 1204 and MKA Certificate (FROM Malaysia Kennel Association) With Import Taiwan Parent', NULL, NULL, NULL, '440734944', 'Penang', '15955539062ebd1af60651ffdce829fa8d8f309d82dog3.jpg', '15955539072ebd1af60651ffdce829fa8d8f309d82dog1.jpg', '15955539072ebd1af60651ffdce829fa8d8f309d82dog2.jpg', NULL, NULL, NULL, '15955539072ebd1af60651ffdce829fa8d8f309d82dog2.jpg', NULL, '2020-07-24 01:24:41', '2020-07-24 01:24:41'),
(8, '1ace82f92431a66e5a92d9492440eda0', ' ', 'JUL-MP-08', ' ', ' ', NULL, '5000', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Copper &amp; White', 'Medium', 'Available', ' ', 'Welsh Corgi', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', '', NULL, NULL, NULL, '439881094', 'Penang', '15955540981ace82f92431a66e5a92d9492440eda0doga4.jpg', '15955540981ace82f92431a66e5a92d9492440eda0doga1.jpg', '15955540981ace82f92431a66e5a92d9492440eda0doga2.jpg', '15955540981ace82f92431a66e5a92d9492440eda0doga3.jpg', NULL, NULL, '15955540981ace82f92431a66e5a92d9492440eda0doga1.jpg', NULL, '2020-07-24 01:28:10', '2020-07-24 01:28:10'),
(9, '675a0a889779d4fbf779f58760f362ae', ' ', 'JUL-WW-01', ' ', ' ', NULL, '2000', '2 months', '1st Vaccination Done', 'Yes', 'Female', 'White', 'Small', 'Available', ' ', 'Schnauzer', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'Short Leg', NULL, NULL, NULL, '436506013', 'Penang', '1595554354675a0a889779d4fbf779f58760f362aedogb1.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb2.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb3.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb5.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb4.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb6.jpg', '1595554354675a0a889779d4fbf779f58760f362aedogb1.jpg', NULL, '2020-07-24 01:32:26', '2020-07-24 01:32:26'),
(10, '90075ab9581eb002175bd9a88febe543', ' ', 'JUL-MP-03', ' ', ' ', NULL, '4000', '2 months', '1st Vaccination Done', 'Yes', 'Male', 'Cream', 'Small', 'Available', ' ', 'Dachshund', 'b312b2a08c82e29a07d4e54daf2b765c', 'Yes', 'With MKA Cert &amp; Microchip', NULL, NULL, NULL, '435822780', 'Penang', '159555455890075ab9581eb002175bd9a88febe543dogc1.jpg', '159555455890075ab9581eb002175bd9a88febe543dogc2.jpg', '159555455890075ab9581eb002175bd9a88febe543dogc3.jpg', NULL, NULL, NULL, '159555455890075ab9581eb002175bd9a88febe543dogc1.jpg', NULL, '2020-07-24 01:35:48', '2020-07-24 01:35:48'),
(11, 'ca2bf448c82fe6b3e89621145290cd07', ' ', 'asd', ' ', ' ', NULL, '124356', '345', '1st Vaccination Done', 'Yes', 'Male', 'Apricot', 'Large', 'Available', ' ', 'Alaskan Malamute', 'b312b2a08c82e29a07d4e54daf2b765c', NULL, '1234', NULL, NULL, NULL, '12345', 'Penang', '1595842510ca2bf448c82fe6b3e89621145290cd071.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd072.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd073.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd074.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd075.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd076.jpg', '1595842510ca2bf448c82fe6b3e89621145290cd075.jpg', NULL, '2020-07-27 09:33:37', '2020-07-27 09:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `reported_article`
--

CREATE TABLE `reported_article` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `article_uid` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reported_article`
--

INSERT INTO `reported_article` (`id`, `uid`, `username`, `article_uid`, `reason`, `result`, `date_created`, `date_updated`) VALUES
(1, '2255a46dc84bcb6dc0ef8309a71e5ce0', 'user', 'b6514af54ff5209a1bc8b9e0ae1a6ce6', 'asd asd asd', NULL, '2020-05-13 04:24:36', '2020-05-13 04:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `reported_reviews`
--

CREATE TABLE `reported_reviews` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `article_uid` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reported_reviews`
--

INSERT INTO `reported_reviews` (`id`, `uid`, `username`, `article_uid`, `reason`, `result`, `date_created`, `date_updated`) VALUES
(1, '87fe7166a03c03a394405881c8929e4f', 'user', 'c2df33a07c1570f52333555c813bbab5', 'asd', NULL, '2020-05-13 02:31:14', '2020-05-13 02:31:14');

-- --------------------------------------------------------

--
-- Table structure for table `reptile`
--

CREATE TABLE `reptile` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `price` decimal(8,0) NOT NULL,
  `age` varchar(255) NOT NULL,
  `vaccinated` varchar(255) NOT NULL,
  `dewormed` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `breed` varchar(255) NOT NULL,
  `seller` varchar(255) NOT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `details_two` text DEFAULT NULL,
  `details_three` text DEFAULT NULL,
  `details_four` text DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `default_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reptile`
--

INSERT INTO `reptile` (`id`, `uid`, `name`, `sku`, `slug`, `keyword_one`, `keyword_two`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `featured_seller`, `details`, `details_two`, `details_three`, `details_four`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `image_six`, `default_image`, `date_created`, `date_updated`) VALUES
(1, 'b5a2d1ab7c7115cbd5c7fc8382558438', 'Reptile 1', 'reptile1590563604', 'reptile-1-for-sale', 'reptile,', NULL, '1000', '2', '1st Vaccination Done', 'No', 'Male', 'orange', 'Small', 'Available', 'No', 'Lizard', '6e4c10ac188a7bcdf1e42b975a2bb84a', NULL, '-', NULL, NULL, NULL, 'https://player.vimeo.com/video/392887053', 'Malacca', 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', NULL, NULL, 'b5a2d1ab7c7115cbd5c7fc8382558438rep1.jpg', '2020-05-27 07:13:24', '2020-05-27 07:13:24'),
(2, 'f0f129ae06aeb98f454eabbb63d76ab6', 'Reptile 2', 'reptile1590563684', 'reptile-2-for-sale', 'reptile, kakaka', NULL, '2088', '2', '1st Vaccination Done', 'No', 'Female', 'Black', 'Medium', 'Available', 'Yes', 'Terrier', '24160b0831be5b60214634d01a9459bc', NULL, '-', NULL, NULL, NULL, 'https://player.vimeo.com/video/392887053', 'Kuala Lumpur', 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', NULL, NULL, 'f0f129ae06aeb98f454eabbb63d76ab6rep2.jpg', '2020-05-27 07:14:44', '2020-05-27 07:14:44'),
(3, 'd3aae8ca5d06379ba61134348690362f', 'Reptile 3', 'reptile1590563843', 'reptile-3-for-sale', 'reptile,', NULL, '2888', '3', '1st Vaccination Done', 'Yes', 'Female', 'Green', 'Small', 'Available', 'No', 'Lizard', 'd13b9e3753d17def166d762326f88610', NULL, '-', NULL, NULL, NULL, 'https://player.vimeo.com/video/392887053', 'Penang', 'd3aae8ca5d06379ba61134348690362frep3.jpg', 'd3aae8ca5d06379ba61134348690362frep3.jpg', 'd3aae8ca5d06379ba61134348690362frep3.jpg', 'd3aae8ca5d06379ba61134348690362frep3.jpg', NULL, NULL, 'd3aae8ca5d06379ba61134348690362frep3.jpg', '2020-05-27 07:17:23', '2020-05-27 07:17:23'),
(4, '3f64e4ceac8f57e7490d6bfcfed103ea', 'Reptile 4', 'reptile1590564100', 'reptile-4-for-sale', 'reptile,', NULL, '3088', '2', '1st Vaccination Done', 'Yes', 'Male', 'orange', 'Medium', 'Available', 'Yes', 'Lizard', '6e4c10ac188a7bcdf1e42b975a2bb84a', NULL, '-', NULL, NULL, NULL, 'https://player.vimeo.com/video/392887053', 'Malacca', '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', NULL, NULL, '3f64e4ceac8f57e7490d6bfcfed103earep4.jpg', '2020-05-27 07:21:40', '2020-05-27 07:21:40'),
(5, '10da53b8df11f36c68c2c17bb1026222', 'Reptile 5', 'reptile1590564181', 'reptile-5-for-sale', 'reptile, wohooo', NULL, '10888', '2', '1st Vaccination Done', 'Yes', 'Male', 'Green', 'Medium', 'Available', 'Yes', 'Lizard', '24160b0831be5b60214634d01a9459bc', NULL, '-', NULL, NULL, NULL, 'https://player.vimeo.com/video/392887053', 'Kuala Lumpur', '10da53b8df11f36c68c2c17bb1026222rep5.jpg', '10da53b8df11f36c68c2c17bb1026222rep5.jpg', '10da53b8df11f36c68c2c17bb1026222rep5.jpg', '10da53b8df11f36c68c2c17bb1026222rep5.jpg', NULL, NULL, '10da53b8df11f36c68c2c17bb1026222rep5.jpg', '2020-05-27 07:23:01', '2020-05-27 07:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `company_uid` varchar(255) DEFAULT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `rating` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `likes` varchar(255) DEFAULT '0',
  `report_ppl` varchar(255) DEFAULT NULL,
  `report_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `uid`, `company_uid`, `author_uid`, `author_name`, `title`, `paragraph_one`, `paragraph_two`, `paragraph_three`, `rating`, `image`, `likes`, `report_ppl`, `report_id`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '0fa9a146ad4eb0501f735e6b74f046cb', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test one', NULL, NULL, '1', '0fa9a146ad4eb0501f735e6b74f046cbFacebookBanner-like.png', '0', NULL, NULL, NULL, 'Rejected', '2020-05-11 01:46:58', '2020-05-15 10:00:37'),
(2, 'c2df33a07c1570f52333555c813bbab5', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test two', NULL, NULL, '2', 'c2df33a07c1570f52333555c813bbab5image-01.png', '6', 'user', '87fe7166a03c03a394405881c8929e4f', 'Reported', 'Yes', '2020-05-11 01:49:14', '2020-05-18 07:42:40'),
(3, '09d79c335af30a8f3ec949d9d1670b0b', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test three', NULL, NULL, '3', '09d79c335af30a8f3ec949d9d1670b0bimage-02.jpg', '4', NULL, NULL, NULL, 'Yes', '2020-05-11 01:50:31', '2020-05-18 07:19:05'),
(4, 'a672bbbbd21bf0d5fd918549600b842a', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test four', NULL, NULL, '4', 'a672bbbbd21bf0d5fd918549600b842aimage-03.jpg', '0', NULL, NULL, NULL, 'Pending', '2020-05-11 01:50:58', '2020-05-15 10:00:42'),
(5, 'f43e93914a35b0796266a626178fda52', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test five', NULL, NULL, '5', 'f43e93914a35b0796266a626178fda52image-04.png', '2', NULL, NULL, NULL, 'Yes', '2020-05-11 01:51:10', '2020-05-18 07:19:07'),
(6, '901d3e7f615b01156b0ef5be37c31d13', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test six', NULL, NULL, '1', '901d3e7f615b01156b0ef5be37c31d13image-05.png', '0', NULL, NULL, NULL, 'Pending', '2020-05-11 01:51:42', '2020-05-15 10:00:47'),
(7, 'fb724c31955b569b77437b11f8e6a10b', 'b312b2a08c82e29a07d4e54daf2b765c', '9349aede685bae49c49b0b895e465f6d', 'user', 'Wiskey', 'review test seven', NULL, NULL, '2', 'fb724c31955b569b77437b11f8e6a10bimage-06.png', '0', NULL, NULL, NULL, 'Pending', '2020-05-11 01:51:53', '2020-05-15 10:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `reviews_respond`
--

CREATE TABLE `reviews_respond` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `review_uid` varchar(255) DEFAULT NULL,
  `like_amount` int(255) DEFAULT NULL,
  `dislike_amount` int(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `company_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `featured_seller` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_person_no` varchar(255) DEFAULT NULL,
  `sec_contact_person` varchar(255) DEFAULT NULL,
  `sec_contact_person_no` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `cert` varchar(255) DEFAULT NULL,
  `services` varchar(255) DEFAULT NULL,
  `breed_type` varchar(255) DEFAULT NULL,
  `other_info` text DEFAULT NULL,
  `info_two` text DEFAULT NULL,
  `info_three` text DEFAULT NULL,
  `info_four` text DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `company_pic` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `uid`, `company_name`, `slug`, `registration_no`, `contact_no`, `email`, `address`, `state`, `country`, `account_status`, `featured_seller`, `contact_person`, `contact_person_no`, `sec_contact_person`, `sec_contact_person_no`, `experience`, `cert`, `services`, `breed_type`, `other_info`, `info_two`, `info_three`, `info_four`, `company_logo`, `company_pic`, `rating`, `date_created`, `date_updated`) VALUES
(1, 'd9eb76014b9f8dd22233475e617635a6', 'A Famosa Pet House', 'a-famosa-pet-house', NULL, 'https://api.whatsapp.com/send?phone=60166163293', NULL, NULL, 'Malacca', NULL, NULL, NULL, 'Ms Janet', '60166163293', NULL, NULL, '5', '', 'Delivery Services', 'Golden Retriever, Maltese, Pomeranian, Chow Chow, Shih Tzu, Alaskan Malamute, Husky, Pug, Beagle', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(2, '88087606a81fdd8dbbd9a08f9f215942', 'Albipetzone', 'albipetzone', NULL, 'https://api.whatsapp.com/send?phone=60123639846', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Mr Issam', '60123639846', NULL, NULL, '20', '', 'Import &amp; Export Cats, Boarding and Grooming', 'British Shorthair, Scottish Fold, Munchkin, Minuet, Ragdol, Spynx, Bengal', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(3, '8f5330486e33a300cd9dfdcd8475afb3', 'Ardonia Marketing', 'ardonia-marketing', NULL, 'https://api.whatsapp.com/send?phone=60195900627', NULL, NULL, 'Penang', NULL, NULL, NULL, 'Mr Jason', '60195900627', NULL, NULL, '10', '', 'Free Delivery Services to whole Peninsular Malaysia', 'Pomeranian', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(4, '746531fb4b8d21f30c8bf99df8461d8b', 'AZ Pets Empire', 'az-pets-empire', NULL, 'https://api.whatsapp.com/send?phone=60162277882', NULL, NULL, 'Johor', NULL, NULL, NULL, 'Zan', '60162277882', NULL, NULL, '10', 'GSD Dog Show Trophy (Champion)', 'Pet Shop in Kulai, JB (AZ Pets Empire) - Grooming, Boarding, Delivery Service, Food and Accessories', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(5, '6aaab7fece8b0178b98a25f9762921f2', 'D&K Pet Centre', 'd&k-pet-centre', NULL, 'https://api.whatsapp.com/send?phone=60126377288', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Mr Alex', '60126377288', NULL, NULL, '5', '', 'D&K Pet Centre Address: 28, Lorong Batu Nilam 21B, Bandar Bukit Tinggi 2, 41200 Klang, Selangor', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(6, '7bb065abe1e97c67aa54701e055b7e8b', 'Dorzwer Pet Shop', 'dorzwer-pet-shop', NULL, 'https://api.whatsapp.com/send?phone=601111908293', NULL, NULL, 'Johor', NULL, NULL, NULL, 'Mr Jackson', '601111908293', NULL, NULL, '3', '', 'Pet Shop at Kulai, Johor. Search map \"Dorzwer Pet Shop\", Selling Pet Accessories, Home Made Pet\'s Food &amp; Boarding Services. (Appointments Basis Only)', 'All small, medium & large breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(7, '16d51a64fe481b79bc03c4b3b54b9fdc', 'Durain Sam', 'durain-sam', NULL, 'https://api.whatsapp.com/send?phone=60127003821', NULL, NULL, 'Penang', NULL, NULL, NULL, 'Mr Sam', '60127003821', NULL, NULL, '25', 'Breeding USA Multiple Champions and Grand Champion Parents', 'Delivery Services', 'Pomeranian', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(8, '1c09bb800c158651ac4acaebadd1e759', 'Fur', 'fur', NULL, 'https://api.whatsapp.com/send?phone=60176720082', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Ms Winnie Yap', '60176720082', NULL, NULL, '10', '', 'Delivery Services', 'Pomeranian, Pembroke Welsh Corgi, French Bulldog', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(9, '26c06fe5e5cbbc65d643de3f911043a5', 'Furrbulus', 'furrbulus', NULL, 'https://api.whatsapp.com/send?phone=60123472412', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Mr Iman', '60123472412', NULL, NULL, '6', '', 'Cat Breeder', 'British Short Hair, British Long Hair, Scottish Fold, Scottish Straight, Munchkin, Mainecoon, Domestic Longhair, Domestic Shorthair', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(10, 'e7cd4949d2c9bc307271aba119313682', 'Happiness Pet House', 'happiness-pet-house', NULL, 'https://api.whatsapp.com/send?phone=60177737431', NULL, NULL, 'Johor', NULL, NULL, NULL, 'Ms Yoshiko', '60177737431', NULL, NULL, '7', '', 'Boarding and Delivery Services', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(11, 'ee02c7823df5c79d5f7167d8ec3f00fe', 'Happy Paws', 'happy-paws', NULL, 'https://api.whatsapp.com/send?phone=60166330770', NULL, NULL, 'Kedah', NULL, NULL, NULL, 'Ms Bonnie', '60166330770', NULL, NULL, '8', '', 'Boarding and Delivery Services', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(12, '9a2775d0e0838cb7c27e511c3e1c4c96', 'Happy Pets Mart', 'happy-pets-mart', NULL, 'https://api.whatsapp.com/send?phone=60143386866', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Mr Jiason', '60143386866', NULL, NULL, '5', '', 'Stud, Consultation, Trading, Boarding and Delivery Services', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '5', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(13, '741afb08f978e0baab253c09eae91086', 'Harmony Pet House', 'harmony-pet-house', NULL, 'https://api.whatsapp.com/send?phone=60175566918', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Ms Samantha', '60175566918', NULL, NULL, '5', '', 'Boarding and Delivery Services', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(14, 'be45ad8a94c7ae37bb9fd57049a22bde', 'Infinite Bully', 'infinite-bully', NULL, 'https://api.whatsapp.com/send?phone=60165230069', NULL, NULL, 'Perak', NULL, NULL, NULL, 'Mr Hah', '60165230069', NULL, NULL, '6', 'American Bully Show', 'Delivery Services', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(15, 'a0940fc88a6ef5b451e8446d4a4cad15', 'J Puppies Corner', 'j-puppies-corner', NULL, 'https://api.whatsapp.com/send?phone=60177897181', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Ms Jasmine', '60177897181', NULL, NULL, '15', '', 'Grooming and Boarding', 'All small breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(16, '8e5e28b768cf149c46ab409c87ced6b6', 'J2 Pet House', 'j2-pet-house', NULL, 'https://api.whatsapp.com/send?phone=60183684310', NULL, NULL, 'Johor', NULL, NULL, NULL, 'Mr J2', '60183684310', NULL, NULL, '5', '', 'Delivery Services', 'All small, medium and large breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(17, '11673a3887df271a0916ef92905a2e3f', 'Joe\'s Pet', 'joe\'s-pet', NULL, 'https://api.whatsapp.com/send?phone=60162777008', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Mr Joe', '60162777008', NULL, NULL, '8', '', 'Boarding', 'All small breeds', NULL, NULL, NULL, NULL, NULL, NULL, '2', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(18, '506f9f33e9e190ccedd19761500ae8db', 'Klang Pet House', 'klang-pet-house', NULL, 'https://api.whatsapp.com/send?phone=60166019837', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Ms Zhewen', '60166019837', NULL, NULL, '5', '', 'Delivery Services', 'Poodle, Siberian Husky, Wooly Husky, Golden Retriever, Pomeranian', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(19, '928c124540423a283e93b00dcec2657c', 'Kooi Lim Pet Kingdom', 'kooi-lim-pet-kingdom', NULL, 'https://api.whatsapp.com/send?phone=60182823933', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Ms MC', '60182823933', NULL, NULL, '25', 'In house more than 50 champion dogs', 'Grooming and Boarding Services, Consultation', 'Cat-British short hair pure, British shorthair mix  Scottish fold, British shorthair mix scottish straight, British shorthair mix munchkin, Bengal, Bengal mix fold,maine coon, Persian, Persian mix Scottish fold, Persian mix scottish straight, persian mix ', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(20, 'f450808f0be36184255e5ff0fa1b5b48', 'L & Lee Pets', 'l-&-lee-pets', NULL, 'https://api.whatsapp.com/send?phone=60102900321', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Ms Xiao Wei', '60102900321', NULL, NULL, '7', '', 'Grooming, Boarding and Delivery Services, Pet Shop Address: Seksyen U5 Jalan Musytari Am U5/Am, No 5-1, 40150 Shah Alam, Selangor', 'Poodle and Mix Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(21, '39f1729a071c38f40b7344435cf61147', 'LEC Pets World', 'lec-pets-world', NULL, 'https://api.whatsapp.com/send?phone=60193380602', NULL, NULL, 'Malacca', NULL, NULL, NULL, 'Mr Li Heng', '60193380602', NULL, NULL, '3', '', 'Selling Pet Accessories, Boarding & Delivery Services', 'All small, medium and large breed', NULL, NULL, NULL, NULL, NULL, NULL, '5', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(22, '0ffd14950bab62254d0a80691934ba21', 'Liewworldkenn', 'liewworldkenn', NULL, 'https://api.whatsapp.com/send?phone=60169219775', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Mr Alex', '60169219775', NULL, NULL, '13', 'Malaysia Rottweiler Club + MKA Special Joint Show (Reserve Best Junior In Show)', 'Stud and Delivery Services', 'Rottweiler, Boerboel, Poodle, Doberman, German Shepherd', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(23, '1cc743bab1bbf17684a5c6fdc61b7ac5', 'LK Pom', 'lk-pom', NULL, 'https://api.whatsapp.com/send?phone=60169779775', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Mr LK', '60169779775', NULL, NULL, '3', '', 'Delivery Services', 'Pomeranian, Shih Tzu, Poodle, Maltese, Yorkshire Terrier, Corgi', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(24, 'e9910b4e71aa545522aa637be262f40c', 'Love Nest Pet Shop', 'love-nest-pet-shop', NULL, 'https://api.whatsapp.com/send?phone=60165139182', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Ms Denise', '60165139182', NULL, NULL, '2', '', 'Grooming and Boarding Services, Love Nest Pet Shop @ Kepong', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(25, 'f52f5fa478d156cc5930bc683e6fd7c3', 'Lucky 19 Pet', 'lucky-19-pet', NULL, 'https://api.whatsapp.com/send?phone=60182359681', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Ms Jolie', '60182359681', NULL, NULL, '2', '', 'Delivery Services (West Malaysia & Singapore), SSM Certified Seller for Dog Accessories', 'All small, medium and large breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(26, 'aecf0bf36fda97ef057839b38f294ea8', 'May Pet Station', 'may-pet-station', NULL, 'https://api.whatsapp.com/send?phone=60186666162', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Ms Kong', '60186666162', NULL, NULL, '8', '', 'Boarding and Delivery Services', 'Poodle, Maltese, Chihuahua, Pomeranian, Pug', NULL, NULL, NULL, NULL, NULL, NULL, '5', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(27, '6546b8f7c18864a3468f153c41f033d9', 'Mr JJ Pet Centre', 'mr-jj-pet-centre', NULL, 'https://api.whatsapp.com/send?phone=60164776755', NULL, NULL, 'Penang', NULL, NULL, NULL, 'Ms Anna / Mr Jeft', '60164776755', NULL, NULL, '5', '', 'Boarding and Delivery Services, Selling Pet Accessories', 'All small, medium and large breed', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(28, '799481115fb73784831dda85f741c84d', 'Pandora Pet Shop', 'pandora-pet-shop', NULL, 'https://api.whatsapp.com/send?phone=60189128055', NULL, NULL, 'Penang', NULL, NULL, NULL, 'Mr Kelvin / Ms Shandy', '60189128055', NULL, NULL, '9', '', 'Pet Shop at Butterworth, Penang. Google search \"Pandora Pet Shop\", provides boarding and grooming services', 'All breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(29, 'd34e01b73d69c7c85352e42f59ec7027', 'Pawfurry', 'pawfurry', NULL, 'https://api.whatsapp.com/send?phone=60124731911', NULL, NULL, 'Penang', NULL, NULL, NULL, 'Ms Kerry', '60124731911', NULL, NULL, '3', '', 'Pet Shop in Bukit Mertajam (Pawfurry), Grooming, Boarding and Delivery Services', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(30, '442f6b70a976442fdd5e378cdab1963c', 'Pet Store 1393', 'pet-store-1393', NULL, 'https://api.whatsapp.com/send?phone=60127131393', NULL, NULL, 'Johor', NULL, NULL, NULL, 'Mdm Foo', '60127131393', NULL, NULL, '11', '', 'Delivery Service', 'French Bulldog', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(31, '50a89de4c82acc93360a6fa586d79c91', 'PetsValley', 'petsvalley', NULL, 'https://api.whatsapp.com/send?phone=601111923168', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Ms Yuki / Mr MoongJi', '601111923168', NULL, NULL, '9', '', 'Worldwide & Local Delivery Services', 'Imported puppies, Poodle, Bichon Frise, Shih Tzu, Maltese, Schnauzer, Corgi, French Bulldog, English Bulldog', NULL, NULL, NULL, NULL, NULL, NULL, '5', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(32, '292a5b9b3bca4e993c6aea2cfe40bd8c', 'Petswow Dreamland', 'petswow-dreamland', NULL, 'https://api.whatsapp.com/send?phone=60162250952', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Ms Rachel', '60162250952', NULL, NULL, '10', '', 'Boarding and Delivery Services', 'All breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(33, 'fba46e4f2661852757fe7bc12cda48de', 'Puppy Asia', 'puppy-asia', NULL, 'https://api.whatsapp.com/send?phone=60183571669', NULL, NULL, 'Johor', NULL, NULL, NULL, 'Ms Gan', '60183571669', NULL, NULL, '5', '', 'Boarding, Grooming and Delivery Services', 'All Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(34, '408b3d3d25bfdae8c2c8442a8183ba10', 'Pure Pretty', 'pure-pretty', NULL, 'https://api.whatsapp.com/send?phone=60189671427', NULL, NULL, 'Perak', NULL, NULL, NULL, 'Ms Miki +6016-535 9325 / Ms Audrey  +6011-5756 8832 / Ms Chlowe', '60189671427', NULL, NULL, '9', '', 'Delivery Service', 'All breeds', NULL, NULL, NULL, NULL, NULL, NULL, '5', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(35, '24218a98971e37fa6b06739b8787f43a', 'Sam Chong', 'sam-chong', NULL, 'https://api.whatsapp.com/send?phone=60125450005', NULL, NULL, 'Kedah', NULL, NULL, NULL, 'Mr Sam', '60125450005', NULL, NULL, '12', '', 'USA Pure Breed Pomeranian', 'Pomeranian', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(36, '6ce88e7d7790349bd72747dcfbb5167b', 'Shun Shun', 'shun-shun', NULL, 'https://api.whatsapp.com/send?phone=60172836133', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Shun Shun', '60172836133', NULL, NULL, '8', 'Grand Champion Malaysia, Champion Korea, Thailand and Russia', 'Sells Pet Food, Barf and Supplement', 'All breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(37, '265756ee1782024d61b397309af7c208', 'The Love Ones', 'the-love-ones', NULL, 'https://api.whatsapp.com/send?phone=60164563432', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Ms Mandy', '60164563432', NULL, NULL, '7', 'Grade C Certified Professional, Pet Groomer', 'Grooming, Boarding & Pet Transportation', 'Maltese, Pug, Bichon Frise, Silky Terrier, Jack Russell Terrier, Chow Chow, Alaskan Malamute', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(38, '95bd3cbf54083abe326efaec413026f9', 'The One Kennel - Precious Pets', 'the-one-kennel', NULL, 'https://api.whatsapp.com/send?phone=60192798218', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Ms Angel / Mr Elvin', '60192798218', NULL, NULL, '12', 'Certified Groomer Level B, Certified Show Handler, Multi best in group @ MKA Dog Show, Multi Best of Breed @ MKA Dog Show.          -Yorkshire Terrier: Malaysia Champion, Thailand & International Champion bloodline  -Toy Poodle: Malaysia Junior Champion &', 'Pet Shop Name - Precious Pets @ 10-2-2, Jalan Setia Prima (Q) U13/Q, Setia Alam Seksyen U13, 40170 Shah Alam, Selangor. Selling Pets Food, Pets Grooming Services, Pets treatment (only for in-house customers), Pets boarding &amp; Pets relocation', 'All type small / medium/ large breed.   Speciality at breeds (Pomeranian, Yorkshire Terrier, Toy Poodle, Bichon Frise, Shih Tzu, Pekingese, Miniature Schnauzer, Standard Poodle, Maltese, West highland White Terrier, Shiba Inu, Siberian Husky with Wooly Co', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(39, 'aad5f29fb6feb7e8c22fcf686f91358e', 'The Pet Shop', 'the-pet-shop', NULL, 'https://api.whatsapp.com/send?phone=60177980895', NULL, NULL, 'Johor', NULL, NULL, NULL, 'Mr Ronald', '60177980895', NULL, NULL, '5', '', 'Boarding and Delivery Services', 'Corgi, Poodle, Golden Retriever, Pomeranian, Pug', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(40, '03e56e8b550289fff66ee4b7d9ae71ec', 'Dept Pet Grooming House', 'dept-pet-grooming-house', NULL, 'https://api.whatsapp.com/send?phone=60163522100', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Ms Amy', '60163522100', NULL, NULL, '3', '', 'Pets Grooming and Boarding services, Selling Pet Accessories, etc. Dept Pet Grooming House Address: J-G-15, Jalan PJU 1/43 Aman Suria Damansara 47301 Petaling Jaya Selangor', 'All type small / medium/ large breed (Pomeranian, Shih Tzu, Poodle, Maltese, Corgi, French Bulldog, Schnauzer .....)', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(41, 'b26cf76ff37fb89b313859ea96b55333', 'Potato\'s Pets World', 'potato\'s-pets-world', NULL, 'https://api.whatsapp.com/send?phone=60165230069', NULL, NULL, 'Perak', NULL, NULL, NULL, 'Mr Hah', '60165230069', NULL, NULL, '6', 'American Bully Show', 'Delivery Services', 'All breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(42, 'a7759222da88bab975cb252ce3b16ad1', 'Paw & Tail', 'paw-&-tail', NULL, 'https://api.whatsapp.com/send?phone=60172910191', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Mr Ivan', '60172910191', NULL, NULL, '1', '', 'Delivery Services', 'Small / Medium Breeds', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(43, '724532d1177895b7dcb6e791e31238f3', 'Tommy Pet Station', 'tommy-pet-station', NULL, 'https://api.whatsapp.com/send?phone=60169444910', NULL, NULL, 'Pahang', NULL, NULL, NULL, 'Mr Tommy', '60169444910', NULL, NULL, '8', '', 'Delivery services, Home made pet\'s food, Boarding services etc.', 'Chihuahua, Shih Tzu, Maltese, Poodle, Corgi, Golden Retriever, French Bulldog, English Bulldog, Pug', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(44, 'a71af509bcf23d582e4042b1b4da269f', 'Pets Avenue', 'pets-avenue', NULL, 'https://api.whatsapp.com/send?phone=60123502886', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Mr William +6012-938 3022 /', '60123502886', NULL, NULL, '9', 'Enrolled with Master of Science in Engineering and Master of Business Administration, with the interest and hobbies of providing services in canine field allow us to quickly grow in business. Looking for long term business strategy by using a customer man', 'Pets Grooming, Pets Boarding, Mobile Grooming, Pet Taxi and_x000D_ Transportation. Chinchilla and small animal', '[Dogs]:  Small Breed - Pomeranians, Poodle, Chihuahua, Miniature Schnauzer, Pug. Medium - Corgi, Beagle, French Bulldog, Chow Chow, Border Collie, Shetland Sheepdog. Large - German Shepherds, Golden Retriever, Labrador Retriever, Mastiff Tibetan, Dalmatio', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(45, 'b72493473c88c1cccbae85495faee326', 'Win Win Pet House', 'win-win-pet-house', NULL, 'https://api.whatsapp.com/send?phone=60164224712', NULL, NULL, 'Penang', NULL, NULL, NULL, 'Ms Khoo +6010-391 7932 /', '60164224712', NULL, NULL, '1', '', 'Delivery Services, Pet Supplies including Food, Milk and Accessories', 'All small / medium/ large breed types', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(46, 'bb5655991ac030b56624d41212d8958f', 'Yuniken', '-yuniken-', NULL, 'https://api.whatsapp.com/send?phone=60182678722', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Michael +6012-329 8732 / Nick', '60182678722', NULL, NULL, '20', 'Grandparents Imported Bloodline', 'Delivery Services', 'Small Breed - French bulldog', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(47, 'c8b360739f6e9b7f17aa5f12106a496e', 'Catrosaz Cattery', 'catrosaz-cattery', NULL, 'https://api.whatsapp.com/send?phone=60122133030', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Rosina Othman / Zayd Azam', '60122133030', NULL, NULL, '5', 'TICA Cert', 'Delivery Services', 'Mainecoon and others', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(48, '7ade6f7d35f0df52942978a46417263b', 'Little Asia Pet Station', 'little-asia-pet-station', NULL, 'https://api.whatsapp.com/send?phone=601111661678', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Ben +6016-907 0005 / Linny', '601111661678', NULL, NULL, '10', '-', 'Pet Hotel, Grooming, Boarding and Selling Pet Accessories', 'All type small / medium/ large breed (Dog: Chihuahua, Shih Tzu, Poodle, Maltese, Pomeranian, Corgi, Golden Retriever, French Bulldog, English Bulldog, Pug and others. Cat: American Short Hair, British Short Hair, Persian, Munchkin).', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(49, '2f9a57163c56b126cb94e4803782b232', 'R.I.A World Kennel Pet Shop', 'r.i.a-world-kennel-pet-shop', NULL, 'https://api.whatsapp.com/send?phone=60142171445', NULL, NULL, 'Selangor', NULL, NULL, NULL, 'Ms Jassy', '60142171445', NULL, NULL, '30', '', 'Boarding, Whelping and Nursery, Dog Walker, Pet Taxi &amp; Stud Services', 'All Type Small, Medium and Large Breed', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(50, 'c94241f6405a4f7333af77c1ffa02082', 'Baby Pet Hub', 'baby-pet-hub', NULL, 'https://api.whatsapp.com/send?phone=60169443887', NULL, NULL, 'Johor', NULL, NULL, NULL, 'Xiao Bai', '60169443887', NULL, NULL, '5', '', 'Selling Pet Accessories and Food', 'Pomeranian', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(51, '6feb8489b7741dea24d07a358ef0a5ca', 'Gold Star Beauty Petstore - You & Me Pets', 'gold-star-beauty-petstore', NULL, 'https://api.whatsapp.com/send?phone=60192333855', NULL, NULL, 'Kuala Lumpur', NULL, NULL, NULL, 'Mr Ling', '60192333855', NULL, NULL, '20', '', 'Pet Nutrition/supplement and Supply Pet Accessories, Mainly offering healthy and active Puppy to cheer your day.', 'Specialised at breeding Pomeranian, Poodle and Malteses puppy (Imported Parent with reputable bloodline)', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(52, '9f46703c15e386718ee38435e6dc6e87', 'Pet N Go Cat Centre', 'pet-n-go-partnership', NULL, 'https://api.whatsapp.com/send?phone=60195158228', NULL, NULL, 'Kedah', NULL, NULL, NULL, 'Pet N Go', '60195158228', NULL, NULL, '2', '', 'Grooming, Boarding and Delivery Food &amp; Accessories services, Selling Pet Nutrition and supplements.', 'Specialised at breeding British Short (Imported Parent with reputable bloodline) British Long Hair, Exotic Short / Long Hair, Scottish Fold & Persians etc.', NULL, NULL, NULL, NULL, NULL, NULL, '4', '2020-07-28 08:43:56', '2020-07-28 08:43:56');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) NOT NULL,
  `service` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Grooming Services', 'Available', 1, '2020-07-03 01:45:10', '2020-07-03 02:07:36'),
(2, 'Pets Hotel / Boarding Services', 'Available', 1, '2020-07-03 01:45:34', '2020-07-03 02:15:12'),
(3, 'Stud Services', 'Available', 1, '2020-07-03 01:45:44', '2020-07-03 01:45:44'),
(4, 'Delivery Services / Pet Taxi', 'Available', 1, '2020-07-03 01:46:08', '2020-07-03 01:46:08'),
(5, 'Selling Pet Food &amp; Pet Accessories', 'Available', 1, '2020-07-03 01:46:28', '2020-07-03 01:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `img_name` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `uid`, `img_name`, `link`, `status`, `date_created`, `date_updated`) VALUES
(1, '052f3bd0866f1f6847471f054e005fd7', '1595496209052f3bd0866f1f6847471f054e005fd7.png', 'https://pariepets.com/dog.html', 'Show', '2020-05-15 02:23:59', '2020-07-23 09:23:29'),
(2, '27c4b80568e129104977f628cf1f33bf', '159549622827c4b80568e129104977f628cf1f33bf.png', 'https://pariepets.com/cat.html', 'Show', '2020-05-15 02:24:20', '2020-07-23 09:23:48'),
(3, '650fbc9ff8a916fcc1920fca983fa937', '650fbc9ff8a916fcc1920fca983fa9371589510512poster3.jpg', 'https://mypetslibrary.com/', 'Delete', '2020-05-15 02:41:52', '2020-07-08 01:14:17');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name`, `date_created`, `date_updated`) VALUES
(1, 'Johor', '2020-05-22 06:42:19', '2020-05-29 01:44:12'),
(2, 'Kedah', '2020-05-22 06:42:48', '2020-05-29 01:44:08'),
(3, 'Kelantan', '2020-05-22 06:42:48', '2020-05-29 01:44:03'),
(4, 'Negeri Sembilan', '2020-05-22 06:43:46', '2020-05-29 01:43:59'),
(5, 'Pahang', '2020-05-22 06:43:46', '2020-05-29 01:43:55'),
(6, 'Perak', '2020-05-22 06:44:16', '2020-05-29 01:43:52'),
(7, 'Perlis', '2020-05-22 06:44:16', '2020-05-29 01:43:50'),
(8, 'Selangor', '2020-05-22 06:44:37', '2020-05-29 01:43:45'),
(9, 'Terengannu', '2020-05-22 06:44:37', '2020-05-29 01:43:40'),
(10, 'Malacca', '2020-05-22 06:44:48', '2020-05-22 06:44:48'),
(11, 'Penang', '2020-05-22 06:44:48', '2020-05-22 06:44:48'),
(12, 'Sabah ', '2020-05-22 06:44:58', '2020-05-22 06:44:58'),
(13, 'Sarawak', '2020-05-22 06:44:58', '2020-05-22 06:44:58'),
(14, 'Kuala Lumpur', '2020-05-22 06:48:57', '2020-05-22 06:48:57'),
(15, 'Labuan', '2020-05-22 06:48:57', '2020-05-22 06:48:57'),
(16, 'Putrajaya', '2020-05-22 06:49:05', '2020-05-22 06:49:05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT 'Malaysia',
  `phone_no` varchar(255) DEFAULT NULL,
  `tac` varchar(255) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(64) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0=admin, 1=user, 2=seller',
  `fb_id` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `receiver_contact_no` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_area` varchar(255) DEFAULT NULL,
  `shipping_postal_code` varchar(255) DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT 'profile-pic.jpg',
  `points` varchar(255) DEFAULT NULL,
  `favorite_puppy` text DEFAULT NULL,
  `favorite_kitten` text DEFAULT NULL,
  `favorite_reptile` text DEFAULT NULL,
  `favorite_product` text DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `name`, `email`, `country`, `phone_no`, `tac`, `password`, `salt`, `user_type`, `fb_id`, `birthday`, `gender`, `account_status`, `receiver_name`, `receiver_contact_no`, `shipping_state`, `shipping_area`, `shipping_postal_code`, `shipping_address`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name_on_card`, `card_no`, `card_type`, `expiry_date`, `ccv`, `postal_code`, `billing_address`, `profile_pic`, `points`, `favorite_puppy`, `favorite_kitten`, `favorite_reptile`, `favorite_product`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `date_created`, `date_updated`) VALUES
(1, 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'admin@gmail.com', 'Malaysia', '0121122330', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:16', '2020-04-28 05:52:34'),
(2, '9349aede685bae49c49b0b895e465f6d', 'user', 'user@gmail.com', 'Malaysia', '0124455660', NULL, 'fb1343bf3ac191ede70b6d323ac4a19b10905f49ad1df13338c5b6a40ca33f54', 'e70e7e71ece4a3469028b5a4b2cf316bbb448d70', 1, NULL, NULL, NULL, NULL, 'asd', '0126012', 'cvb', 'asd zxc', '123321', '123,asd asd', 'HSBC', 'Oliver Queen', '1234455667', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1595495202.png', '962', NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:55', '2020-07-23 09:06:42'),
(3, 'c2e4a20007e868502ea2857e094af93b', 'user2', 'user2@gmail.com', 'Singapore', '0127788990', NULL, 'fa0a1acac2e0844b2a9996cf8d57d86db163b04f7e93cdd157056992962273e4', '84d6588f07b2dd352d4e413a2dfe53fad13ff245', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:06:21', '2020-04-28 05:40:25'),
(4, 'd9eb76014b9f8dd22233475e617635a6', 'A Famosa Pet House', NULL, 'Malaysia', '60166163293', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(5, '88087606a81fdd8dbbd9a08f9f215942', 'Albipetzone', NULL, 'Malaysia', '60123639846', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(6, '8f5330486e33a300cd9dfdcd8475afb3', 'Ardonia Marketing', NULL, 'Malaysia', '60195900627', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(7, '746531fb4b8d21f30c8bf99df8461d8b', 'AZ Pets Empire', NULL, 'Malaysia', '60162277882', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(8, '6aaab7fece8b0178b98a25f9762921f2', 'D&K Pet Centre', NULL, 'Malaysia', '60126377288', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(9, '7bb065abe1e97c67aa54701e055b7e8b', 'Dorzwer Pet Shop', NULL, 'Malaysia', '601111908293', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(10, '16d51a64fe481b79bc03c4b3b54b9fdc', 'Durain Sam', NULL, 'Malaysia', '60127003821', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(11, '1c09bb800c158651ac4acaebadd1e759', 'Fur', NULL, 'Malaysia', '60176720082', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(12, '26c06fe5e5cbbc65d643de3f911043a5', 'Furrbulus', NULL, 'Malaysia', '60123472412', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(13, 'e7cd4949d2c9bc307271aba119313682', 'Happiness Pet House', NULL, 'Malaysia', '60177737431', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(14, 'ee02c7823df5c79d5f7167d8ec3f00fe', 'Happy Paws', NULL, 'Malaysia', '60166330770', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(15, '9a2775d0e0838cb7c27e511c3e1c4c96', 'Happy Pets Mart', NULL, 'Malaysia', '60143386866', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(16, '741afb08f978e0baab253c09eae91086', 'Harmony Pet House', NULL, 'Malaysia', '60175566918', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(17, 'be45ad8a94c7ae37bb9fd57049a22bde', 'Infinite Bully', NULL, 'Malaysia', '60165230069', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(18, 'a0940fc88a6ef5b451e8446d4a4cad15', 'J Puppies Corner', NULL, 'Malaysia', '60177897181', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(19, '8e5e28b768cf149c46ab409c87ced6b6', 'J2 Pet House', NULL, 'Malaysia', '60183684310', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(20, '11673a3887df271a0916ef92905a2e3f', 'Joe\'s Pet', NULL, 'Malaysia', '60162777008', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(21, '506f9f33e9e190ccedd19761500ae8db', 'Klang Pet House', NULL, 'Malaysia', '60166019837', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(22, '928c124540423a283e93b00dcec2657c', 'Kooi Lim Pet Kingdom', NULL, 'Malaysia', '60182823933', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(23, 'f450808f0be36184255e5ff0fa1b5b48', 'L & Lee Pets', NULL, 'Malaysia', '60102900321', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(24, '39f1729a071c38f40b7344435cf61147', 'LEC Pets World', NULL, 'Malaysia', '60193380602', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(25, '0ffd14950bab62254d0a80691934ba21', 'Liewworldkenn', NULL, 'Malaysia', '60169219775', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(26, '1cc743bab1bbf17684a5c6fdc61b7ac5', 'LK Pom', NULL, 'Malaysia', '60169779775', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(27, 'e9910b4e71aa545522aa637be262f40c', 'Love Nest Pet Shop', NULL, 'Malaysia', '60165139182', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(28, 'f52f5fa478d156cc5930bc683e6fd7c3', 'Lucky 19 Pet', NULL, 'Malaysia', '60182359681', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(29, 'aecf0bf36fda97ef057839b38f294ea8', 'May Pet Station', NULL, 'Malaysia', '60186666162', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(30, '6546b8f7c18864a3468f153c41f033d9', 'Mr JJ Pet Centre', NULL, 'Malaysia', '60164776755', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(31, '799481115fb73784831dda85f741c84d', 'Pandora Pet Shop', NULL, 'Malaysia', '60189128055', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(32, 'd34e01b73d69c7c85352e42f59ec7027', 'Pawfurry', NULL, 'Malaysia', '60124731911', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(33, '442f6b70a976442fdd5e378cdab1963c', 'Pet Store 1393', NULL, 'Malaysia', '60127131393', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(34, '50a89de4c82acc93360a6fa586d79c91', 'PetsValley', NULL, 'Malaysia', '601111923168', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(35, '292a5b9b3bca4e993c6aea2cfe40bd8c', 'Petswow Dreamland', NULL, 'Malaysia', '60162250952', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(36, 'fba46e4f2661852757fe7bc12cda48de', 'Puppy Asia', NULL, 'Malaysia', '60183571669', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(37, '408b3d3d25bfdae8c2c8442a8183ba10', 'Pure Pretty', NULL, 'Malaysia', '60189671427', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(38, '24218a98971e37fa6b06739b8787f43a', 'Sam Chong', NULL, 'Malaysia', '60125450005', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(39, '6ce88e7d7790349bd72747dcfbb5167b', 'Shun Shun', NULL, 'Malaysia', '60172836133', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(40, '265756ee1782024d61b397309af7c208', 'The Love Ones', NULL, 'Malaysia', '60164563432', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(41, '95bd3cbf54083abe326efaec413026f9', 'The One Kennel - Precious Pets', NULL, 'Malaysia', '60192798218', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(42, 'aad5f29fb6feb7e8c22fcf686f91358e', 'The Pet Shop', NULL, 'Malaysia', '60177980895', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(43, '03e56e8b550289fff66ee4b7d9ae71ec', 'Dept Pet Grooming House', NULL, 'Malaysia', '60163522100', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(44, 'b26cf76ff37fb89b313859ea96b55333', 'Potato\'s Pets World', NULL, 'Malaysia', '60165230069', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(45, 'a7759222da88bab975cb252ce3b16ad1', 'Paw & Tail', NULL, 'Malaysia', '60172910191', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(46, '724532d1177895b7dcb6e791e31238f3', 'Tommy Pet Station', NULL, 'Malaysia', '60169444910', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(47, 'a71af509bcf23d582e4042b1b4da269f', 'Pets Avenue', NULL, 'Malaysia', '60123502886', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(48, 'b72493473c88c1cccbae85495faee326', 'Win Win Pet House', NULL, 'Malaysia', '60164224712', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(49, 'bb5655991ac030b56624d41212d8958f', 'Yuniken', NULL, 'Malaysia', '60182678722', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(50, 'c8b360739f6e9b7f17aa5f12106a496e', 'Catrosaz Cattery', NULL, 'Malaysia', '60122133030', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(51, '7ade6f7d35f0df52942978a46417263b', 'Little Asia Pet Station', NULL, 'Malaysia', '601111661678', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(52, '2f9a57163c56b126cb94e4803782b232', 'R.I.A World Kennel Pet Shop', NULL, 'Malaysia', '60142171445', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(53, 'c94241f6405a4f7333af77c1ffa02082', 'Baby Pet Hub', NULL, 'Malaysia', '60169443887', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(54, '6feb8489b7741dea24d07a358ef0a5ca', 'Gold Star Beauty Petstore - You & Me Pets', NULL, 'Malaysia', '60192333855', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56'),
(55, '9f46703c15e386718ee38435e6dc6e87', 'Pet N Go Cat Centre', NULL, 'Malaysia', '60195158228', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'profile-pic.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-07-28 08:43:56', '2020-07-28 08:43:56');

-- --------------------------------------------------------

--
-- Table structure for table `vaccination`
--

CREATE TABLE `vaccination` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vaccination`
--

INSERT INTO `vaccination` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '1st Vaccination Done', 'Available', 1, '2020-06-25 01:40:17', '2020-06-25 01:40:17'),
(2, '2nd Vaccination Done', 'Available', 1, '2020-06-25 01:40:17', '2020-06-25 01:40:17'),
(3, '3rd Vaccination Done', 'Available', 1, '2020-06-25 01:40:45', '2020-06-25 01:40:45'),
(4, 'No', 'Available', 1, '2020-06-25 01:40:45', '2020-06-25 01:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `variation`
--

CREATE TABLE `variation` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `variation` varchar(255) NOT NULL,
  `variation_price` decimal(8,0) NOT NULL,
  `variation_stock` varchar(255) NOT NULL,
  `variation_image` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `animal_type` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keyword_one` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `variation`
--

INSERT INTO `variation` (`id`, `uid`, `product_id`, `variation`, `variation_price`, `variation_stock`, `variation_image`, `category`, `brand`, `name`, `sku`, `slug`, `animal_type`, `expiry_date`, `status`, `description`, `keyword_one`, `date_created`, `date_updated`) VALUES
(1, '385e377209fd52baf005900c16321745', 'd83d2545ed58fb580453e85d6ad6e4ea', 'Pedigree Chicken', '30', '100', 'pedigree1.jpg', 'Food', 'Brand 1', 'Pedigree', '', 'https://www.pedigree.com', 'Puppy', '2021-12-25', 'Available', 'Delicious Food, Dogs Favorite', 'pedigree, dog&#039;s food', '2020-06-04 02:31:47', '2020-06-04 02:31:47'),
(2, '385e377209fd52baf005900c16321745', 'd83d2545ed58fb580453e85d6ad6e4ea', 'Pedigree Mutton', '32', '110', 'pedigree2.jpg', 'Food', 'Brand 1', 'Pedigree', '', 'https://www.pedigree.com', 'Puppy', '2021-12-25', 'Available', 'Delicious Food, Dogs Favorite', 'pedigree, dog&#039;s food', '2020-06-04 02:31:47', '2020-06-04 02:31:47'),
(3, '385e377209fd52baf005900c16321745', 'd83d2545ed58fb580453e85d6ad6e4ea', 'Pedigree Fish', '36', '120', 'pedigree3.jpg', 'Food', 'Brand 1', 'Pedigree', '', 'https://www.pedigree.com', 'Puppy', '2021-12-25', 'Available', 'Delicious Food, Dogs Favorite', 'pedigree, dog&#039;s food', '2020-06-04 02:31:47', '2020-06-04 02:31:47'),
(4, '385e377209fd52baf005900c16321745', 'd83d2545ed58fb580453e85d6ad6e4ea', 'Pedigree Beef', '34', '130', 'pedigree4.jpg', 'Food', 'Brand 1', 'Pedigree', '', 'https://www.pedigree.com', 'Puppy', '2021-12-25', 'Available', 'Delicious Food, Dogs Favorite', 'pedigree, dog&#039;s food', '2020-06-04 02:31:47', '2020-06-04 02:31:47'),
(5, '573045bf397b345eb57b33a3b84e3c75', 'fcdce3745589555162f2479d56924866', 'Whiskas Ocean Fish', '32', '120', 'product1.jpg', 'Food', 'Brand 2', 'Whiskas', '', 'https://www.whiskas.com', 'Kitten', '2021-07-22', 'Available', 'Cats aged 1-6 need lots of playtime and a balanced diet to help them stay lean and healthy. Cats are carnivores while humans omnivores, so cats need two times more protein', 'cat food, whiskas', '2020-06-04 02:39:53', '2020-06-04 02:39:53'),
(6, '573045bf397b345eb57b33a3b84e3c75', 'fcdce3745589555162f2479d56924866', 'Whiskas Chicken', '32', '120', 'product2.jpg', 'Food', 'Brand 2', 'Whiskas', '', 'https://www.whiskas.com', 'Kitten', '2021-07-22', 'Available', 'Cats aged 1-6 need lots of playtime and a balanced diet to help them stay lean and healthy. Cats are carnivores while humans omnivores, so cats need two times more protein', 'cat food, whiskas', '2020-06-04 02:39:53', '2020-06-04 02:39:53'),
(7, '573045bf397b345eb57b33a3b84e3c75', 'fcdce3745589555162f2479d56924866', 'Whiskas Tuna', '34', '130', 'product3.jpg', 'Food', 'Brand 2', 'Whiskas', '', 'https://www.whiskas.com', 'Kitten', '2021-07-22', 'Available', 'Cats aged 1-6 need lots of playtime and a balanced diet to help them stay lean and healthy. Cats are carnivores while humans omnivores, so cats need two times more protein', 'cat food, whiskas', '2020-06-04 02:39:53', '2020-06-04 02:39:53'),
(8, '573045bf397b345eb57b33a3b84e3c75', 'fcdce3745589555162f2479d56924866', 'Whiskas Packet Maackerel', '34', '130', 'product4.jpg', 'Food', 'Brand 2', 'Whiskas', '', 'https://www.whiskas.com', 'Kitten', '2021-07-22', 'Available', 'Cats aged 1-6 need lots of playtime and a balanced diet to help them stay lean and healthy. Cats are carnivores while humans omnivores, so cats need two times more protein', 'cat food, whiskas', '2020-06-04 02:39:53', '2020-06-04 02:39:53'),
(9, '2e34d411fce5adf3f04bb51a99a36b6d', '72de97bd3c37723cfd0425bebd8432b9', 'Very Soft Toy Wool Age 0-5', '15', '50', 'wool1.jpg', 'Accessory', 'Brand 3', 'Cat Wool Toy', '', 'https://www.pets.com', 'Kitten', '', 'Available', 'Made of soft wool material, it is environmentally friendly and non-toxic.When your pet chews or squeezes it, it&#039;s fun.', 'cat toy, wool, soft material', '2020-06-04 02:51:13', '2020-06-04 02:51:13'),
(10, '2e34d411fce5adf3f04bb51a99a36b6d', '72de97bd3c37723cfd0425bebd8432b9', 'Soft Toy Wool Age 5-10', '17', '60', 'wool2.jpg', 'Accessory', 'Brand 3', 'Cat Wool Toy', '', 'https://www.pets.com', 'Kitten', '', 'Available', 'Made of soft wool material, it is environmentally friendly and non-toxic.When your pet chews or squeezes it, it&#039;s fun.', 'cat toy, wool, soft material', '2020-06-04 02:51:13', '2020-06-04 02:51:13'),
(11, '2e34d411fce5adf3f04bb51a99a36b6d', '72de97bd3c37723cfd0425bebd8432b9', 'Medium SoftToy Wool Age 10-15', '19', '70', 'wool3.jpg', 'Accessory', 'Brand 3', 'Cat Wool Toy', '', 'https://www.pets.com', 'Kitten', '', 'Available', 'Made of soft wool material, it is environmentally friendly and non-toxic.When your pet chews or squeezes it, it&#039;s fun.', 'cat toy, wool, soft material', '2020-06-04 02:51:14', '2020-06-04 02:51:14'),
(12, '2e34d411fce5adf3f04bb51a99a36b6d', '72de97bd3c37723cfd0425bebd8432b9', 'Medium Hard Toy Wool Age 15-20', '18', '80', 'wool4.jpg', 'Accessory', 'Brand 3', 'Cat Wool Toy', '', 'https://www.pets.com', 'Kitten', '', 'Available', 'Made of soft wool material, it is environmentally friendly and non-toxic.When your pet chews or squeezes it, it&#039;s fun.', 'cat toy, wool, soft material', '2020-06-04 02:51:14', '2020-06-04 02:51:14'),
(13, 'a9fb76fe1f43f888b7575dc028de9b0b', '7a3f87c9d4a98b78597d4310ef91175d', 'Bone Toy', '8', '300', 'toy1.jpg', 'Accessory', 'Brand 1', 'Bone Toy', '', 'https://www.toypets.com', 'Puppy', '', 'Available', 'package includes:1 x toy bone 100% brand new, high quality Safe, non-toxic, excellent hardness Effectively clean teeth, design color Daily interactive toy for your dog', 'dog toy, bone', '2020-06-04 02:54:07', '2020-06-04 02:54:07'),
(14, '5bb478c971805715b0877f4a30d9d3b1', '88ed8f3c474237be8026df5d08e1e4cf', 'Soft Ball', '9', '100', 'ball1.jpg', 'Accessory', 'Brand 2', 'Ball Toy for Dog', '', 'https://www.pets.com', 'Puppy', '', 'Available', 'Top selling and most favorite', 'dog toy, ball', '2020-06-04 02:56:26', '2020-06-04 02:56:26'),
(15, '5ac2c30a1dfb0372add40cb11c5a4b77', '083aca31db3f2d46204b16c3a6e5f084', 'Alpo Chicken ', '26', '100', 'alpo1.jpg', 'Food', 'Brand 2', 'Alpo', '', 'https://www.pets.com', 'Puppy', '', 'Available', 'Product Description Features Great meaty taste, Everything your dog needs - protein, antioxidants, calcium, omega 3&amp;6, Complete &amp; balanced for adult dogs Product', 'Alpo, dog&#039;s food', '2020-06-04 03:00:26', '2020-06-04 03:00:26'),
(16, '5ac2c30a1dfb0372add40cb11c5a4b77', '083aca31db3f2d46204b16c3a6e5f084', 'Alpo Mutton', '27', '120', 'alpo2.png', 'Food', 'Brand 2', 'Alpo', '', 'https://www.pets.com', 'Puppy', '', 'Available', 'Product Description Features Great meaty taste, Everything your dog needs - protein, antioxidants, calcium, omega 3&amp;6, Complete &amp; balanced for adult dogs Product', 'Alpo, dog&#039;s food', '2020-06-04 03:00:26', '2020-06-04 03:00:26'),
(17, '5ac2c30a1dfb0372add40cb11c5a4b77', '083aca31db3f2d46204b16c3a6e5f084', 'Alpo Beef', '28', '120', 'alpo3.jpg', 'Food', 'Brand 2', 'Alpo', '', 'https://www.pets.com', 'Puppy', '', 'Available', 'Product Description Features Great meaty taste, Everything your dog needs - protein, antioxidants, calcium, omega 3&amp;6, Complete &amp; balanced for adult dogs Product', 'Alpo, dog&#039;s food', '2020-06-04 03:00:26', '2020-06-04 03:00:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `breed`
--
ALTER TABLE `breed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kitten`
--
ALTER TABLE `kitten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_details`
--
ALTER TABLE `pet_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puppy`
--
ALTER TABLE `puppy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reported_article`
--
ALTER TABLE `reported_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reported_reviews`
--
ALTER TABLE `reported_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reptile`
--
ALTER TABLE `reptile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews_respond`
--
ALTER TABLE `reviews_respond`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`name`);

--
-- Indexes for table `vaccination`
--
ALTER TABLE `vaccination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variation`
--
ALTER TABLE `variation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `breed`
--
ALTER TABLE `breed`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kitten`
--
ALTER TABLE `kitten`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pet_details`
--
ALTER TABLE `pet_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `puppy`
--
ALTER TABLE `puppy`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `reported_article`
--
ALTER TABLE `reported_article`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reported_reviews`
--
ALTER TABLE `reported_reviews`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reptile`
--
ALTER TABLE `reptile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `reviews_respond`
--
ALTER TABLE `reviews_respond`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `vaccination`
--
ALTER TABLE `vaccination`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `variation`
--
ALTER TABLE `variation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
