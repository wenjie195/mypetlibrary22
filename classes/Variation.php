<?php
class Variation {
    /* Member variables */
    var $id,$uid,$productId,$variation,$variationPrice,$variationStock,$variationImage,$category,$brand,$name,$sku,$slug,$animalType,
    $expiryDate,$status,$description,$keywordOne,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getVariation()
    {
        return $this->variation;
    }

    /**
     * @param mixed $variation
     */
    public function setVariation($variation)
    {
        $this->variation = $variation;
    }

    /**
     * @return mixed
     */
    public function getVariationPrice()
    {
        return $this->variationPrice;
    }

    /**
     * @param mixed $variationPrice
     */
    public function setVariationPrice($variationPrice)
    {
        $this->variationPrice = $variationPrice;
    }

    /**
     * @return mixed
     */
    public function getVariationStock()
    {
        return $this->variationStock;
    }

    /**
     * @param mixed $variationStock
     */
    public function setVariationStock ($variationStock)
    {
        $this->variationStock = $variationStock;
    }

    /**
     * @return mixed
     */
    public function getVariationImage()
    {
        return $this->variationImage;
    }

    /**
     * @param mixed $variationImage
     */
    public function setVariationImage($variationImage)
    {
        $this->variationImage = $variationImage;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getAnimalType()
    {
        return $this->animalType;
    }

    /**
     * @param mixed $animalType
     */
    public function setAnimalType($animalType)
    {
        $this->animalType = $animalType;
    }

    /**
     * @return mixed
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param mixed $expiryDate
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getVariation($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","product_id","variation","variation_price","variation_stock","variation_image","category","brand",
    "name","sku","slug","animal_type","expiry_date","status","description","keyword_one","date_created","date_updated");//follow database

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"variation"); //database name
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$productId,$variation,$variationPrice,$variationStock,$variationImage,$category,$brand,
        $name,$sku,$slug,$animalType,$expiryDate,$status,$description,$keywordOne,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Variation;
            $class->setId($id);
            $class->setUid($uid);
            $class->setProductId($productId);
            $class->setVariation($variation);
            $class->setVariationPrice($variationPrice);
            $class->setVariationStock($variationStock);
            $class->setVariationImage($variationImage);
            $class->setCategory($category);
            $class->setBrand($brand);
            $class->setName($name);
            $class->setSku($sku);
            $class->setSlug($slug);
            $class->setAnimalType($animalType);
            $class->setExpiryDate($expiryDate);
            $class->setStatus($status);
            $class->setDescription($description);
            $class->setKeywordOne($keywordOne);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createProductList($products,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true){
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";


    if(!$products){
        return $productListHtml; //no product return empty list
    }

    if(isset($_GET['id'])){
        $referrerUidLink = $_GET['id'];
    }

    $count = 1;
    $index = 0;

    foreach ($products as $product){  //foreach element(of $array) defined as $value. Elements of the array will be retrieved inside the loop as $value
        $quantity = 0;
        if($postQuantityRows){
            $quantity = $postQuantityRows[$index];
        }

        if($quantity <= 0 && !$isIncludeNotSelectedProductToo){
            $productListHtml .= '<div style="display: none;">';
        }else{
            $productListHtml .= '<div style="display: block;">';
        }

        $conn=connDB();
        //$productArray = getProduct($conn);
            $id  = $product->getId();
            //$id  = $product->getVariation();
            // Include the database configuration file


            // Get images from the database
            $query = $conn->query("SELECT variation_image FROM variation WHERE id = '$id'");

            if($query->num_rows > 0){
                while($row = $query->fetch_assoc()){
                    if($product->getProductId()== $referrerUidLink){
                    $imageURL = './uploads/'.$row["variation_image"];

                    $productListHtml .= '
                    <!-- Product -->

                    <div class="left-image-div">
                        <div class="item">            
                            <div class="clearfix">
                                <ul id="image-gallery" class="gallery list-unstyled">
                                    <li data-thumb="'.$imageURL.'" class="pet-slider-li"> 
                                        <img src="'.$imageURL.'" class="pet-slider-img" alt="Pet Name" title="Pet Name" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="right-content-div2">

                        <p class="green-text breed-p">'.$product->getVariation().'</p>
                        <p class="price-p2">
                            RM '.$product->getVariationPrice().' .00
                        </p>
                        <div class="clear"></div>
                        <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                        <div class="input-group">
                            <button type="button" class="button-minus math-button transparent-button add-minus-button hover1 clean"><img src="img/minus.png" class="minus-icon hover1a" alt="Minus" title="Minus"><img src="img/minus2.png" class="minus-icon hover1b" alt="Minus" title="Minus"></button>
                                <input type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean">
                            <button type="button" class="button-plus math-button transparent-button add-minus-button hover1 clean"><img src="img/add.png" class="minus-icon hover1a" alt="Plus" title="Minus"><img src="img/add2.png" class="minus-icon hover1b" alt="Plus" title="Plus"></button>
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="clear"></div>
                    
              </div>
              ';
            }
            else{
                $productListHtml .= '
                    <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                    <div type="hidden" class="input-group">
                        <input type="hidden" type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean">
                    </div>
                    </div>
                ';
            }
            }
            $index++;
        }
    }

    $productListHtml .= '
        <script>
            $(".button-minus").on("click", function(e) {
                e.preventDefault();
                var $this = $(this);
                var $input = $this.closest("div").find("input");
                var value = parseInt($input.val());

                if (value > 1) {
                    value = value - 1;
                } else {
                    value = 0;
                }

              $input.val(value);

            });

            $(".button-plus").on("click", function(e) {
                e.preventDefault();
                var $this = $(this);
                var $input = $this.closest("div").find("input");
                var value = parseInt($input.val());

                if (value < 100) {
                    value = value + 1;
                } else {
                    value = 100;
                }

                $input.val(value);
            });
        </script>
    ';

    return $productListHtml;
}

function getProductPrice($conn,$productId){
    $price = 0;

    $productRows = getVariation($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $price = $productRows[0]->getVariationPrice();
    }

    return $price;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

//        if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
//        }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];
        $orderId = insertDynamicData($conn,"orders",array("uid"),array($uid),"s");
        //$orderId = insertDynamicData($conn,"orders",array("uid","username","full_name"),array($uid,$username,$fullName),"sss");

        if($orderId){
            $totalPrice = 0;

            for($index = 0; $index < count($shoppingCart); $index++){
                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                $quantity = $thisCart['quantity'];
                $originalPrice = getVariationPrice($conn,$productId);
                $totalPrice += ($originalPrice * $quantity);

                if(!insertDynamicData($conn,"product_orders",array("product_id","order_id","quantity","final_price","original_price","discount_given"),
                    array($productId,$orderId,$quantity,$originalPrice,$originalPrice,0),"iiiddd")){
                    promptError("error creating order for product : $productId");
                }
            }


            //todo this 2 code is AFTER payment successfully done then only execute
//            insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
//            initiateReward($conn,$orderId,$uid,$totalPrice);

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getVariation($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}
